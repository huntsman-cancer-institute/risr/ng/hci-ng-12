import {Component, Input} from "@angular/core";
import {animate, state, style, transition, trigger} from "@angular/animations";

import {interval} from "rxjs";
import {take} from "rxjs/operators";

import {Notification} from "../model/notification.entity";
import {NotificationService} from "../notification.service";

@Component({
  selector: "hci-notification-item",
  template: `
    <div class="notification-item d-flex flex-nowrap"
         (click)="$event.stopPropagation()"
         [@close]="'in'"
        [class.info]="notification.type !== 'ERROR' && notification.type !== 'WARN'"
        [class.warn]="notification.type === 'WARN'"
        [class.error]="notification.type === 'ERROR'">
      <div class="text">
        <div class="icon">
          <ng-container *ngIf="notification.type === 'INFO'">
            <i class="fas fa-info-circle fa-lg"></i>
          </ng-container>
          <ng-container *ngIf="notification.type === 'WARN'">
            <i class="fas fa-exclamation-circle fa-lg"></i>
          </ng-container>
          <ng-container *ngIf="notification.type === 'ERROR'">
            <i class="fas fa-exclamation-circle fa-lg"></i>
          </ng-container>
        </div>
        <div>
          <span *ngIf="notification.html === true" [innerHTML]="notification['notification']"></span>
          <span *ngIf="notification.html === false">{{notification["notification"]}}</span>
        </div>
      </div>
      <div class="options">
        <button class="btn btn-primary"
                (click)="$event.stopPropagation(); acknowledge();"
                ngbTooltip="Keep"
                placement="left"
                style="margin-bottom: 5px;">
          <i class="fas fa-check"></i>
        </button>
        <button class="btn btn-primary"
                (click)="$event.stopPropagation(); discard();"
                ngbTooltip="Discard"
                placement="left">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
  `,
  animations: [
    trigger("close",
      [
        state("in", style({opacity: 1})),
        transition("* => void", [
          animate(1000, style({opacity: 0}))
        ]),
      ]
    )
  ]
})
export class NotificationItemComponent {

  @Input() popup: boolean = false;
  @Input() notification: Notification;

  constructor(private notificationService: NotificationService) {}

  ngAfterViewInit() {
    if (this.popup) {
      interval(this.notificationService.popupRemoveDelay)
        .pipe(take(1))
        .subscribe(() => {
          this.notificationService.hide(this.notification);
        });
    }
  }

  acknowledge() {
    this.notificationService.acknowledge(this.notification);
  }

  discard() {
    this.notificationService.discard(this.notification);
  }
}
