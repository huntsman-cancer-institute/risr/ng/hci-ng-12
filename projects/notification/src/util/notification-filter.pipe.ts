import {Pipe, PipeTransform} from "@angular/core";

import {Notification} from "../model/notification.entity";

@Pipe({
  name: "notificationFilter",
  pure: false
})
export class NotificationFilterPipe implements PipeTransform {

  transform(list: Notification[], status: string, hasStatus?: boolean): Notification[] {
    return list.filter((o: Notification) => {
      if (status === "All") {
        return list;
      }
      if (hasStatus !== undefined) {
        if (hasStatus) {
          return o.status === status;
        } else {
          return o.status !== status;
        }
      } else {
        return o.status === status;
      }
    });
  }
}
