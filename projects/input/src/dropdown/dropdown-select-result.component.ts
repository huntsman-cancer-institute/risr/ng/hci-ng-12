import {Component, EventEmitter, Input, OnInit, Output, TemplateRef} from "@angular/core";

import {SelectItem} from "./select-item";

@Component({
  selector: "hci-dropdown-select-result",
  template: `
    <div class="select-results-container" *ngIf="items !== undefined && items.length > 0" id="resultsContainer">
      <div *ngFor="let item of items; let i = index;"
           class="select-result" id="item-{{i}}"
           (mousedown)="onSelected(item)" [class.selected]="isSelected(item)"
           [class.active]="i === activeIndex"
           (mouseover)="onMouseOver(i)"
           (mouseenter)="onHovering($event)">
        <ng-container [ngTemplateOutlet]="templateRef"
                      [ngTemplateOutletContext]="{$item: item, $entity: item.entity, $id: item.id, $index: i}"></ng-container>
        <ng-container *ngIf="!templateRef">{{item.text}}</ng-container>
      </div>
    </div>
  `
})
export class DropdownSelectResultComponent implements OnInit {

  @Input() items: SelectItem[];
  @Input() searchFocused: boolean;
  @Input() selectedItems: SelectItem[];
  @Input() templateRef: TemplateRef<any>;
  @Output() itemSelectedEvent: EventEmitter<any> = new EventEmitter();
  activeIndex = 0;
  private usingKeys = false;

  ngOnInit() {}

  onSelected(item: SelectItem) {
    this.itemSelectedEvent.emit(item);
  }

  onMouseOver(index: number) {
    if (!this.usingKeys) {
      this.activeIndex = index;
    }
  }

  onHovering(event) {
    this.usingKeys = false;
  }

  isSelected(currentItem) {
    let result = false;
    this.selectedItems.forEach(item => {
      if (item.id === currentItem.id) {
        result = true;
      }
    });
    return result;
  }

  activeNext() {
    if (this.activeIndex >= this.items.length - 1) {
      this.activeIndex = this.items.length - 1;
    } else {
      this.activeIndex++;
    }
    this.scrollToElement();
    this.usingKeys = true;
  }

  activePrevious() {
    if (this.activeIndex - 1 < 0) {
      this.activeIndex = 0;
    } else {
      this.activeIndex--;
    }
    this.scrollToElement();
    this.usingKeys = true;
  }

  scrollToElement() {
    const element = document.getElementById("item-" + this.activeIndex);
    const container = document.getElementById("resultsContainer");

    if (element) {
      container.scrollTop = element.offsetTop;
    }
  }

  selectCurrentItem() {
    if (this.items[this.activeIndex]) {
      this.onSelected(this.items[this.activeIndex]);
      this.activeIndex = 0;
    }
  }
}
