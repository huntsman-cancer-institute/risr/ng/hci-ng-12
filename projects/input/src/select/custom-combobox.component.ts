import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChange,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from "@angular/core";
import {AbstractControl, ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, NgControl} from "@angular/forms";
import {combineLatest, Subscription} from "rxjs";
import {debounceTime} from "rxjs/operators";
import {CdkVirtualScrollViewport} from "@angular/cdk/scrolling";
import {MatAutocompleteTrigger} from "@angular/material/autocomplete";
import {ListRange} from "@angular/cdk/collections";
import {ListKeyManager, Highlightable} from '@angular/cdk/a11y';
import {MatOption} from "@angular/material/core";
import {FloatLabelType} from "@angular/material/form-field/form-field";

@Component({
  selector: "hci-combobox",
  template: `
    <mat-form-field (click)="onClick($event)"
                    (mousedown)="startClick()"
                    (mouseup)="endClick()"
                    [appearance]="appearance"
                    [floatLabel]="floatLabel"
                    [matTooltip]="this.tooltip"
                    [ngClass]="customFieldClasses"
                    >
      <mat-label *ngIf="label !== undefined">{{ label }}</mat-label>
      <mat-label *ngIf="label === undefined">
        {{ temporaryPlaceholder ? (innerControl.value ? '' : placeholder) : placeholder }}
      </mat-label>
      <input #input
             matInput
             name="customComboBoxFilter"
             [placeholder]="temporaryPlaceholder ? (innerControl.value ? '' : placeholder) : placeholder"
             [matAutocomplete]="auto"
             autocomplete="off"
             [formControl]="innerControl"
             [attr.cdkFocusInitial]="cdkFocusInitial === undefined ? null : cdkFocusInitial"
             [tabindex]="tabindex"
             (keydown)="onKey($event)"
             (focus)="onFocus()"
             (focusout)="onFocusOut()" attr.aria-label="hci-ng-custom-combobox-{{label}}"
             >
      <div matSuffix
           class="hci-combobox-arrow-wrapper"
           (click) = "toggleOpen($event)"
           (mousedown)="startClick()"
           (mouseup)="endClick()"
           >
        <div class="hci-combobox-arrow"></div>
      </div>
      <mat-spinner matSuffix *ngIf="showLoader()" [diameter]="30" style="float: right; margin-left: 8px"></mat-spinner>
      <mat-autocomplete #auto="matAutocomplete"
                        autoActiveFirstOption
                        (optionSelected)="this.selectOption($event.option.value)"
                        (opened)="this.onOpened()"
                        (closed)="this.onClosed()"
                        [displayWith]="this.displayFn">
        <cdk-virtual-scroll-viewport #viewport
                                     [style.display]="isOpen ? null : 'none'"
                                     class="hci-combobox-viewport"
                                     [ngClass]= "customViewportClass"
                                     [itemSize]="optionSizePx"
                                     [style.height.px]="numOptionsToShow * optionSizePx"
                                     [minBufferPx]="numOptionsToShow * optionSizePx * 2"
                                     [maxBufferPx]="numOptionsToShow * optionSizePx * 3"
                                     (mousedown)="startClick()"
                                     (mouseup)="endClick()"
                                     >
          <mat-option *ngIf="allowNone && isOpen && (this.forceShowNone || !this.innerControl.value)"
                      [ngClass]="customOptionClasses"
                      [style.height.px]="optionSizePx"
                      >
            None
          </mat-option>
          <mat-option *cdkVirtualFor="let opt of this.loadedOptions"
                      [value]="opt"
                      [ngClass]="customOptionClasses"
                      [class.hci-combobox-selected]="(valueField ? opt[valueField] : opt) === outerControl.value"
                      [class.mat-selected]="(valueField ? opt[valueField] : opt) === outerControl.value"
                      [style.height.px]="optionSizePx"
                      >
            {{ displayField ? opt[displayField] : opt }}
          </mat-option>
        </cdk-virtual-scroll-viewport>
      </mat-autocomplete>
      <mat-error>
        {{ getError() }}
      </mat-error>
      <ng-content></ng-content>
    </mat-form-field>
  `,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CustomComboBoxComponent),
    multi: true,
  }],
  host: {
    class: "hci-combobox-container"
  },
  styles: [`
    .hci-combobox-container {
      display: inline-block;
    }

    .hci-combobox-arrow {
      width: 0;
      height: 0;
      border-left: 5px solid transparent;
      border-right: 5px solid transparent;
      border-top: 5px solid;
      margin: 0 4px;
      color: var(--black-darkest);
    }

    .hci-combobox-arrow-wrapper {
      display: table-cell;
      vertical-align: middle;
    }

    .hci-combobox-viewport mat-option {
      display: flex;
      align-items: center;
      padding: 0 .2rem;
    }

    .hci-combobox-viewport mat-option .mat-option-text {
      white-space: nowrap;
    }

    .hci-combobox-viewport .hci-combobox-selected {
      background-color: #dddddd;
    }

    .hci-combobox-viewport .mat-active {
      background-color: #f3f3f3;
    }

    /* fix jittery scroll bug in chrome */
    .hci-combobox-viewport {
      overflow-anchor: none;
    }
  `],
  encapsulation: ViewEncapsulation.None
})

export class CustomComboBoxComponent implements AfterViewInit, OnChanges, OnDestroy, ControlValueAccessor {
  public optionSizePx: number = 25;
  public numOptionsToShow: number = 10;

  @Input()
  public maxOptionsToShow: number = 10;

  @Input()
  public customViewportClass: string = "";

  @Input()
  public set customFieldClasses(value: string) {
    this._customFieldClasses = value ? value : "";
  }

  public get customFieldClasses(): string {
    return !!this._customFieldClasses ? this._customFieldClasses : "mat-form-field-should-float";
  }

  @Input()
  public set customOptionClasses(value: string) {
    this._customOptionClasses = value ? value : "";
  }

  public get customOptionClasses(): string {
    return !!this._customOptionClasses ? this._customOptionClasses : "";
  }

  // If a label is not specified the placeholder is promoted to a label.
  // To create a form field with only a placeholder, specify an empty
  // label to prevent the placeholder from being promoted.
  @Input() public label: string;
  @Input() public placeholder: string = "";

  // Removes the placeholder after a selection is made
  @Input() public temporaryPlaceholder: boolean = false;

  @Input() public tooltip: string = "";
  @Input() public allowNone: boolean = true;

  @Input() public selectTextOnOpen = true;

  // tell mat-dialog to pass initial focus to the inner input
  @HostBinding("attr.cdkFocusInitial") _cdkFocusInitial = null;
  @Input() public cdkFocusInitial: string = undefined;

  // on tab, skip the host element and focus the input
  @HostBinding("attr.tabindex") _tabindex = null;
  @Input() public tabindex: number = 0;

  public forceShowNone: boolean = false;

  @Input() public options: any[] = [];
  @Input() allowLoader: boolean = false;

  public isOpen: boolean = false;
  public loadedOptions: any[] = [];

  @Input() public valueField: string;
  @Input() private forceEmitObject: boolean = false;
  @Input() public displayField: string;
  @Input() public appearance: string = "";
  @Input() public floatLabel: FloatLabelType;

  // Set to empty to not show errors
  @Input()
  public defineErrors: any = {required: "This is a required field"};

  private _showLoader = false;
  private _customFieldClasses: string = "";
  private _customOptionClasses: string = "";

  public outerControl: AbstractControl = new FormControl();
  public innerControl: FormControl = new FormControl(null);
  private ignoreInnerControlChanges: boolean = false;
  private subs: Subscription = new Subscription();
  private noNgControl: boolean = false;
  public selectedIndex: number;

  keyManager: ListKeyManager<Highlightable>;
  viewportRenderRange: ListRange;
  viewportVisibleRange: ListRange = {
    start: 0,
    end: this.numOptionsToShow - 1
  };

  activeValue: any;

  public clickInProgress: boolean = false;

  @Output() optionSelected: EventEmitter<any> = new EventEmitter<any>();
  @Output() optionChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output() optionsLoaded: EventEmitter<any[]> = new EventEmitter<any[]>();

  @ViewChild("viewport", {static: true})
  viewport: CdkVirtualScrollViewport;

  @ViewChild("input", {static: true})
  inputElement: ElementRef;

  @ViewChild("input", {read: MatAutocompleteTrigger, static: false})
  autoCompleteTrigger: MatAutocompleteTrigger;

  private onChangeFn: (val: any) => void = () => {};
  private onTouchedFn: () => void = () => {};

  public displayFn: (opt?: any) => string | undefined = (opt?: any) => {
    return opt ? (this.displayField ? opt[this.displayField] : opt) : undefined;
  }

  constructor(private cdr: ChangeDetectorRef,
              private injector: Injector) { }

  ngOnInit(): void {
    this._showLoader = this.allowLoader && (! this.options || this.options.length === 0);
  }

  ngAfterViewInit(): void {
    let ngControl: NgControl = this.injector.get(NgControl, null);
    if (ngControl && ngControl.control) {
      this.outerControl = ngControl.control;
      this.innerControl.setValidators(this.outerControl.validator);
      this.innerControl.setAsyncValidators(this.outerControl.asyncValidator);
      this.innerControl.updateValueAndValidity();
      this.cdr.detectChanges();
    } else {
      this.noNgControl = true;
    }

    this.subs.add(this.innerControl.valueChanges.pipe(debounceTime(300)).subscribe((_value: any) => {
      if (this.isOpen && !this.ignoreInnerControlChanges) {
        this.filterOptions();
      } else {
        this.ignoreInnerControlChanges = false;
      }
    }));

    // work around for validators not always propagating
    this.subs.add(this.outerControl.statusChanges.subscribe((_event: any) => {
      this.innerControl.setValidators(this.outerControl.validator);
      this.innerControl.setAsyncValidators(this.outerControl.asyncValidator);
      this.innerControl.updateValueAndValidity();
      this.innerControl.setErrors(this.outerControl.errors);

      if (this.outerControl.touched) {
        this.innerControl.markAsTouched();
      }
    }));

    // this object is used to control the active state when keying up and down
    this.keyManager = this.autoCompleteTrigger.autocomplete._keyManager;
    this.keyManager.withWrap(false);

    this.subs.add(combineLatest([
      this.viewport.renderedRangeStream,
      this.viewport.scrolledIndexChange
    ]).subscribe(([renderedRange, scrolledIndex]: [ListRange, number]) => {

      // Instead of loading the whole item list at once, the virtual scroll viewport buffers
      // a range of entries, only a portion of which are within the visible fold
      this.viewportRenderRange = renderedRange;

      // we have to figure out the visible range ourselves
      let len: number = Math.min(this.loadedOptions.length + (this.allowNone ? 1 : 0), this.numOptionsToShow - 1);

      this.viewportVisibleRange = {
        start: scrolledIndex,
        end: scrolledIndex + len
      };
    }));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.options) {
      let optionsChange: SimpleChange = changes.options;
      if (!optionsChange.currentValue || optionsChange.currentValue.length < 1) {
        this.options = [];
        if(this.innerControl.value != null){
          //Reset the innerControl value of the dropdown list to an empty array in case of no results are present to be displayed.
          //This flushes out the stale value from the list if there are no results to be displayed.
          this.innerControl.setValue([]);
        }
      }

      let currentOpts: any[] = <any[]>optionsChange.currentValue;
      if (currentOpts && "length" in currentOpts && currentOpts !== optionsChange.previousValue) {
        this._showLoader = false;
        this.optionsLoaded.emit();

        // If number of options is less than our max, reduce size of overlay
        this.numOptionsToShow = Math.min(currentOpts.length + (this.allowNone ? 1 : 0), this.maxOptionsToShow);
      } else {
        this._showLoader = true;
      }
    }

    setTimeout(() => {
      // The autocomplete will automatically pop open once it has options filtered into it,
      // but we don't want it to on initial page load or when tabbing though
      if (this.isOpen) {
        this.filterOptions();
      } else {
        this.loadOnlyCurrentValue(this.outerControl.value);
      }
    });
  }

  writeValue(obj: any): void {
    this.loadOnlyCurrentValue(obj);
  }

  registerOnChange(fn: any): void {
    this.onChangeFn = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedFn = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.innerControl.disable();
    } else {
      this.innerControl.enable();
    }
  }

  getError(): string {
    for (const error in this.outerControl.errors) {
      if (this.defineErrors && error in this.defineErrors) {
        return this.defineErrors[error];
      }
    }
  }

  private loadOnlyCurrentValue(cntrlVal: any): void {
    if (!this.options || this.options.length < 1) {
      return;
    }

    let newValue: any = null;
    if (cntrlVal !== null && cntrlVal !== undefined) {
      let currentlySelected: any = this.options.find((opt: any) => {
        let optValue: any = this.valueField ? opt[this.valueField] : opt;
        if (this.forceEmitObject) {
          let outerValue: any = cntrlVal ? (this.valueField ? cntrlVal[this.valueField] : cntrlVal) : null;
          if (optValue === outerValue) {
            return true;
          }
        } else {
          return optValue === cntrlVal;
        }
      });
      if (currentlySelected) {
        newValue = currentlySelected;
      }
    }

    // prefer to check id if it has one  vs straight against it's object identity
    if (this.innerControl.value && newValue != null && this.valueField) {
      if (this.innerControl.value[this.valueField] !== newValue[this.valueField]) {
        this.ignoreInnerControlChanges = true;
        this.innerControl.setValue(newValue);
        this.optionSelected.emit(newValue[this.valueField]);
      }
    } else if (this.innerControl.value !== newValue) {
      this.ignoreInnerControlChanges = true;
      this.innerControl.setValue(newValue);
      let nv = newValue ? ((this.valueField && !this.forceEmitObject) ? newValue[this.valueField] : newValue) : null;
      this.optionSelected.emit(nv);
    }
  }

  private filterOptions(showAll: boolean = false): void {
    this.forceShowNone = showAll;
    this.isOpen = true;
    if (!this.options || this.options.length < 1) {
      return;
    }

    if (showAll || !this.innerControl.value) {
      this.loadedOptions = [...this.options];
    } else {
      let searchValue: string = "";
      if (typeof this.innerControl.value === "string") {
        searchValue = this.innerControl.value.toLowerCase();
      } else if (this.displayField) {
        searchValue = this.innerControl.value[this.displayField].toLowerCase();
      }
      this.loadedOptions = this.options.filter((opt: any) => {
        let optDisplay: string = (this.displayField ? opt[this.displayField] : opt).toLowerCase();
        return optDisplay.includes(searchValue);
      });
      this.forceShowNone = this.loadedOptions.length === 0;
    }

    this.selectedIndex = this.getOptIndex(this.outerControl.value);
  }

  public showLoader(): boolean {
    return this.allowLoader && this._showLoader;
  }

  public open() {
    if (this.innerControl.disabled) {
      return;
    }

    // The autocomplete will only open when options are loaded
    this.filterOptions(true);
    this.autoCompleteTrigger.openPanel();
  }

  public close() {
    this.autoCompleteTrigger.closePanel();
  }

  public onOpened(): void {
    this.forceShowNone = true;
    if (this.isOpen) {
      return;
    }
    this.isOpen = true;

    if(this.selectTextOnOpen){
      this.inputElement.nativeElement.select(); // Highlights text
    }

    if (this.selectedIndex > 0) {
      // make the selected option also the active option
      this.activeValue = this.getOptVal(this.loadedOptions[this.selectedIndex]);

      setTimeout(() => {
        this.scrollToSelectedOption();
      });
    }
  }

  public onClosed(): void {
    if (!this.isOpen) {
      return;
    }

    this.isOpen = false;

    if (!this.innerControl.value && this.outerControl.value) {
      this.selectOption(null);
    }

    this.loadOnlyCurrentValue(this.outerControl.value);

    // reset the options so the autocomplete won't automatically open on the next tab through
    this.loadedOptions = [];
  }

  public startClick() {
    // clicking on certain parts of the mat-field will "flicker" the focus, triggering
    // error validation early. This tracks those internal clicks so they can be ignored
    this.clickInProgress = true;
  }

  public endClick() {
    this.clickInProgress = false;
  }

  public onClick(event: MouseEvent): void {
    event.stopPropagation();
    if (!this.isOpen) {
      this.open();
    }
  }

  public toggleOpen(event: MouseEvent):void {
    event.stopPropagation();

    if (this.isOpen) {
      this.close();
    } else {
      this.open();
    }
  }

  public onKey(event: KeyboardEvent): void {
    if (event.key === "Tab" || event.key === "Enter") {
      return;
    } else {
      if (!this.isOpen) {
        this.open();
      }

      // There is a bug where the cdk a11y KeyManager implementation is not compatible with
      // the MatAutocompleteTrigger, which causes the Drop-down not scroll with keyboard
      // on long option lists.
      //
      // https://github.com/angular/components/issues/16598
      //
      // This work around takes control of the keyboard and manually scrolls and activates
      // the options
      //
      // References:
      //   https://github.com/angular/components/blob/master/src/material/autocomplete/autocomplete-trigger.ts
      //   https://github.com/angular/components/blob/master/src/cdk/a11y/key-manager/list-key-manager.ts
      //   https://github.com/angular/components/blob/master/src/cdk/a11y/key-manager/activedescendant-key-manager.ts
      if (event.key === "ArrowUp" || event.key === "ArrowDown") {
        event.stopPropagation();

        this.initActiveItem();
        this.shiftActiveItem(event.key);
        this.checkActiveBoundaries();
      }
    }
  }

  // before our keypress binding even triggers, angular has already moved the active selection
  // unsyncing it with our display. this code re-initializes the active option
  initActiveItem(): void {
    if (this.activeValue === undefined || this.activeValue === null) {
      this.keyManager.setFirstItemActive();
    } else {
      let activeIndex: number = this.getActiveIndex();

      // this does the conversion from an index within all options down to a position within the
      // displayed range, which is all the keymanager is aware of
      let position = activeIndex - this.viewportRenderRange.start;
      this.keyManager.setActiveItem(position);
    }
  }

  shiftActiveItem(key: string): void {
    if (key === "ArrowUp") {
      this.keyManager.setPreviousItemActive();
    } else if (key === "ArrowDown") {
      this.keyManager.setNextItemActive();
    }

    if (this.keyManager.activeItem) {
      this.activeValue = this.getOptVal((<MatOption>this.keyManager.activeItem).value);
    }
  }

  // check if the active option index is within the displayed viewport window
  // if not, scroll it into view
  checkActiveBoundaries(): void {
    let activeIndex: number = this.getActiveIndex();

    if (activeIndex < this.viewportVisibleRange.start) {
      setTimeout(() => {
        this.viewport.scrollToOffset(activeIndex * this.optionSizePx);
      });

    } else if (activeIndex > this.viewportVisibleRange.end) {
      setTimeout(() => {
        this.viewport.scrollToOffset((activeIndex - (this.numOptionsToShow - 1)) * this.optionSizePx);
      });
    }
  }

  getActiveIndex() {
    let activeIndex: number = 0;

    if (this.activeValue !== undefined && this.activeValue !== null) {

      // if we added a none option the returned index would be off by one
      let valIndex = this.getOptIndex(this.activeValue) + (this.allowNone && (this.forceShowNone || !this.innerControl.value) ? 1 : 0);

      // the index should be within the range of our entire buffer, or just allow it to reset
      if (valIndex >= this.viewportRenderRange.start
        && (!this.viewportRenderRange.end || valIndex <= this.viewportRenderRange.end)
      ) {
        activeIndex = valIndex;
      }
    }

    return activeIndex;
  }

  public onFocus() {
    if (!this.isOpen) {
      // Highlights text
      if (this.selectTextOnOpen) {
        this.inputElement.nativeElement.select();
      }
    }
  }

  public onFocusOut() {
    // mark control touched and trigger error validation
    if (!this.clickInProgress) {
      this.close();
      this.onTouchedFn();
    }
  }

  public selectOption(opt: any): void {
    let newVal = this.getOptVal(opt, this.forceEmitObject);

    if (this.noNgControl) {
      this.outerControl.setValue(newVal);
    }

    this.onChangeFn(newVal);
    this.optionSelected.emit(newVal);
    this.optionChanged.emit(newVal);
  }

  public scrollToSelectedOption(): void {
    // scroll to the currently selected option when we open the dropdown
    if (this.selectedIndex > 0) {
      let offsetIndex: number = this.selectedIndex + (this.allowNone ? 1 : 0); // adjust for None entry
      offsetIndex -= (this.numOptionsToShow / 2) - 1; // calculate middle of viewport
      this.viewport.scrollToOffset(offsetIndex * this.optionSizePx);
    }
  }

  getOptVal(opt: any, forceObject: boolean = false): any {
    if (this.valueField && !forceObject) {
      if (opt !== null && opt !== undefined && typeof opt === "object" && this.valueField in opt) {
        return opt[this.valueField];
      } else {
        return undefined;
      }
    }
    return opt;
  }

  getOptIndex(optValue: any): number {
    // find the index for the currently selected option
    if (optValue !== null && optValue !== undefined) {
      return this.loadedOptions.findIndex((option: any) => {
        return this.getOptVal(option) === optValue;
      });
    }
    return undefined;
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
