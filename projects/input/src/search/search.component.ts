import {
  ChangeDetectorRef, Component, EventEmitter, Input, isDevMode, Output, SimpleChange,
  SimpleChanges
} from "@angular/core";

/**
 * I'm just screwing around with some ideas here, beware of actually using any of this code.
 */
@Component({
  selector: "hci-search",
  template: `
    <div class="input-group ">
      <input #searchBox type="text"
             (keyup.enter)="doSearch(searchBox.value)"
             class="form-control"
             placeholder="Search for..."
             aria-label="Search for text">
      <div class="input-group-append">
        <button class="btn btn-outline-secondary" type="button" (click)="doSearch(searchBox.value)">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  `
})
export class SearchComponent {

  CHILD_NODE: string = "children";

  search: string;
  @Input() isArray: boolean;
  @Input() isIncludeChildren: boolean;

  @Input() anyComponent: any;
  @Input() input: string;
  @Input() data: any[];

  @Output() dataFiltered: EventEmitter<any[]> = new EventEmitter<any[]>();

  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  ngAfterViewInit() {
    if (isDevMode()) {
      console.debug("SearchComponent.ngAfterViewInit");
    }
    setTimeout(() => {
      if (this.anyComponent && this.data && this.input) {
        let changes = {};
        changes[this.input] = new SimpleChange([], this.data, false);
        this.anyComponent.ngOnChanges(changes);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (isDevMode()) {
      console.debug("SearchComponent.ngOnChanges");
    }
    if (changes.data && this.isArray === undefined) {
      for (let item of this.data) {
        if (item[this.CHILD_NODE]) {
          this.isArray = false;
          return;
        }
      }
    }
  }

  /**
   * A tree or a grid?  Who knows.
   *
   * @param {KeyboardEvent} event
   */
  doSearch(searchBox: string) {
    if (isDevMode()) {
      console.debug("doSearch");
    }

    this.search = searchBox;
    let filteredData = null;
    if (this.isArray) {
      console.debug("array option");
      filteredData = this.filterArray(this.data);
    } else if(this.isIncludeChildren) {
      console.debug("child option");
      filteredData = this.filterTreeIncludeChildren(JSON.parse(JSON.stringify(this.data)));
    } else {
      console.debug("not child option");
      filteredData = this.filterTree(JSON.parse(JSON.stringify(this.data)));
    }

    // This is what triggers the @Output
    this.dataFiltered.emit(filteredData);

    if (this.anyComponent) {
      let changes = {};
      changes[this.input] = new SimpleChange(this.anyComponent[this.input], filteredData, false);
      this.anyComponent.ngOnChanges(changes);
    }

    this.changeDetectorRef.detectChanges();
  }

  filterArray(data: any[]): any[] {
    if (isDevMode()) {
      console.debug("filterArray");
    }
    return data.filter((o: any) => {
      return (o["name"].indexOf(this.search) >= 0);
    });
  }

  /**
   * Recursive tree search.  It will include parent nodes if a leaf node matches the search string.
   *
   * @param {any[]} data
   * @returns {any[]}
   */
  filterTree(data: any[]): any[] {
    if (isDevMode()) {
      console.debug("filterTreeOrigional");
    }
    let filteredData: any[] = [];
    for (let child of data) {
      let pushed: boolean = false;
      if (child["children"]) {
        child["children"] = this.filterTree(child["children"]);
        if (child["children"].length > 0) {
          pushed = true;
          filteredData.push(child);
        }
      }
      if (!pushed && child["name"].indexOf(this.search) >= 0) {
        filteredData.push(child);
      }
    }
    return filteredData;
  }

  /**
   * Recursive tree search.  It will include children nodes if a parent node matches the search string.
   *
   * @param {any[]} data
   * @returns {any[]}
   */
  filterTreeIncludeChildren(data: any[]): any[] {
    if (isDevMode()) {
      console.debug("filterTreeIncludeChildren");
    }
    let filteredData: any[] = [];
    for (let child of data) {
      if (child["name"].indexOf(this.search) >= 0) {
        filteredData.push(child);
      }else {
        let pushed: boolean = false;
        if (child["children"]) {
          child["children"] = this.filterTreeIncludeChildren(child["children"]);
          if (child["children"].length > 0) {
            pushed = true;
            filteredData.push(child);
          }
        }
        if (!pushed && child["name"].indexOf(this.search) >= 0) {
          filteredData.push(child);
        }
      }
    }
    return filteredData;
  }
}
