/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {TestBed, ComponentFixture} from "@angular/core/testing";

import {} from "jasmine";

import {HelpComponent} from "./help.component";
import {HELP_BASE_ENDPOINT, HELP_CACHE_SIZE, TOOLTIP_BASE_ENDPOINT} from "./help.service";

/**
 * @since 1.0.0
 */
let fixture: ComponentFixture<HelpComponent>;
describe("HelpComponent Tests", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [HelpComponent],
      providers: [
        {provide: HELP_CACHE_SIZE, useValue: 40},
        {provide: TOOLTIP_BASE_ENDPOINT, useValue: "https://localhost:8080/api/tooltip"},
        {provide: HELP_BASE_ENDPOINT, useValue: "https://localhost:8080/api/help"}
      ]
    });

    fixture = TestBed.createComponent(HelpComponent);
  });
});
