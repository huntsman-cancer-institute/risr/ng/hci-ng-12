import {async, TestBed, fakeAsync, inject} from "@angular/core/testing";
import {HttpClient} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

import {Observable} from "rxjs";

import {} from "jasmine";

import {HelpService} from "./help.service";
import {HelpEntity} from "./help.entity";
import {ApplicationContextEnum} from "./application-context.enum";

/**
 * @since 1.0.0
 */
describe("HelpService Tests", () => {
  const id: number = 42;
  const appCtxt: string = ApplicationContextEnum[0];
  const toolTipEndpoint: string = "https://localhost:8080/core/api/tooltips";
  const helpDetailEndpoint: string = "https://localhost:8080/core/api/helps";
  const tooltip: HelpEntity = new HelpEntity("my tooltip");
  const modalHelp: HelpEntity = new HelpEntity(null, "my title", "my body");

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
  });

  it("Should lookup a tooltip from the server when not cached.", async(
    inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {
      let tooltipResourceUrl: string = toolTipEndpoint + "/" + appCtxt + "/" + id;

      let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

      helpService.lookupTooltip(id, appCtxt).subscribe((resp: string) => {
        expect(resp).toBe(tooltip.Tooltip);
      });

      let request = backend.expectOne({url: tooltipResourceUrl, method: "GET"});
      request.flush(tooltip);
    })
  ));

/* TODO Figure out error
  it("Should return a default error when a server error is returned without a message from lookupTooltip.", async(
    inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {
      let tooltipResourceUrl: string = toolTipEndpoint + "/" + appCtxt + "/" + id;
      let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

      helpService.lookupTooltip(id, appCtxt).subscribe((resp: string) => {
        fail("Expected Error");
      }, ((error: any) => {
        expect(error).toBe(HelpService.GENERIC_ERR_MSG);
      }));

      let request = backend.expectOne(tooltipResourceUrl).error(new ErrorEvent("Expected Error"));
    })
  ));*/

  /*
    it("Should return error message when a server error is returned with a message from lookupTooltip.",
      inject([AuthHttp, MockBackend], fakeAsync((http: AuthHttp, mockBackend: MockBackend) => {
        let errMsg: string = "Bad Stuff";
        mockBackend.connections.subscribe((conn: MockConnection) => {
          conn.mockError(new Error(errMsg));
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.lookupTooltip(id, appCtxt).subscribe((resp: string) => {
          fail("Expected Error");
        }, ((error: any) => {
          expect(error).toBe(errMsg);
        }));
      })));

    it(
      "Should return a default error with status information when an unexpected successful response is returned from the server from lookupTooltip.",
      inject([AuthHttp], fakeAsync((http: AuthHttp) => {
        let tooltipResourceUrl: string = toolTipEndpoint + "/" + appCtxt + "/" + id;
        let status: number = 204;
        let respOpts: ResponseOptions = new ResponseOptions({
          status: status
        });
        spyOn(http, "get").and.callFake((params: any) => {
          if (params === tooltipResourceUrl) {
            return of(new Response(respOpts));
          }
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.lookupTooltip(id, appCtxt).subscribe((resp: string) => {
          fail("Expected 204");
        }, ((error: any) => {
          expect(error).toBe(HelpService.INVALID_ERR_MSG + status);
        }));
      })));

    it("Should lookup a tooltip from the cache when it exists.", inject([AuthHttp], fakeAsync((http: AuthHttp) => {
      let tooltipResourceUrl: string = toolTipEndpoint + "/" + appCtxt + "/" + id;
      let respOpts: ResponseOptions = new ResponseOptions({
        body: JSON.stringify(tooltip),
        status: 200
      });
      let getTooltipSpy = spyOn(http, "get").and.callFake((params: any) => {
        if (params === tooltipResourceUrl) {
          return of(new Response(respOpts));
        }
      });

      let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

      helpService.lookupTooltip(id, appCtxt).subscribe((resp: string) => {
        let tooltip: string = resp;

        helpService.lookupTooltip(id, appCtxt).subscribe((cacheResp: string) => {
          expect(getTooltipSpy).toHaveBeenCalledTimes(1);
          expect(cacheResp).toBe(tooltip);
        });
      });
    })));

    it("Should return a default error when a server error is returned without a message from lookupHelp.",
      inject([AuthHttp, MockBackend], fakeAsync((http: AuthHttp, mockBackend: MockBackend) => {
        mockBackend.connections.subscribe((conn: MockConnection) => {
          conn.mockError(new Error());
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.lookupHelp(id, appCtxt).subscribe((resp: any) => {
          fail("Expected Error");
        }, ((error: any) => {
          expect(error).toBe(HelpService.GENERIC_ERR_MSG);
        }));
      })));

    it("Should return error message when a server error is returned with a message from lookupHelp.",
      inject([AuthHttp, MockBackend], fakeAsync((http: AuthHttp, mockBackend: MockBackend) => {
        let errMsg: string = "Bad Stuff";
        mockBackend.connections.subscribe((conn: MockConnection) => {
          conn.mockError(new Error(errMsg));
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.lookupHelp(id, appCtxt).subscribe((resp: any) => {
          fail("Expected Error");
        }, ((error: any) => {
          expect(error).toBe(errMsg);
        }));
      })));

    it(
      "Should return a default error with status information when an unexpected successful response is returned from the server from lookupHelp.",
      inject([AuthHttp], fakeAsync((http: AuthHttp) => {
        let tooltipResourceUrl: string = helpDetailEndpoint + "/" + appCtxt + "/" + id;
        let status: number = 204;
        let respOpts: ResponseOptions = new ResponseOptions({
          status: status
        });
        spyOn(http, "get").and.callFake((params: any) => {
          if (params === tooltipResourceUrl) {
            return of(new Response(respOpts));
          }
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.lookupHelp(id, appCtxt).subscribe((resp: any) => {
          fail("Expected 204");
        }, ((error: any) => {
          expect(error).toBe(HelpService.INVALID_ERR_MSG + status);
        }));
      })));

    it("Should lookup help detail from the server when not cached.", inject([AuthHttp], fakeAsync((http: AuthHttp) => {
      let helpResourceUrl: string = helpDetailEndpoint + "/" + appCtxt + "/" + id;
      let respOpts: ResponseOptions = new ResponseOptions({
        body: JSON.stringify(modalHelp),
        status: 200
      });
      let getHelpSpy = spyOn(http, "get").and.callFake((params: any) => {
        if (params === helpResourceUrl) {
          return of(new Response(respOpts));
        }
      });

      let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

      helpService.lookupHelp(id, appCtxt).subscribe((resp: any) => {
        expect(resp.title).toBe(modalHelp.Title);
        expect(resp.body).toBe(modalHelp.Body);
        expect(getHelpSpy).toHaveBeenCalled();
      });
    })));

    it("Should lookup help detail from the cache when it exists.", inject([AuthHttp], fakeAsync((http: AuthHttp) => {
      let helpResourceUrl: string = helpDetailEndpoint + "/" + appCtxt + "/" + id;
      let respOpts: ResponseOptions = new ResponseOptions({
        body: JSON.stringify(modalHelp),
        status: 200
      });
      let getHelpSpy = spyOn(http, "get").and.callFake((params: any) => {
        if (params === helpResourceUrl) {
          return of(new Response(respOpts));
        }
      });

      let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

      helpService.lookupHelp(id, appCtxt).subscribe((resp: any) => {
        let helpDetail: any = resp;

        helpService.lookupHelp(id, appCtxt).subscribe((cacheResp: any) => {
          expect(getHelpSpy).toHaveBeenCalledTimes(1);
          expect(cacheResp.Title).toBe(helpDetail.title);
          expect(cacheResp.Body).toBe(cacheResp.Body);
        });
      });
    })));

    it("Should return a default error when a server error is returned without a message from submitTooltipUpdate.",
      inject([AuthHttp, MockBackend], fakeAsync((http: AuthHttp, mockBackend: MockBackend) => {
        mockBackend.connections.subscribe((conn: MockConnection) => {
          conn.mockError(new Error());
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.submitTooltipUpdate(id, appCtxt, tooltip).subscribe((resp: boolean) => {
          fail("Expected Error");
        }, ((error: any) => {
          expect(error).toBe(HelpService.GENERIC_ERR_MSG);
        }));
      })));

    it("Should return error message when a server error is returned with a message from submitTooltipUpdate.",
      inject([AuthHttp, MockBackend], fakeAsync((http: AuthHttp, mockBackend: MockBackend) => {
        let errMsg: string = "Bad Stuff";
        mockBackend.connections.subscribe((conn: MockConnection) => {
          conn.mockError(new Error(errMsg));
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.submitTooltipUpdate(id, appCtxt, tooltip).subscribe((resp: boolean) => {
          fail("Expected Error");
        }, ((error: any) => {
          expect(error).toBe(errMsg);
        }));
      })));

    it(
      "Should return a default error with status information when an unexpected successful response is returned from the server from submitTooltipUpdate.",
      inject([AuthHttp], fakeAsync((http: AuthHttp) => {
        let tooltipResourceUrl: string = toolTipEndpoint + "/" + appCtxt + "/" + id;
        let status: number = 200;
        let respOpts: ResponseOptions = new ResponseOptions({
          status: status
        });
        spyOn(http, "put").and.callFake((params: any) => {
          if (params === tooltipResourceUrl) {
            return of(new Response(respOpts));
          }
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.submitTooltipUpdate(id, appCtxt, tooltip).subscribe((resp: boolean) => {
          fail("Expected 200");
        }, ((error: any) => {
          expect(error).toBe(HelpService.INVALID_ERR_MSG + status);
        }));
      })));

    it("Should submit a tooltip update when a valid id and application context is provided.",
      inject([AuthHttp], fakeAsync((http: AuthHttp) => {
        let tooltipResourceUrl: string = toolTipEndpoint + "/" + appCtxt + "/" + id;
        let respOpts: ResponseOptions = new ResponseOptions({
          status: 204
        });
        let putHelpSpy = spyOn(http, "put").and.callFake((params: any) => {
          if (params === tooltipResourceUrl) {
            return of(new Response(respOpts));
          }
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.submitTooltipUpdate(id, appCtxt, tooltip).subscribe((resp: boolean) => {
          expect(resp).toBeTruthy();
          expect(putHelpSpy).toHaveBeenCalledTimes(1);

          helpService.lookupTooltip(id, appCtxt).subscribe((tooltipResp: string) => {
            expect(tooltipResp).toBe(tooltip.Tooltip);
          });
        });
      })));

    it("Should return a default error when a server error is returned without a message from submitHelpUpdate.",
      inject([AuthHttp, MockBackend], fakeAsync((http: AuthHttp, mockBackend: MockBackend) => {
        mockBackend.connections.subscribe((conn: MockConnection) => {
          conn.mockError(new Error());
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.submitHelpUpdate(id, appCtxt, modalHelp).subscribe((resp: boolean) => {
          fail("Expected Error");
        }, ((error: any) => {
          expect(error).toBe(HelpService.GENERIC_ERR_MSG);
        }));
      })));

    it("Should return error message when a server error is returned with a message from submitHelpUpdate.",
      inject([AuthHttp, MockBackend], fakeAsync((http: AuthHttp, mockBackend: MockBackend) => {
        let errMsg: string = "Bad Stuff";
        mockBackend.connections.subscribe((conn: MockConnection) => {
          conn.mockError(new Error(errMsg));
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.submitHelpUpdate(id, appCtxt, modalHelp).subscribe((resp: boolean) => {
          fail("Expected Error");
        }, ((error: any) => {
          expect(error).toBe(errMsg);
        }));
      })));

    it(
      "Should return a default error with status information when an unexpected response is returned from the server from submitHelp.",
      inject([AuthHttp], fakeAsync((http: AuthHttp) => {
        let tooltipResourceUrl: string = helpDetailEndpoint + "/" + appCtxt + "/" + id;
        let status: number = 200;
        let respOpts: ResponseOptions = new ResponseOptions({
          status: status
        });
        spyOn(http, "put").and.callFake((params: any) => {
          if (params === tooltipResourceUrl) {
            return of(new Response(respOpts));
          }
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.submitHelpUpdate(id, appCtxt, modalHelp).subscribe((resp: boolean) => {
          fail("Expected 200");
        }, ((error: any) => {
          expect(error).toBe(HelpService.INVALID_ERR_MSG + status);
        }));
      })));

    it("Should submit a help update when a valid id and application context is provided.",
      inject([AuthHttp], fakeAsync((http: AuthHttp) => {
        let helpResourceUrl: string = helpDetailEndpoint + "/" + appCtxt + "/" + id;
        let respOpts: ResponseOptions = new ResponseOptions({
          status: 204
        });
        let putHelpSpy = spyOn(http, "put").and.callFake((params: any) => {
          if (params === helpResourceUrl) {
            return of(new Response(respOpts));
          }
        });

        let helpService: HelpService = new HelpService(http, toolTipEndpoint, helpDetailEndpoint);

        helpService.submitHelpUpdate(id, appCtxt, tooltip).subscribe((resp: boolean) => {
          expect(resp).toBeTruthy();
          expect(putHelpSpy).toHaveBeenCalledTimes(1);

          helpService.lookupHelp(id, appCtxt).subscribe((helpResp: any) => {
            expect(helpResp.title).toBe(tooltip.Title);
            expect(helpResp.body).toBe(helpResp.Body);
          });
        });
      })));
      */
});
