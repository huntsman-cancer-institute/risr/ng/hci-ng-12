/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */

/**
 * @since 1.0.0
 */
// TODO: BHY (10/16/16) - Consider moving this to a more generic feature module as it may be more generally applicable.
export enum ApplicationContextEnum {
  GNOMEX,
  // TODO: BHY (10/16/16) - Add more application contexts as they are implemented
}
