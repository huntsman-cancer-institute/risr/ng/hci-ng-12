/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, ViewChild, Input, OnInit, AfterViewInit} from "@angular/core";
import {NgbModal, NgbModalRef, NgbTooltip} from "@ng-bootstrap/ng-bootstrap";
import {HelpService} from "./help.service";
import {AuthenticationService} from "@huntsman-cancer-institute/authentication";

/**
 * A help component providing a help tooltip on hover and help dialog when clicked. This information can be provided
 * statically by the application by setting the attributes 'staticTooltip' and/or 'staticHelp' or defining the
 * appropriate tooltip and help ids and a valid application context (see {@link HelpComponent#getApplicationContexts}
 * and {@link ApplicationContextEnum}).
 *
 * @since 1.0.0
 */
@Component({
  selector: "hci-help",
  exportAs: "hciHelp",
  template: `
    <div class="hci-help-container" [ngbTooltip]="helpTooltip">
      <span>
        <i class="fas fa-lg fa-question-circle" (click)="showHelp()"></i>
      </span>
    </div>
    <ng-template #helpTooltip>
      <div *ngIf="_tooltipContent && !_isRetrievingTooltip" [innerHTML]="_tooltipContent"></div>
      <span *ngIf="!staticTooltip && _isRetrievingTooltip">
        <i class="fas fa-spinner fa-pulse fa-fw"></i>
      </span>
    </ng-template>
    
    <ng-template #helpModal class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div *ngIf ="_modalDetail && _modalDetail.title" class="modal-header">
        <button type="button" class="close" (click)="closeModal()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 *ngIf="_modalDetail" [innerHTML] = "_modalDetail.title"></h4>
      </div>
      <div class="modal-body">
        <div *ngIf="_modalDetail && !_modalDetail.body">
          <i class="fas fa-spinner fa-pulse fa-fw fa-4x hci-loading-help"></i>
        </div>
        <div *ngIf="_modalDetail" [innerHTML] = "_modalDetail.body"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" (click)="closeModal()">Close</button>
          <ng-template [ngIf]="_isAuthenticated">
            <ng-template [hciHasRole]="editRole">
              <button *ngIf="!_isEditing && !_hasError" type="button" class="btn btn-primary" (click)="editHelp()">Edit</button>
            </ng-template>
          </ng-template>
        <button *ngIf="_isEditing" type="submit" class="btn btn-success">Submit</button>
      </div>
    </ng-template>
  `,
  styles: [`
    .hci-help-container {
      display: inline-block;
    }
    
    .hci-loading-help {
      margin: auto;
      width: 100%;
    }
  `]
})
export class HelpComponent implements AfterViewInit {

  public static readonly DEFAULT_HELP_MODAL_TITLE = "Help Information";

  /**
   * A general id attribute that will be applied for both help tooltip and dialog resources if set, if this detail needs
   * to be retrieved from the server.
   */
  @Input("id") public id: number;

  /**
   * An attribute for the id of the help tooltip resource, if this detail needs to be retrieved from the server.
   */
  @Input("tooltipId") public tooltipId: number;

  /**
   * An attribute that directs tooltip placement, where the default is 'top'. Other options are 'bottom', 'left' and 'right'.
   */
  @Input("tooltipPlacement") public tooltipPlacement: "top" | "bottom" | "left" | "right" = "top";

  /**
   * An attribute defining this help components application context, if the tooltip and/or dialog needs to be retrieved
   * from the server.
   */
  @Input("applicationContext") public applicationContext: number;

  /**
   * An attribute defining the static text, which can include markup, to be provided for this help components tooltip.
   * If this is set the tooltip will not be retrieved from the server.
   */
  @Input("staticTooltip") public staticTooltip: string;

  /**
   * An attribute to set the role necessary to edit the help information provided by this component. This attribute is only
   * valid for dynamic content (retrieved from server resources).
   */
  @Input("helpEditRole") public editRole: string;

  /**
   * An attribute for the id of the help modal resource, if this detail needs to be retrieved from the server.
   */
  @Input("modalId") public modalId: number;

  /**
   * An attribute defining the static text, which can include markup, to be provided for this help components modal dialog.
   * If this is set the modal detail will not be retrieved from the server.
   */
  @Input("staticHelp") public staticHelp: string;

  @Input("useDefaultTitle") public useDefaultTitle: boolean;

  @ViewChild("helpModal", {static: false}) private _helpModalDirective: NgbModalRef;
  private _helpModalDirectiveRef: NgbModalRef;
  @ViewChild("helpTooltip", {static: false}) private _helpTooltipDirective: NgbTooltip;

 _isEditing: boolean;
 _hasError: boolean;
 _modalDetail: any;
 _isAuthenticated: boolean = false;
 _tooltipInFocus: boolean;
 _isRetrievingTooltip: boolean;
 _tooltipContent: string;

  constructor(private _helpService: HelpService, private _authenticationService: AuthenticationService, private modalService: NgbModal) {}

  /**
   * Validates the component configuration, sets up the ids for the tooltip and modal dialog, establishes the correct
   * beginning state (or potentially, the absolute state if all values are static) for both the tooltip and modal help.
   */
  public ngAfterViewInit(): void {
    this._authenticationService.isAuthenticated().subscribe((isAuthenticated: boolean) => {
      this._isAuthenticated = isAuthenticated;
    });
    this.validateConfiguration();
    this.setupIds();
    this.setupTooltip();
    this.setupModal();
  }

  /**
   * An accessor for an enumeration of valid application contexts.
   *
   * @returns {ApplicationContextEnum} defining all valid contexts
   */
  public get applicationContexts(): any {
    return this._helpService.applicationContexts;
  }

  /**
   * The function registered to handle state change events on the help tool tip.
   * <p>
   * Based on the hover state and the state of the component the tooltip will be displayed and the tooltip detail will
   * be retrieved from the server, if necessary.
   *
   * @param state of the user hover on the help tooltip
   */
  public tooltipStateChanged(state: boolean): void {
    if (state && !this.staticTooltip && !this._isRetrievingTooltip) {
      this._tooltipInFocus = true;
      this._isRetrievingTooltip = true;

      this._helpService.lookupTooltip(this.tooltipId, this._helpService.applicationContexts[this.applicationContext])
        .subscribe((tooltip: string) => {
          this._tooltipContent = tooltip;

          if (this._tooltipInFocus) {
            this._helpModalDirectiveRef = this.modalService.open(this._helpModalDirective);
          }

          this._isRetrievingTooltip = false;
        }, (error: any) => {
          // TODO: BHY (10/20/16) - Consider communicating an error in tooltip. However, I think leaving the spinner animation works.
          this._isRetrievingTooltip = false;
        });
    }
  }

  /**
   * A function to set the appropriate component state when the user's mouse leaves the help icon.
   */
  public onLeaveTooltip(): void {
    this._tooltipInFocus = false;
  }

  /**
   * A function to display the help modal dialog and hide the corresponding tooltip, retrieving it from the server if
   * necessary.
   */
  public showHelp(): void {
    this._helpModalDirectiveRef = this.modalService.open(this._helpModalDirective);

    if (!this._modalDetail || this._hasError) {
      this._modalDetail = {
        title: "Loading Help..."
      };
      this._helpService.lookupHelp(this.modalId, this._helpService.applicationContexts[this.applicationContext])
        .subscribe((helpDetail: any) => {
          this._hasError = false;
          this._modalDetail = helpDetail;

          if (!this._modalDetail.title && this.useDefaultTitle) {
            this._modalDetail.title = HelpComponent.DEFAULT_HELP_MODAL_TITLE;
          }
        }, (error: any) => {
          this._hasError = true;

          this._modalDetail = {
            title: "Error",
            body: error
          };
        });
    }
  }

  /**
   * A method to close the help modal dialog and reset this components help modal internal state.
   */
  public closeModal(): void {
    this._isEditing = false;
    this._helpModalDirectiveRef.close(null);
  }

  /**
   * A method to submit the modal dialog form.
   */
  public editHelp(): void {
    this._isEditing = true;
    // TODO: BHY (10/17/16) - Implement edit.
  }

  /**
   * A utility function to verify that the state of this component is valid.
   * <p>
   * A valid HelpComponent configuration will require that either static help and/or tooltip detail (staticTooltip and
   * staticHelp attributes), or that an id and application context is configured to retrieve this information from the server.
   * <p>
   * If the tooltip and help ids are the same only the id attribute needs to be configured, otherwise both the tooltipId
   * and modalId attributes need to be configured independently.
   */
  private validateConfiguration(): void {
    if (!this.staticTooltip && ((this.id < 0 && this.tooltipId < 0) || this.applicationContext < 0)) {
      throw new Error("BUG ALERT! Invalid HelpComponent configuration. If [staticTooltip] is not set, then " +
        "[id] or [tooltipId] and [applicationContext] must be configured.");
    }

    if (!this.staticHelp && ((this.id < 0 && this.modalId < 0) || this.applicationContext < 0)) {
      throw new Error("BUG ALERT! Invalid HelpComponent configuration. If [staticHelp] is not set, then " +
        "[id] or [modalId] and [applicationContext] must be configured.");
    }
  }

  /**
   * A utility function to setup the ids, given the various configurations.
   */
  private setupIds(): void {
    if (this.id) {
      this.tooltipId = this.id;
      this.modalId = this.id;
    }
  }

  /**
   * A utility function to setup the help tooltip with a 'wait for async response' template or the static help tooltip and
   * configure the tooltip placement, if set.
   * <p>
   * The capability to set a custom tooltip class was intentionally excluded to allow the CORE team to consider the
   * consistency of the CORE branded styling before allowing such customization to application engineers.
   */
  private setupTooltip(): void {
    if (this.tooltipPlacement) {
      this._helpTooltipDirective.placement = this.tooltipPlacement;
    }

    if (this.staticTooltip) {
      this._tooltipContent = this.staticTooltip;
    } else {
      this._helpService.lookupTooltip(this.tooltipId, this._helpService.applicationContexts[this.applicationContext])
        .subscribe((tooltip: string) => {
          this._tooltipContent = tooltip;

          this._isRetrievingTooltip = false;
        }, (error: any) => {
          // TODO: BHY (10/20/16) - Consider communicating an error in tooltip. However, I think leaving the spinner animation works.
          this._isRetrievingTooltip = false;
        });
    }
  }

  /**
   * A utility function to setup the help modal dialog with static help modal content, if available.
   */
  private setupModal(): void {
    if (this.staticHelp) {
      this._modalDetail = this.staticHelp;
    }
  }
}
