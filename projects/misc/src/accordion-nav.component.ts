import {
  ChangeDetectorRef, Component, Input, Output, isDevMode, QueryList,
  SimpleChange, EventEmitter, ViewChild, TemplateRef, AfterViewInit,
  OnChanges, OnDestroy
} from "@angular/core";

import { Subscription } from "rxjs";
import {NgbAccordion, NgbPanel, NgbPanelChangeEvent} from "@ng-bootstrap/ng-bootstrap";

/**
 * A type of navigation footer used to control a NgbAccordion.  It will control an accordion when a button representing
 * a panel is clicked and the accordion will update the footer when expanded.
 */
@Component({
  selector: "hci-accordion-nav",
  template: `
    <div class="d-flex flex-row justify-content-around">
      <ng-container *ngFor="let panel of panelData">
        <div class="d-flex flex-column item" (click)="select(panel.id)" [class.active]="panel.active">
         <div class="d-flex">
              <ng-container *ngTemplateOutlet="getIconTemplate(panel.id); context: {rowData: rowData}"></ng-container>
          </div>

          <div>{{panel.title}}</div>
        </div>
      </ng-container>
    </div>

      <!-- Default icon template for a panel if a panel Icon is not specified -->
      <ng-template #defaultIconTemplate>
         <div class="circle ml-auto mr-auto"></div>
      </ng-template>
  `
})
export class AccordionNavComponent implements AfterViewInit, OnChanges, OnDestroy {

  @Input() accordionId: string;
  @Input() accordion: NgbAccordion;
  // Sometimes the title is not in the ngb panel title property (because we want extra customization)
  // In that case pass the titles through this input. The titles should be passed as a JSON object 
  // {panel-id:panel-title,panel-id:panel-title,...}
  @Input() panelTitles: any;
  @Input() panelIcons: Object[];
  @Output() panelClicked = new EventEmitter();

  rowData: any;

  @ViewChild('defaultIconTemplate', {read: TemplateRef, static: true}) defaultIconTemplate: TemplateRef<any>;

  panels: QueryList<NgbPanel>;
  panelData: any[] = [];

  subscriptions: Subscription = new Subscription();

  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  ngAfterViewInit(): void {
    if (this.accordion) {
      this.panels = this.accordion.panels;
      this.setupPanels();
      this.listenToPanelChange();
    }
    this.changeDetectorRef.detectChanges();
  }

  /**
   * Listen to changes in bound inputs such as the view panels if they change.
   *
   * @param {{[p: string]: SimpleChange}} changes
   */
  ngOnChanges(changes: {[propName: string]: SimpleChange}): void {
    if (isDevMode()) {
      console.debug("AccordionNavComponent.ngOnChanges");
    }

    if (changes["accordion"] && this.accordion) {
      this.listenToPanelChange();
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  /**
   * A way to manually set the accordion as opposed to the @Input.
   *
   * @param {NgbAccordion} accordion
   */
  setAccordion(accordion: NgbAccordion): void {
    if (isDevMode()) {
      console.debug("AccordionNavComponent.setAccordion");
    }

    this.accordion = accordion;
    this.panels = accordion.panels;

    this.setupPanels();
    this.listenToPanelChange();
  }

  /**
   * Iterate over the view's panels and extract the id, title which are used to create the navigation bar.
   */
  setupPanels(): void {
    if (isDevMode()) {
      console.debug("AccordionNavComponent.setupPanels");
    }

    this.panelData = [];

    this.panels.forEach((panel: NgbPanel) => {
        if (this.panelTitles) {
            this.panelData.push({id: panel.id, title: this.panelTitles[panel.id], active: false}); 
        }
        else {
            this.panelData.push({id: panel.id, title: panel.title, active: false});
        }
    });
  }

  /**
   * With the accordion set, listen to its panelChange emitter.  This event contains the id of the
   * selected accordion panel.
   */
  listenToPanelChange(): void {
    if (this.accordion) {
      this.subscriptions.add(this.accordion.panelChange.subscribe((event: NgbPanelChangeEvent) => {
        if (event.nextState) {
          this.select(event.panelId);
        } else {
          this.select(undefined);
        }
      }));
    }
  }

  /**
   * Select a panel based on the activeIds.  However, the accordion may allow multiple actives, the accordion nav will
   * only set as active the most recent one.  Also, if the accordion is large, this will find the element of the active
   * panel and bring it in to view.
   *
   * @param {string | string[]} activeIds
   */
  select(activeIds: string | string[]): void {
    if (isDevMode()) {
      console.debug("select: " + activeIds);
    }

    if (this.accordion) {
      this.accordion.activeIds = activeIds;

      for (let panel of this.panelData) {
        panel.active = false;

        if (!activeIds) {
          continue;
        } else if (typeof activeIds === "string") {
          if (panel.id === activeIds) {
            panel.active = true;

            if (this.accordionId) {
              // Find the active element and bring it in to view.
              let e: HTMLElement = document.querySelector("#" + this.accordionId).querySelector("#" + panel.id + "-header");
              if (e) {
                e.scrollIntoView();
                // emit an event telling the parent component which panel was clicked
                const ngbPanelEventChange: NgbPanelChangeEvent = {
                    panelId: panel.id,
                    nextState: true, 
                    preventDefault: undefined
                };
                this.panelClicked.emit(ngbPanelEventChange);
              }
            }
          }
        } else if (activeIds.indexOf(panel.id) > -1) {
          panel.active = true;
        }
      }
    } else {
      console.warn("AccordionNavComponent: Accordion not set.");
    }
  }
   
   getIconTemplate(panelId) {
       if (!this.panelIcons || !this.panelIcons[panelId]) {
           return this.defaultIconTemplate;
       }
       else {
           return this.panelIcons[panelId];
       }
   }
}
