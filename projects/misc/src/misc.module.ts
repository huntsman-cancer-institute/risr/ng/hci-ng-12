import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {BusyComponent} from "./busy.component";
import {AccordionNavComponent} from "./accordion-nav.component";
import {PairedDataComponent} from "./paired-data.component";

@NgModule({
  imports: [
    CommonModule,
    NgbModule
  ],
  declarations: [
    AccordionNavComponent,
    BusyComponent,
    PairedDataComponent
  ],
  exports: [
    AccordionNavComponent,
    BusyComponent,
    PairedDataComponent
  ]
})
export class MiscModule {}
