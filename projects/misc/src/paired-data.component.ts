 import {
  Component,
  ElementRef,
  HostBinding,
  isDevMode,
  Renderer2,
  ViewEncapsulation,
  Input,
  TemplateRef,
  ViewChild,
   ChangeDetectorRef
 } from "@angular/core";

 /**
 * Simple horizontal data display
 */
@Component({
  encapsulation: ViewEncapsulation.None,
  selector: "hci-paired-data",
  template: `
<div class="d-flex flex-column" style="flex:1 1 100%">
    <div class="d-flex" style="align-items:center;color:#00AAFF">
         <i  class="hci {{header.icon}} fa-2x" aria-hidden="true"></i>{{header.title}}
    </div>
    <div class="d-flex">
      <ng-container *ngFor="let col of createNumCols()">
       <div class="d-flex" [ngStyle]="{'flex' : colStyle}">
      <div class="d-flex flex-column" style="align-items:flex-end">
        <ng-container *ngFor="let rowData of getRowsForColumn(col); let i = index;">
            <div class="d-flex">
              <ng-container *ngTemplateOutlet="getKeyTemplate(col,i); context: {rowData: rowData}"></ng-container>
            </div>
        </ng-container>
      </div>

      <div class="d-flex flex-column">
        <ng-container *ngFor="let rowData of getRowsForColumn(col); let i = index;">
            <div class="d-flex">
              <ng-container *ngTemplateOutlet="getValueTemplate(col,i); context: {rowData: rowData}"></ng-container>
            </div>
        </ng-container>
      </div>
      </div>
       <!-- vertical separator -->
       <div *ngIf="col < numColumns - 1" class="d-flex ml-2 mr-2" style="border-style:solid;border-width:1px;color:#98C7E0;"></div>
      </ng-container>
   </div>
 </div>
   <!-- Default templates for a row -->
    <ng-template #defaultKeyTemplate let-row="rowData">
      <div class="d-flex flex-grow-1 pl-2" style="align-items: center;">
        {{row.key}}
      </div>
    </ng-template>

    <!--The &nbsp; makes sure the div will have a dimension even when row.value is undefined/null -->
    <ng-template #defaultValueTemplate let-row="rowData">
      <div class="d-flex flex-grow-1 pl-2" style="align-items: center;font-weight:bold">
        {{row.value}}<span [innerHTML]="'&nbsp;'"></span>
      </div>
    </ng-template>

   `, styles: [`


  `]
})
export class PairedDataComponent {

   // how many columns
   @Input("numColumns") numColumns: number;
   // the header title, icon, ...
   @Input("header") header: any;
   // the data rows
   @Input("dataArray") dataArray: Object[];

   @ViewChild("defaultKeyTemplate", {read: TemplateRef, static: true}) defaultKeyTemplate: TemplateRef<any>;
   @ViewChild("defaultValueTemplate", {read: TemplateRef, static: true}) defaultValueTemplate: TemplateRef<any>;

   numRowsPerColumn: number;
   colStyle: string = "1 1 100%";

   constructor(private changeDetectorRef: ChangeDetectorRef) {}

   ngAfterViewInit(): void {
       this.numRowsPerColumn = Math.ceil(this.dataArray.length/this.numColumns);
       this.colStyle = '1 1 ' + (Math.floor(100/this.numColumns)-2) + "%";
       this.changeDetectorRef.detectChanges();
   }

   // A placeholder method to return an array of integers representing the columns numbers
   createNumCols() {
        if (!this.numColumns)
           return;
        let items: number[] = [];
        for (let i = 0; i < this.numColumns; i++) {
            items.push(i);
        }
        return items;
   }

   // Return the rows for the given column
   getRowsForColumn(col: number) {
      if (!this.numColumns || !this.numRowsPerColumn)
           return;
      let columnRowData: Object[] = [];
      if (col < (this.numColumns - 1)) {
         for (let i = col * this.numRowsPerColumn; i < (col + 1) * this.numRowsPerColumn; i++) {
            columnRowData.push(this.dataArray[i]);
         }
      }
      else {
        // last column just has the remaining rows which can be less than numRows
        for (let i = (this.numColumns - 1) * this.numRowsPerColumn; i < this.dataArray.length; i++) {
            columnRowData.push(this.dataArray[i]);
        }
      }
      return columnRowData;
   }

   getKeyTemplate(col: number, row: number) {
       let index: number = col * this.numRowsPerColumn + row;
       if (!this.dataArray[index]['keyTemplate']) {
           return this.defaultKeyTemplate;
       }
       else {
           return this.dataArray[index]['keyTemplate'];
       }
   }

   getValueTemplate(col: number, row: number) {
       let index: number = col * this.numRowsPerColumn + row;
       if (!this.dataArray[index]['valueTemplate']) {
           return this.defaultValueTemplate;
       }
       else {
           return this.dataArray[index]['valueTemplate'];
       }
   }
}
