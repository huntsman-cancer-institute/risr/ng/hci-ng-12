/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Type} from "@angular/core";
import {LocationStrategy} from "@angular/common";
import {TestBed, tick, fakeAsync, async, discardPeriodicTasks} from "@angular/core/testing";
import {Router, NavigationExtras, UrlTree} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {HttpClientTestingModule} from "@angular/common/http/testing";

import {} from "jasmine";
import {of} from "rxjs";
import {JwtHelperService} from "@auth0/angular-jwt";

import {
  AuthenticationService,
  AUTHENTICATION_LOGOUT_PATH,
  AUTHENTICATION_DIRECT_ENDPOINT,
  AUTHENTICATION_TOKEN_ENDPOINT,
  AUTHENTICATION_ROUTE
} from "./authentication.service";
import {AUTHENTICATION_TOKEN_KEY, AuthenticationProvider} from "./authentication.provider";
import {AuthenticationModule} from "./authentication.module";
import {CoolLocalStorage} from "@angular-cool/storage";

/**
 * {@link AuthenticationService} unit tests.
 *
 * @since 1.0.0
 */

class MockInjector {
  get(type: Type<any>): JwtHelperService {
    return new JwtHelperService({});
  }
}

class MockRouter {
  navigate(commands: any[], extras?: NavigationExtras): Promise<boolean> {
    return Promise.resolve(true);
  }

  navigateByUrl(url: string | UrlTree, extras?: NavigationExtras): Promise<boolean> {
    return Promise.resolve(true);
  }
}

class MockCoolLocalStorage {
  getItem(key: string) {
    return "jwt_value";
  }

  setItem(key: String, value: any) {
    return;
  }

  removeItem(key: string) {
    return;
  }
}

class MockHttp {
  get(url: String, options: any) {
    return of({status: 200, body: {auth_token: "new_jwt_value"}});
  }
}

class MockLocationStrategy {

}

describe("AuthenticationService Tests", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AuthenticationModule.forRoot()
      ],
      providers: [
        {provide: LocationStrategy, useValue: MockLocationStrategy},
        {provide: Router, useValue: new MockRouter()},
        {provide: AUTHENTICATION_LOGOUT_PATH, useValue: "https://localhost:8080/auth/logout"},
        {provide: AUTHENTICATION_DIRECT_ENDPOINT, useValue: "https://localhost:8080/core/api/user/user-session/active"},
        {provide: AUTHENTICATION_TOKEN_ENDPOINT, useValue: "https://localhost:8080/core/api/token"},
        {provide: AUTHENTICATION_ROUTE, useValue: "/authentication"},
        {provide: AUTHENTICATION_TOKEN_KEY, useValue: "jwt_token"},
        {provide: HttpClient, useValue: new MockHttp()},
        {provide: CoolLocalStorage, useValue: new MockCoolLocalStorage()}
      ]
    });
  });

  it("Should store the token, mark the user authenticated and subscribe for refresh if there is initially a valid token in local storage.",
    fakeAsync(() => {
      spyOn(JwtHelperService.prototype, "isTokenExpired").and.callFake(function() {
        return false;
      });
      spyOn(JwtHelperService.prototype, "getTokenExpirationDate").and.callFake(function() {
        return new Date();
      });
      let storeTokenSpy = spyOn(AuthenticationService.prototype, "storeToken").and.callThrough();
      let validateSpy = spyOn(AuthenticationService.prototype, "validateToken").and.callThrough();
      let subscribeSpy = spyOn(AuthenticationService.prototype, "subscribeToTokenRefresh");

      let authenticationService: AuthenticationService = new AuthenticationService(
        <HttpClient>TestBed.get(HttpClient),
        <Router>TestBed.get(Router),
        <CoolLocalStorage>TestBed.get(CoolLocalStorage),
        <JwtHelperService>TestBed.get(JwtHelperService),
        <AuthenticationProvider>TestBed.get(AuthenticationProvider),
        "", "", "", undefined, undefined, undefined, undefined, undefined,
        <LocationStrategy>TestBed.get(LocationStrategy));

      expect(storeTokenSpy).toHaveBeenCalledWith("jwt_value");
      expect(validateSpy).toHaveBeenCalledWith("jwt_value");
      authenticationService.isAuthenticated().toPromise().then((value) => {
        expect(value).toBe(true);
      });

      expect(subscribeSpy).toHaveBeenCalled();
      discardPeriodicTasks();
    }));

  it("Should store the token, mark the user authenticated and subscribe for refresh if there is initially a valid token in local storage.",
    () => {
      spyOn(JwtHelperService.prototype, "isTokenExpired").and.callFake(function() {
        return false;
      });
      spyOn(JwtHelperService.prototype, "getTokenExpirationDate").and.callFake(function() {
        return new Date();
      });
      let storeTokenSpy = spyOn(AuthenticationService.prototype, "storeToken").and.callThrough();
      let validateSpy = spyOn(AuthenticationService.prototype, "validateToken").and.callThrough();
      let subscribeSpy = spyOn(AuthenticationService.prototype, "subscribeToTokenRefresh");

      let authenticationService: AuthenticationService = new AuthenticationService(
        <HttpClient>TestBed.get(HttpClient),
        <Router>TestBed.get(Router),
        <CoolLocalStorage>TestBed.get(CoolLocalStorage),
        <JwtHelperService>TestBed.get(JwtHelperService),
        <AuthenticationProvider>TestBed.get(AuthenticationProvider),
        "", "", "", null, null, null, null, null,
        <LocationStrategy>TestBed.get(LocationStrategy));

      expect(storeTokenSpy).toHaveBeenCalledWith("jwt_value");
      expect(validateSpy).toHaveBeenCalledWith("jwt_value");
      authenticationService.isAuthenticated().toPromise().then((value) => {
        expect(value).toBe(true);
      });

      expect(subscribeSpy).toHaveBeenCalled();
    });

    it("Should return the authentication token key for AuthHttp", () => {
      spyOn(JwtHelperService.prototype, "isTokenExpired").and.callFake(function() {
        return false;
      });
      spyOn(JwtHelperService.prototype, "getTokenExpirationDate").and.callFake(function() {
        return new Date();
      });
      let updateSpy = spyOn(AuthenticationService.prototype, "updateUserActivity").and.callThrough();

      let authenticationService: AuthenticationService = new AuthenticationService(
        <HttpClient>TestBed.get(HttpClient),
        <Router>TestBed.get(Router),
        <CoolLocalStorage>TestBed.get(CoolLocalStorage),
        <JwtHelperService>TestBed.get(JwtHelperService),
        <AuthenticationProvider>TestBed.get(AuthenticationProvider),
        "", "", "", undefined, undefined, 1, 59, undefined,
        <LocationStrategy>TestBed.get(LocationStrategy));

      let value = authenticationService.authenticationTokenKey;
      expect(value).toBe("jwt_token");
    });

    it("Should not be about to time out when the user is first updated, but should be about to time out after the interval",
      fakeAsync(() => {
        spyOn(JwtHelperService.prototype, "isTokenExpired").and.callFake(function() {
          return false;
        });
        spyOn(JwtHelperService.prototype, "getTokenExpirationDate").and.callFake(function() {
          return new Date();
        });
        spyOn(AuthenticationService.prototype, "subscribeToTokenRefresh").and.callFake(function() {
          return;
        });

        let authenticationService: AuthenticationService = new AuthenticationService(
          <HttpClient>TestBed.get(HttpClient),
          <Router>TestBed.get(Router),
          <CoolLocalStorage>TestBed.get(CoolLocalStorage),
          <JwtHelperService>TestBed.get(JwtHelperService),
          <AuthenticationProvider>TestBed.get(AuthenticationProvider),
          "", "", "", undefined, undefined, 1, 59, undefined,
          <LocationStrategy>TestBed.get(LocationStrategy));

        authenticationService.isAuthenticated().toPromise().then((value) => {
          expect(value).toBe(true);
        });

        let token = authenticationService.authToken;

        authenticationService.isAboutToTimeOut().toPromise().then((value) => {
          expect(value).toBe(false);
        });

        tick(1000);

        authenticationService.isAboutToTimeOut().toPromise().then((value) => {
          expect(value).toBe(true);
        });

        authenticationService.updateUserActivity();

        authenticationService.isAboutToTimeOut().toPromise().then((value) => {
          expect(value).toBe(false);
        });

        discardPeriodicTasks();
      }));

    it("Should proceed after successful token load if coming from direct login component", fakeAsync(() => {
      spyOn(JwtHelperService.prototype, "isTokenExpired").and.callFake(function() {
        return false;
      });
      spyOn(JwtHelperService.prototype, "getTokenExpirationDate").and.callFake(function() {
        return new Date(Date.now() + 60000);
      });

      let authenticationService: AuthenticationService = new AuthenticationService(
        <HttpClient>TestBed.get(HttpClient),
        <Router>TestBed.get(Router),
        <CoolLocalStorage>TestBed.get(CoolLocalStorage),
        <JwtHelperService>TestBed.get(JwtHelperService),
        <AuthenticationProvider>TestBed.get(AuthenticationProvider),
        "route", "", "", undefined, undefined, 2, 69, undefined,
        <LocationStrategy>TestBed.get(LocationStrategy));

      let proceedSpy = spyOn(authenticationService, "proceedIfAuthenticated").and.callThrough();

      authenticationService.isAuthenticated().toPromise().then((value) => {
        expect(value).toBe(true);
      });

      authenticationService.requestAccessToken(true);
      expect(proceedSpy).toHaveBeenCalled();

      discardPeriodicTasks();
    }));
});
