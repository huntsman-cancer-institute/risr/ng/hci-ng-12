/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {Router} from "@angular/router";
import {By} from "@angular/platform-browser";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {TestBed, fakeAsync, tick, discardPeriodicTasks, ComponentFixture} from "@angular/core/testing";

import {} from "jasmine";
import {of} from "rxjs";

import {TimeoutNotificationComponent} from "./timeout-notification.component";
import {
  AuthenticationService,
  AUTHENTICATION_LOGOUT_PATH,
  AUTHENTICATION_DIRECT_ENDPOINT,
  AUTHENTICATION_TOKEN_ENDPOINT,
  AUTHENTICATION_ROUTE,
  AUTHENTICATION_MAX_INACTIVITY_MINUTES,
  AUTHENTICATION_USER_COUNTDOWN_SECONDS
} from "./authentication.service";
import {AUTHENTICATION_TOKEN_KEY} from "./authentication.provider";
import {AuthenticationModule} from "./authentication.module";
import {CoolLocalStorage} from "@angular-cool/storage";

let fixture: ComponentFixture<TimeoutNotificationComponent>;

class MockRouter {
  navigate(config: any) {
    return;
  }
}

class MockHttp  {
  get(url: String, options: any) {
    return {status: 200};
  }
}

class MockCoolLocalStorage {
  getItem(key: string) {
    return null;
  }

  removeItem(key: string) {
    return;
  }
}

describe("TimeoutNotificationComponent Tests", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        AuthenticationModule.forRoot()
      ],
      providers: [
        {provide: Router, useValue: new MockRouter()},
        {provide: AUTHENTICATION_LOGOUT_PATH, useValue: "https://localhost:8080/auth/logout"},
        {provide: AUTHENTICATION_DIRECT_ENDPOINT, useValue: "https://localhost:8080/core/api/user/user-session/active"},
        {provide: AUTHENTICATION_TOKEN_ENDPOINT, useValue: "https://localhost:8080/core/api/token"},
        {provide: AUTHENTICATION_ROUTE, useValue: "/authentication"},
        {provide: AUTHENTICATION_TOKEN_KEY, useValue: "jwt_token"},
        {provide: AUTHENTICATION_MAX_INACTIVITY_MINUTES, useValue: 1},
        {provide: AUTHENTICATION_USER_COUNTDOWN_SECONDS, useValue: 1},
        {provide: CoolLocalStorage, useValue: new MockCoolLocalStorage()},
        {provide: MockHttp, useValue: new MockHttp()}
      ]
    });
  });

  it("Should be hidden when NOT about to time out", fakeAsync(() => {
    let authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
    let timeoutSpy = spyOn(authenticationService, "isAboutToTimeOut").and.returnValue(of(false));

    fixture = TestBed.createComponent(TimeoutNotificationComponent);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(fixture.componentInstance.openState).toBe("hidden");
    });

    discardPeriodicTasks();
  }));

  it("Should be opened when about to time out", fakeAsync(() => {
    let authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
    let timeoutSpy = spyOn(authenticationService, "isAboutToTimeOut").and.returnValue(of(true));

    fixture = TestBed.createComponent(TimeoutNotificationComponent);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(fixture.componentInstance.openState).toBe("opened");
    });

    discardPeriodicTasks();
  }));

  it("Should call AuthenticationService updateUserActivity() when the button is clicked.", fakeAsync(() => {
    let authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
    let timeoutSpy = spyOn(authenticationService, "isAboutToTimeOut").and.returnValue(of(true));
    let updateSpy = spyOn(authenticationService, "updateUserActivity");
    let startSpy = spyOn(fixture.componentInstance, "startCountdown");

    fixture = TestBed.createComponent(TimeoutNotificationComponent);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(startSpy).toHaveBeenCalled();

      let button = fixture.debugElement.query(By.css("#updateBtn")).nativeElement;
      button.click();

      fixture.detectChanges();

      expect(updateSpy).toHaveBeenCalled();
    });

    discardPeriodicTasks();
  }));

  it("Should call AuthenticationService logout() when the button is NOT clicked.", fakeAsync(() => {
    let authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
    let timeoutSpy = spyOn(authenticationService, "isAboutToTimeOut").and.returnValue(of(true));
    let logoutSpy = spyOn(authenticationService, "logout").and.callThrough();
    let startSpy = spyOn(fixture.componentInstance, "startCountdown");

    fixture = TestBed.createComponent(TimeoutNotificationComponent);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(startSpy).toHaveBeenCalled();

      tick(2000);

      fixture.detectChanges();

      fixture.componentInstance.seconds.toPromise().then((value) => {
        expect(value).toBe(0);
      });

      expect(logoutSpy).toHaveBeenCalled();
    });

    discardPeriodicTasks();
  }));
});
