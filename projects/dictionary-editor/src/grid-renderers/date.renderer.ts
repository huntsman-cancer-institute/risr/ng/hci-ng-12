import {Component} from "@angular/core";
import {CellRendererValidation} from "./cell-renderer-validation";
import {ICellRendererAngularComp} from "ag-grid-angular";
import * as moment from "moment";
import {DatePipe} from "@angular/common";


@Component({
    template: `
        <div class="t full-width full-height fix-table">
            <div class="tr">
                <div class="td cell-text-container ellipsis">
                    {{display}}
                </div>
            </div>
        </div>
    `,
    styles: [`
        .t  { display: table;      }
        .tr { display: table-row;  }
        .td { display: table-cell; }

        .cell-text-container {
            vertical-align: middle;
            padding-left: 0.3rem;
        }

        .full-width  { width:  100%; }
        .full-height { height: 100%; }

        .fix-table { table-layout:fixed; }

        .ellipsis {
            overflow: hidden;
            text-overflow: ellipsis;
        }
    `]
})
export class DateRenderer extends CellRendererValidation implements ICellRendererAngularComp {

    value: Date;
    display: string;

    constructor(protected datePipe: DatePipe) {
        super();
    }

    agInit2(params: any): void {
        this.value = undefined;

        if (this.params) {
            if(this.params.value) {
                this.value = this.params.value;
            } else {
                this.value = undefined;
            }
        }

        this.display = this.getDateString(this.value);
    }

    getDateString(date: any): string {

        if (date instanceof Date) {
            if (!isNaN(date.getTime())) {
                return this.datePipe.transform(date, "yyyy-MM-dd");
            } else {
                return "";
            }
        }else if(moment.isMoment(date) ) {
            if(date.isValid()) {
                return date.format("YYYY-MM-DD");
            }else {
                return "";
            }
        }else if(typeof date == "number") {
            if(date > 0) {
                let temp: Date = new Date(date);
                return this.datePipe.transform(temp, "yyyy-MM-dd");
            }else {
                return "";
            }
        }else if(typeof date == "string") {
            return date;
        }else {
            return "";
        }
    }

    refresh(): boolean {
        return false;
    }
}
