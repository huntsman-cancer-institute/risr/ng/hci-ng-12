import {Component} from "@angular/core";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {GridApi, GridOptions, GridReadyEvent} from "ag-grid-community";
import {Subject} from "rxjs";

@Component({
    selector: "new-row",
    template:
      `
        <div class="d-flex flex-column" style="height:11em; width:58vw;">
          <div class="d-flex flex-row">
            <div class="flex-grow-1"></div>
            <button type="button" class="close" aria-label="Close"
                    style="margin-right: 10px; width: 20px;"
                    (click)="activeModalSearch.dismiss('Cancel')">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="flex-grow-1" style="padding-right: 8px; padding-left: 8px;">
            <ag-grid-angular class="ag-theme-balham full-height full-width"
                             [rowData]="newRow"
                             [singleClickEdit]="true"
                             [gridOptions]="gridOptions"
                             (gridReady)="this.onSamplesGridReady($event)">
            </ag-grid-angular>
          </div>

          <div class="d-flex flex-row" style="padding: 0.5em;">
            <div class="flex-grow-1"></div>
            <button class="btn btn-red ml-3"
                    style="color: var(--white-lightest);"
                    (click)="activeModalSearch.dismiss('Cancel')">
              Cancel
            </button>
            <button class="btn btn-primary ml-3"
                    style="color: var(--white-lightest);"
                    (click)="save()">
              Save
            </button>
          </div>
        </div>
      `
    ,
    styles: [
      `
        .full-height {
          height: 100%;
        }

        .full-width {
          width: 100%;
        }

        .ag-theme-balham{
          font-family: "hci-font";
        }

        :host ::ng-deep .ag-ltr .ag-cell {
          border-right-color: var(--grey-darkest);
        }
      `
    ]
  }
)
export class NewRowComponent {
  public newRowSubject: Subject<any[]> = new Subject<any[]>();
  private _colDefs: any[];
  public get colDefs() {
    return this._colDefs;
  }

  public set colDefs(value: any[]) {
    this._colDefs = value;
    this.newRow[0]["editOrNew"] = "new"; //needed for editable function
    for (let colDef of this.colDefs) {
      this.newRow[0][colDef.field] = "";
    }
  }

  public newRow: any[] = [{}];
  dictionary: any;
  gridOptions: GridOptions;
  gridApi: GridApi;

  constructor(public activeModalSearch: NgbActiveModal) {
    this.gridOptions = <GridOptions>{
      context: {
        componentParent: this
      }
    };
  }

  public save() {
    // get contents of row
    //making sure the grid has stopped editing when user clicks save
    // waiting a tick to make sure that async event has finished
    this.gridApi.stopEditing();

    setTimeout(
      () => {
        let newData: any[] = [];

        this.gridApi.forEachNode(
          (node: any) => {
            newData.push(node.data);
          }
        );

        // validate the contents of row
        let isValid: boolean = true;

        for (let colDef of this.colDefs) {
          if (colDef.dictionaryDisplay == true && !newData[0][colDef.field]) {
            let displayDictField = null;

            if (this.gridOptions.context && this.gridOptions.context.componentParent
              && this.gridOptions.context.componentParent.dictionary) {
              let dictFields: any[] = this.gridOptions.context.componentParent.dictionary.fields;
              displayDictField = dictFields.find(field => (field.dictionaryDisplay === true));
            }

            //don't require displayField if it is readonly when making a new entry
            if (displayDictField && displayDictField.protectionType === "Read Only") {
              isValid = true;
            } else {
              isValid = false;
            }
          }
        }

        if (isValid) {
          // send contents of row to other ag-grid
          this.newRowSubject.next(newData);
          this.activeModalSearch.dismiss("Cancel");
        } else {
          // signal they need to fill out the row
          alert("The display value is required, please enter a value and then save again.");
        }
      }
    );
  }

  public onSamplesGridReady(event: GridReadyEvent): void {
    event.api.setColumnDefs(this.colDefs);
    event.api.sizeColumnsToFit();
    this.gridApi = event.api;
  }
}
