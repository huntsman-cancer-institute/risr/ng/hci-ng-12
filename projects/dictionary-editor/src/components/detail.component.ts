import {Component, Inject, isDevMode, ViewChild, HostBinding, TemplateRef} from "@angular/core";
import {ActivatedRoute} from "@angular/router";

import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {finalize, map, takeUntil} from 'rxjs/operators';
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";

import {DICTIONARY_ENDPOINT, DictionaryService} from '@huntsman-cancer-institute/dictionary-service';

import {
  ColumnApi,
  GridOptions,
  GridReadyEvent,
  GridApi
} from "ag-grid-community";
import {SelectRenderer} from "../grid-renderers/select.renderer";
import {SelectEditor} from "../grid-editors/select.editor";
import {NewRowComponent} from "./new-row.component";
import {isNullOrUndefined} from "util";
import {DateRenderer} from "../grid-renderers/date.renderer";
import {AgGridDateCellEditorComponent} from "../grid-editors/ag-grid-date-cell-editor.component";

/**
 * When a dictionary is selected this component is shown.  There is a top grid which for its data shows all the columns
 * for the dictionary. Currently, keep this for reference only and have non-editable.
 * The bottom grid will show those columns as actual columns and its data are all the dictionary values.  Rows can be
 * added.  For deleting, need to make sure the id is not used anywhere.  For editing, need to make sure the code or anything
 * else isn't being referenced.
 *
 * @since 1.0.0
 */
@Component(
  {
    selector: "hci-dictionary-editor-detail",
    template:
      `
        <div class="d-flex flex-column hci-dictionary-detail-body">
          <hci-busy [busySubjects]="loadingSubjects"></hci-busy>

          <!-- Dictionary Title And Metadata -->
          <div class="dictionary-info-section">
            <span class="dictionary-title">
              {{displayName}}
            </span>
            <button class="btn btn-primary metadata-button"
                    (click)="openModal(metaDataModal)">
              View Meta Data
            </button>
          </div>

          <!-- Dictionary Edit Buttons -->
          <div>
            <button [class]="disableDelete? 'deactivated-color': 'active-color'"
                    [disabled]="disableDelete"
                    (click)="openAreYouSureModal(areYouSureModal)">
              <i class="fas fa-minus"></i>
            </button>
            <button style="color: var(--bluewarm-darkest); float: right; font-size: 15px;"
                    [disabled]="disableAddRow"
                    (click)="onClickAddRow()">
              <i class="fas fa-plus"></i>
            </button>
          </div>

          <!-- Dictionary Display Grid -->
          <div class="d-flex flex-column flex-grow-1">
            <ag-grid-angular id="dictionary-grid-display"
                             #dataGrid
                             class="ag-theme-balham full-height full-width hci-dictionary-display-grid"
                             [rowData]="dictionaryEntries"
                             [rowSelection]="'multiple'"
                             [singleClickEdit]="'true'"
                             [pagination]="'true'"
                             [paginationAutoPageSize]="'true'"
                             [gridOptions]="gridOptions"
                             (cellValueChanged)="updateDictionary($event)">
            </ag-grid-angular>
          </div>

          <!-- Dictionary Metadata Popup -->
          <ng-template #metaDataModal let-close="close">
            <div style="width:1000px">
              <div class="modal-header">
                Meta Data for {{className}}
              </div>
              <div class="d-flex flex-column modal-body"
                   style="border: 1px solid #ced4da; width: 100%;">
                <ag-grid-angular #metaGrid
                                 class="ag-theme-balham full-width"
                                 style="height: 250px;"
                                 (gridReady)="this.onMetaGridReady($event)"
                                 [gridOptions]="this.metaGridOptions"
                                 [rowSelection]="'single'"
                                 [frameworkComponents]="frameworkComponents"
                                 [rowData]="dictionary?.fields">
                </ag-grid-angular>
              </div>
              <div class="modal-footer">
                <button class="btn btn-primary" (click)="close('Close')">Close</button>
              </div>
            </div>
          </ng-template>

          <!-- Dictionary Delete Comfirmation Popup -->
          <ng-template #areYouSureModal let-close="close">
            <div class="modal-header">
              Confirmation
            </div>
            <div class="modal-body d-flex">
              Are you sure?
            </div>
            <div class="modal-footer">
              <button class="btn btn-primary" (click)="close('Cancel')">Cancel</button>
              <button class="btn btn-primary btn-red" (click)="close('Delete')">Delete</button>
            </div>
          </ng-template>
        </div>
      `,
    styles: [
      `
        .full-height {
          height: 100%;
        }

        .full-width {
          width: 100%;
        }

        .ag-theme-balham {
          font-family: "hci-font";
          font-weight: 500;
        }

        .active-color {
          float: right;
          font-size: 15px;
          color: var(--bluewarm-darkest);
        }

        .deactivated-color {
          float: right;
          font-size: 15px;
          color: var(--greywarm-darkest);
        }

        .dictionary-info-section {
          padding-top: 10px;
        }

        .dictionary-title {
          float: left;
          font-size: 16px;
          padding-left: 10px;
          padding-right: 10px;
          padding-top: 5px;
        }

        .metadata-button {
          color: var(--white-lightest);
          float: left;
          font-size: 15px;
        }

        :host::ng-deep .ag-header-cell {
          background-color: var(--bluewarmvividfade-lightest);
          border: lightgray solid thin;
        }

        :host::ng-deep .ag-cell {
          border-right: lightgray solid thin;
          border-left: lightgray solid thin;
        }

        :host::ng-deep .ag-row {
          border-bottom: 0;
          border-top: 0;
        }

        :host::ng-deep .ag-row-even {
          background-color: var(--white-medium);
        }

        .hci-dictionary-detail-body {
          height: 100%;
          width: 100%;
          background: linear-gradient(0deg, var(--white-lightest) 30%, var(--bluewarmvividfade-lighter) 100%) !important;
        }

        .hci-dictionary-display-grid {
          padding-right: 5px;
          padding-left: 5px;
          box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        }

        /********************************************************************
        * Dictionary Row Hover CSS
        ********************************************************************/
        :host::ng-deep .ag-row-hover {
          color: var(--grey-darkest) !important;
          background-color: var(--bluewarmvividfade-lighter) !important;
        }

        /********************************************************************
        * Dictionary Cell Selected CSS
        ********************************************************************/
        :host::ng-deep .ag-row-selected .ag-cell {
          color: var(--white-lightest) !important;
        }

        :host::ng-deep .ag-row-selected .ag-cell-not-inline-editing {
          color: var(--white-lightest) !important;
        }

        :host::ng-deep .ag-row-selected .ag-cell-inline-editing {
          color: var(--black-darkest) !important;
        }

        /********************************************************************
        * Dictionary Row Selected CSS
        ********************************************************************/
        :host::ng-deep .ag-row-selected {
          color: var(--white-darkest) !important;
          background-color: var(--bluewarmvivid-lighter) !important;
        }
      `
    ]
  }
)
export class DictionaryEditorDetailComponent {

  @HostBinding("class") classList: string = "outlet-column y-auto";

  private secondaryDictionaryEntries: any;
  className: string;
  displayName: any;
  dictionary: any;
  dictionaryEntries: any[] = [];
  dataColumns: any[];

  idColumn: string;
  selectedRows: any[] = [];
  loadingSubjects: BehaviorSubject<boolean>[] = [];
  halfLoaded: boolean = false;
  processingCall: boolean = false;
  callQueued: boolean = false;

  boundNewRowPostCall: (data: any) => Observable<any>;
  gridOptions: GridOptions;
  newRowSubscription: any;
  private destroy = new Subject<void>();

  metaGridOptions = {
    rowStyle: {
      "font-family": "hci-font"
    },
    enableCellTextSelection: true,
    ensureDomOrder: true
  };
  frameworkComponents: any;
  public gridApiMeta: GridApi;


  constructor(
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private dictionaryService: DictionaryService,
    @Inject(DICTIONARY_ENDPOINT) private DictionaryEndpoint: string
  ) {

    this.gridOptions = <GridOptions>{
      context: {
        componentParent: this
      },
      onGridReady: DictionaryEditorDetailComponent.onSamplesGridReady,
      onGridSizeChanged: DictionaryEditorDetailComponent.onGridSizeChanged,
      suppressHorizontalScroll: false,
      selectionMode: "multiple",
      stopEditingWhenGridLosesFocus: true,
      enableSorting: true,
      enableFilter: true
    };
  }

  /**
   * Based on the route class name, make the call to fetch the dictionary data.  Since this component is route based,
   * when the dictionary class in the route changes, this component is reloaded.
   */
  ngOnInit() {
    this.boundNewRowPostCall = this.newRowPost.bind(this);

    this.loadingSubjects.push(new BehaviorSubject<boolean>(false));

    this.route.params.subscribe(
      params => {

        let className: string = params["className"];

        this.getDictionary(className);
      }
    );

    this.loadingSubjects[0].subscribe((value: boolean) => {
      if (value === true) {
        // A new call was made to be processed, process the call
        this.getDictionaryBegin(this.className);
      } else {
        // A call completed while another call was not queued, processing complete
        this.processingCall = false;
      }
    });
  }

  openModal(modal: TemplateRef<any>): void {

    this.modalService.open(modal, {windowClass: "modal-xl"}).result.then(
      (result) => {
      },
      (reason) => {
      }
    );
  }

  ngAfterViewInit() {

    this.gridOptions.api.sizeColumnsToFit();
  }

  public columnDefMeta() : any[] {

    return [
      {field: "name", headerName: "Name", width: 175, sortable: true, resizable: true, editable: false},
      {field: "description", headerName: "Description", width: 250, sortable: true, resizable: true, editable: false},
      {field: "id", headerName: "Id", width: 60, sortable: true, resizable: true, editable: false},
      {field: "type", headerName: "Type", sortable: true, resizable: true, editable: false},
      {field: "protectionType", headerName: "ProtectionType", sortable: true, resizable: true, editable: false},
      {field: "filterDictionaryClass", headerName: "FilterDictionaryClass", width: 180, sortable: true, resizable: true, editable: false},
      {field: "notNull", headerName: "NotNull", sortable: true, resizable: true, editable: false},
      {field: "integer", headerName: "Integer", width: 75, sortable: true, resizable: true, editable: false},
      {field: "min", headerName: "Min", width: 60, sortable: true, resizable: true, editable: false},
      {field: "max", headerName: "Max", width: 60, sortable: true, resizable: true, editable: false},
      {field: "fraction", headerName: "Fraction", sortable: true, resizable: true, editable: false},
      {field: "dictionaryTooltip", headerName: "DictionaryTooltip", width: 200, sortable: true, resizable: true, editable: false},
      {field: "dictionaryDisplay", headerName: "DictionaryDisplay", width: 175, sortable: true, resizable: true, editable: false},
      {field: "filterable", headerName: "Filterable", sortable: true, resizable: true, editable: false},
      {field: "displayName", headerName: "DisplayName", width: 175, sortable: true, resizable: true, editable: false},
      {field: "displayOrder", headerName: "DisplayOrder", width: 60, sortable: true, resizable: true, editable: false},
      {field: "cellStyle", headerName: "CellStyle", sortable: true, resizable: true, editable: false},
    ]; // the widths for the last 2 could be wrong.

  }

  onMetaGridReady(params : any) {

    this.gridApiMeta = params.api;
    this.gridApiMeta.setColumnDefs(this.columnDefMeta());
    this.gridApiMeta.setRowData(this.dictionary?.fields);

  }

  openAreYouSureModal(modal: TemplateRef<any>): void {

    this.modalService.open(modal).result.then(
      (result) => {
        if (result === "Delete") {
          this.deleteDictionaryEntries();
        }
      },
      (reason) => {
      }
    );
  }

  /**
   * Determine if we are loading something, and then wait until it is done to .
   *
   * @param {string} className
   */
  getDictionary(className: string) {
    this.className = className;
    if (this.processingCall === false) {
      // Call is not being processed, process this call
      this.processingCall = true;
      this.loadingSubjects[0].next(true);
    } else {
      // A call is already being processed, queue a recall when it is complete
      this.callQueued = true;
    }
  }

  /**
   * Fetch the dictionary metadata and dictionary entries based upon the dictionary class name.
   *
   * @param {string} className
   */
  private getDictionaryBegin(className: string) {
    this.halfLoaded = false;
    // When Selecting A New Dictionary, Navigate To First Page
    if (this.gridOptions && this.gridOptions.api) {
      this.gridOptions.api.paginationGoToFirstPage();
    }
    this.dataColumns = undefined;

    this.destroy.next();
    this.destroy.complete();
    this.dictionaryService.getDictionary(className).pipe(
      takeUntil(this.destroy)
    ).subscribe((dictionary: Object) => {
        this.setUpDictionaryGrid(dictionary);
      }
    );

    this.getDictionaryEntries();
  }

  getDictionaryEntries(): void {

    this.dictionaryService.getDictionaryEntries(this.className).pipe(takeUntil(this.destroy)).subscribe(
      (dictionaryEntries: Object[]) => {
        this.dictionaryEntries = dictionaryEntries;
        this.gridOptions.api.setRowData(dictionaryEntries);
        this.gridOptions.api.sizeColumnsToFit();
        if (this.halfLoaded) {
          if (this.callQueued === true) {
            // A call completed while another call was queued so make that call now
            this.callQueued = false;
            this.getDictionaryBegin(this.className);
          } else {
            this.loadingSubjects[0].next(false);
          }
        } else {
          this.halfLoaded = true;
        }
      }
    );
  }

  /**
   * Set the dictionary metadata.  This is used to build the columns for the dictionary entries.  The names, validation,
   * and filter classes build the column definition.
   *
   * @param dictionary
   */
  private setUpDictionaryGrid(dictionary: any): void {

    if (isDevMode()) {
      console.debug("DictionaryEditorDetailComponent.setDictionary");
      console.debug(dictionary);
    }

    this.dictionary = dictionary;
    this.secondaryDictionaryEntries = [];
    let dictionaryClassNames = [];

    for (let field of dictionary.fields) {
      if (field.filterDictionaryClass) {
        dictionaryClassNames.push(field.filterDictionaryClass);
      }
    }

    this.getSecondaryDictionaries(dictionaryClassNames);
  }

  private getSecondaryDictionaries(dictionaryNames: string[]) {
    if (dictionaryNames.length == 0) {
      this.finishColumnSetup();
    } else {
      let dictionaryName = dictionaryNames.pop();
      this.dictionaryService.getDictionaryDropdownEntries(dictionaryName).pipe(
        takeUntil(this.destroy)
      ).subscribe(
        (response: any) => {
          this.secondaryDictionaryEntries.push({dictionaryName: dictionaryName, entries: response});
          this.getSecondaryDictionaries(dictionaryNames);
        }
      );
    }
  }

  private finishColumnSetup() {
    this.displayName = this.dictionary.displayName;

    // This establishes the readOnly functionality
    let cols: any[] = [];

    // Process Columns And Create Column Definition
    for (let field of this.dictionary.fields) {
      if (field.id && (isNullOrUndefined(field.code) || field.code == false)) {
        continue;
      }

      let col: any;

      if (field.filterDictionaryClass) {
        // this is a dropdown of another dictionary
        let options = this.secondaryDictionaryEntries.find(
          (a: any) => {
            return a.dictionaryName === field.filterDictionaryClass;
          }
        );

        col = {
          field: field.name,
          headerName: field.displayName,
          editable: this.isEditable.bind(this),
          cellRendererFramework: SelectRenderer,
          cellEditorFramework: SelectEditor,
          selectOptionsDisplayField: "display",
          selectOptionsValueField: "id",
          sortable: true,
          resizable: true,
          filter: true,
          filterValueGetter: this.comboFilterValueGetter,
          selectOptions: [],
          comparator: (valueA, valueB, nodeA, nodeB, isInverted) => {
            return 0;
          }
        };

        if (options && options.entries) {
          col.selectOptions = options.entries.sort(
            (optionA, optionB) => {
              if (optionA && !optionB) {
                return 1;
              }

              if (optionB && !optionA) {
                return -1;
              }

              if (!optionA && !optionB) {
                return 0;
              }

              if (optionA.display.toLowerCase() > optionB.display.toLowerCase()) {
                return 1;
              } else if ((optionB.display.toLowerCase() > optionA.display.toLowerCase())) {
                return -1;
              } else {
                return 0;
              }
            }
          );

          col.comparator = (valueA, valueB, nodeA, nodeB, isInverted) => {
            if (!options.entries) {
              return 0;
            }

            if (valueA && !valueB) {
              return 1;
            }

            if (valueB && !valueA) {
              return -1;
            }

            if (!valueA && !valueB) {
              return 0;
            }

            let optionA = options.entries.find((entry) => ("" + entry.id === "" + valueA));
            let optionB = options.entries.find((entry) => ("" + entry.id === "" + valueB));

            if (optionA && !optionB) {
              return 1;
            }

            if (optionB && !optionA) {
              return -1;
            }

            if (!optionA && !optionB) {
              return 0;
            }

            if (optionA.display.toLowerCase() > optionB.display.toLowerCase()) {
              return 1;
            } else if ((optionB.display.toLowerCase() > optionA.display.toLowerCase())) {
              return -1;
            } else {
              return 0;
            }
          };
        }

        col.cellStyle = params => {
          let column: any = {};

          for (let field of params.context.componentParent.dictionary.fields) {
            if (params.colDef.field == field.name) {
              column = field;
            }
          }

          if (params.data.editOrNew && params.data.editOrNew == "new") {
            if (params.context.readOnly || column.readOnly ||
              column.protectionType == "Read Only") {
              return {color: "#8a9499"};
            }
          } else {
            if (params.context.readOnly || column.readOnly ||
              column.protectionType == "Read Only" || column.createOnly ||
              column.protectionType == "Create Only" || column.code) {
              return {color: "#8a9499"};
            }
          }
        };

        col.displayOrder = field.displayOrder;
        col.dictionaryDisplay = field.dictionaryDisplay;
        col.dictionaryTooltip = field.dictionaryTooltip;

        // Set default filter as compare by text/number
        // col.filterRenderer = CompareFilterRenderer;
      } else if (field.type && field.type == "Date") {
        //date value
        col = {
          field: field.name,
          headerName: field.displayName,
          cellRendererFramework: DateRenderer,
          cellEditorFramework: AgGridDateCellEditorComponent,
          editable: this.isEditable.bind(this),
          sortable: true,
          resizable: true,
          filter: true,
          dictionaryDisplay: field.dictionaryDisplay
        };

        col.comparator = (valueA, valueB, nodeA, nodeB, isInverted) => {
          let num = this.dateComparator(valueA, valueB);
          return isInverted ? -1 * num : num; //don't have to do anything with inverted?
        };

        col.cellStyle = params => {
          var column: any = {};
          for (let field of params.context.componentParent.dictionary.fields) {
            if (params.colDef.field == field.name) {
              column = field;
            }
          }

          if (params.data.editOrNew && params.data.editOrNew == "new") {
            if (params.context.readOnly || column.readOnly ||
              column.protectionType == "Read Only") {
              return {color: "#8a9499"};
            }
          } else {
            if (params.context.readOnly || column.readOnly ||
              column.protectionType == "Read Only" || column.createOnly ||
              column.protectionType == "Create Only" || column.code) {
              return {color: "#8a9499"};
            }
          }
        };

        col.displayOrder = field.displayOrder;
      } else {
        // this is a string value
        col = {
          field: field.name,
          headerName: field.displayName,
          editable: this.isEditable.bind(this),
          sortable: true,
          resizable: true,
          filter: true,
          dictionaryDisplay: field.dictionaryDisplay
        };

        col.comparator = (valueA, valueB, nodeA, nodeB, isInverted) => {
          if (valueA && !valueB) {
            return 1;
          }

          if (valueB && !valueA) {
            return -1;
          }

          if (!valueA && !valueB) {
            return 0;
          }

          if (valueA.toLowerCase() > valueB.toLowerCase()) {
            return 1;
          } else if ((valueB.toLowerCase() > valueA.toLowerCase())) {
            return -1;
          } else {
            return 0;
          }
        };

        col.cellStyle = params => {
          var column: any = {};
          for (let field of params.context.componentParent.dictionary.fields) {
            if (params.colDef.field == field.name) {
              column = field;
            }
          }
          if (params.data.editOrNew && params.data.editOrNew == "new") {
            if (params.context.readOnly || column.readOnly ||
              column.protectionType == "Read Only") {
              return {color: "#8a9499"};
            }
          } else {
            if (params.context.readOnly || column.readOnly ||
              column.protectionType == "Read Only" || column.createOnly ||
              column.protectionType == "Create Only" || column.code) {
              return {color: "#8a9499"};
            }
          }
        };

        col.displayOrder = field.displayOrder;
        // Set default filter as compare by text/number
        // col.filterRenderer = CompareFilterRenderer;
      }

      cols.push(col);
    }

    // Now we need to sort the columns
    if (!this.dictionary.fields[0].hasOwnProperty("displayOrder")) {
      // default case, no displayOrder
      cols = cols.sort(
        (a, b) => {
          if (!a.headerName && b.headerName) {
            return 1;
          } else if (!b.headerName && a.headerName) {
            return -1;
          } else if (a.dictionaryDisplay === true && b.dictionaryDisplay === false) {
            return -1;
          } else if (b.dictionaryDisplay === true && a.dictionaryDisplay === false) {
            return 1;
          } else if (a.dictionaryTooltip === true && b.dictionaryTooltip === false) {
            return -1;
          } else if (b.dictionaryTooltip === true && a.dictionaryTooltip === false) {
            return 1;
          } else {
            return 0;
          }
        }
      );

    } else {
      // DisplayOrder case
      // I think this works? Check later
      cols = cols.sort(
        (a, b) => {
          if (!a.headerName && b.headerName) {
            return 1;
          } else if (!b.headerName && a.headerName) {
            return -1;
          } else {
            return a.displayOrder - b.displayOrder;
          }
        }
      );
    }

    // After Columns Have Been Sorted
    // Dictionary Grid Will Sort The Grid Ascending Order By First Column
    if(cols  && cols.length != 0) {
      cols[0].sort = "asc";
    }

    this.dataColumns = cols;
    if (Array.isArray(this.dataColumns) && this.gridOptions && this.gridOptions.api) {
      this.gridOptions.api.setColumnDefs(this.dataColumns);
      this.gridOptions.api.sizeColumnsToFit();
    }
    if (this.halfLoaded) {
      if (this.callQueued === true) {
        // A call completed while another call was queued so make that call now
        this.callQueued = false;
        this.getDictionaryBegin(this.className);
      } else {
        this.loadingSubjects[0].next(false);
      }
    } else {
      this.halfLoaded = true;
    }
  }

  isEditable(params): boolean {

    let isEditable: boolean;

    let column: any = {};
    for (let field of this.dictionary.fields) {
      if (params.colDef.field == field.name) {
        column = field;
      }
    }

    if (params.data.editOrNew && params.data.editOrNew == "new") {
      // console.log("new editable?");
      isEditable = !(this.dictionary.readOnly || column.readOnly ||
        column.protectionType == "Read Only");
    } else {
      // console.log("edit editable?");
      isEditable = !(this.dictionary.readOnly || column.readOnly ||
        column.protectionType == "Read Only" || column.createOnly ||
        column.protectionType == "Create Only" || column.code);
    }

    return isEditable;
  }

  deleteDictionaryEntries(): void {

    let deleteRows: any[] = this.gridOptions.api.getSelectedRows();

    for (let deleteId of this.selectedRows) {
      for (let entry of this.dictionaryEntries) {
        if (entry[this.idColumn] === deleteId) {
          deleteRows.push(entry);
          break;
        }
      }
    }

    this.dictionaryService.deleteDictionaryEntries(this.className, deleteRows).subscribe(
      (response: any) => {
        this.getDictionaryEntries();
      }
    );
  }

  updateDictionary(event: any): void {

    // Important to note this will not update cells to the demo but will when imported to CORE or other applications.
    this.dictionaryService.updateDictionaryEntries(this.className, [event.data]).subscribe(
      (data: any[]) => {
        this.getDictionaryEntries();
      }
    );
  }

  setSelectedEntries(): void {

    this.selectedRows = this.gridOptions.api.getSelectedRows();
  }

  newRowPost(data: any): Observable<any> {

    return this.dictionaryService.addDictionaryEntries(this.className, [data])
      .pipe(
        map(
          (data: Object[]) => {
            if (data && data.length === 1) {
              return <any>data[0];
            } else {
              return data;
            }
          }
        )
      );
  }

  public static onSamplesGridReady(event: GridReadyEvent): void {

    event.api.sizeColumnsToFit();
  }

  public static onGridSizeChanged(event: any) {

    if (event && event.api) {
      event.api.sizeColumnsToFit();
    }
  }

  public get disableAddRow(): boolean {

    if (!this.dictionary) {
      return true;
    }

    return this.dictionary.readOnly && this.dictionary.readOnly === true;
  }

  public get disableDelete(): boolean {

    if (!this.dictionary) {
      return true;
    }

    if (this.dictionary.readOnly && this.dictionary.readOnly === true) {
      return true;
    }

    if (this.gridOptions && this.gridOptions.api && this.gridOptions.api.getSelectedRows()) {
      return this.gridOptions.api.getSelectedRows().length === 0;
    } else {
      return true;
    }
  }

  private comboFilterValueGetter(params): any {

    let value = params.data[params.colDef.field];

    if (value) {
      let option = ((params.colDef as any).selectOptions as any[]).find((entry) => (entry[params.colDef.selectOptionsValueField] === value));
      return option ? option[params.colDef.selectOptionsDisplayField] : "";
    }

    return "";
  }

  onClickAddRow() {

    const modalRef = this.modalService.open(NewRowComponent,
      {
        size: "lg",
        keyboard: false,
        backdrop: "static",
        centered: true,
      }
    );

    modalRef.componentInstance.colDefs = this.dataColumns;
    modalRef.componentInstance.dictionary = this.dictionary;

    // if (!this.newRowSubscription) { // removed because it was preventing save after cancel
      // tell other ag-grid to add row to it
      this.newRowSubscription = modalRef.componentInstance.newRowSubject.subscribe(
        (theNewRow: any[]) => {
          this.dictionaryEntries = theNewRow.concat(this.dictionaryEntries);
          this.gridOptions.api.setRowData(this.dictionaryEntries);
          this.newRowSubscription = null;

          // Important to note this will not add rows to the demo but will when imported to CORE or other applications.
          this.newRowPost(theNewRow[0]).subscribe(
            () => {
              this.getDictionaryEntries();
            }
          );
        }
      );
    // }
  }

  /**
   * Comparater To Compare 2 Dates
   * @param date1 Date 1
   * @param date2 Date 2
   */
  dateComparator(date1, date2) {

    let date1Number = monthToComparableNumber(date1);
    let date2Number = monthToComparableNumber(date2);

    if (date1Number === null && date2Number === null) {
      return 0;
    }

    if (date1Number === null) {
      return -1;
    }

    if (date2Number === null) {
      return 1;
    }

    return date1Number - date2Number;
  }
}

/**
 * Extracts Date And Turn To A Numerical Value For Comparing Purposes
 * @param date The date to comparable number
 */
function monthToComparableNumber(date) {

  if (date === undefined || date === null || date.length !== 10) {
    return null;
  }

  let yearNumber = date.substring(0, 4);
  let monthNumber = date.substring(5, 7);
  let dayNumber = date.substring(8, 10);

  return yearNumber * 10000 + monthNumber * 100 + dayNumber;
}
