import {Component, HostBinding, OnInit, Renderer2, TemplateRef, ViewChild} from '@angular/core';
import {DictionaryService} from "@huntsman-cancer-institute/dictionary-service";
import {
  LiNavComponent,
  SearchListComponent,
  SearchListControllerComponent,
  SidebarComponent,
  UlNavComponent
} from "@huntsman-cancer-institute/navigation";
import {HciSortDto} from "@huntsman-cancer-institute/hci-ng-grid-dto";
import {BehaviorSubject} from "rxjs";

/**
 * The root component for the dictionary editor.  This defines a sidebar which will contain the list of dictionaries.
 * The sub route here will display a detail component when a dictionary is selected.
 *
 * @since 1.0.0
 */
@Component(
  {
    selector: "hci-dictionary-editor",
    template:
      `
        <hci-sidebar #sidebar></hci-sidebar>

        <router-outlet></router-outlet>

        <ng-template #dictionaryTemplate let-row="row">
          <div class="search-item" [routerLink]="['detail', row.className]" [routerLinkActive]="['active']">
            <div class="ml-3" [ngStyle]="{'color':row.readOnly === false ? '#005480' : '#8a9499'}">
              {{row.displayName}}
            </div>
          </div>
        </ng-template>
      `
    ,
    styles:
      [
        `
          .active .ml-3 {
            color: #ffffff !important;
          }

          :host::ng-deep .format-bottom-padding {
            padding-bottom: 10px;
          }

          :host::ng-deep .active {
            height: 30px;
          }

          :host::ng-deep .search-item {
            height: 30px;
            overflow: hidden;
            white-space: nowrap;
          }

          :host::ng-deep #filterReadOnly {
            float: left;
            display: flex;
            flex-direction: row-reverse;
          }

          :host::ng-deep #filterAndSortContainer {
            background-color: var(--greywarm-meddark) !important;
            height: 80px;
          }

          :host::ng-deep .results-count {
            height: 0 !important;
          }

          :host::ng-deep #filterReadOnly-li {
            font-size: 15px;
            height: 30px;
          }

          :host::ng-deep #SLCC {
            backgound-color: var(--greywarm-medlight) !important;
          }

          :host::ng-deep .search-button {
            color: var(--grey-darkest) !important;
            background-image: linear-gradient(white, white) !important;
          }

          :host::ng-deep .hci-dic-show-value {
            padding-right: 75px;
            color: var(--white-lightest);
          }
        `
      ]
  }
)
export class DictionaryEditorComponent implements OnInit {

  @HostBinding("class") classList: string = "outlet-row";

  @ViewChild(SidebarComponent, {static: true}) sidebar: SidebarComponent;

  @ViewChild("dictionaryTemplate", {read: TemplateRef, static: true}) dictionaryTemplate: any;

  filterReadOnlyDictionaries: boolean = true;

  sortDictionariesUp: boolean = true;

  constructor(private dictionaryService: DictionaryService, private renderer: Renderer2) {
  }

  dictionariesReference: Object [];

  dictionariesReferenceCopy: Object [];

  loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  /**
   * Upon creation, set up the sidebar and listen for the list of dictionaries from the service.
   */
  ngOnInit() {
    this.dictionariesReference = [];
    this.loadingSubject.next(true);
    this.setDictionarySideBarConfiguration();

    // Makes Call To Get The Dictionaries
    this.dictionaryService.getDictionaries().subscribe(
      theData => {

        this.dictionariesReference = theData.filter(this.checkInternalOnly);

        this.dictionariesReference.sort(
          (dictionaryA: any, dictionaryB: any) => (dictionaryA.displayName < dictionaryB.displayName ? -1 : 1)
        );

        this.dictionariesReferenceCopy = this.dictionariesReference;
        this.dictionariesReference = this.dictionariesReference.filter(this.checkReadOnly);
        this.setDictionarySideBarConfiguration();
        this.loadingSubject.next(false);
      }
    );
  }

  /*************************************************
   * Helper Method To Configure Dictionary Side Bar
   *************************************************/
  private setDictionarySideBarConfiguration() {

    let resultMessage =
      this.dictionariesReference.length == 0
        ? ""
        : this.dictionariesReference.length + " Results";

    this.sidebar.setConfig(
      {
        sidebarSize: 240,
        showCollapse: false,
        children:
          [
            {
              type: SearchListControllerComponent,
              id: "SLCC",
              showTitle: false,
              filterByAllowedFields: ["displayName"],
              sortByAllowedFields: ["displayName"],
              loadingSubjects: [this.loadingSubject]
            },
            {
              type: UlNavComponent,
              ulClass: "hci-dic-ul-class",
              id: "filterAndSortContainer",
              children:
                [
                  {
                    type: LiNavComponent,
                    id: "filterReadOnly",
                    title: "Show All",
                    spanClass: "hci-dic-span-1",
                    liClass: "hci-dic-li-l",
                    iClass: "hci-dic-i",
                    ulClass: "hci-dic-ul",
                    expandable: true,
                    iRight: false,
                    showValueFlag: true,
                    showValue: resultMessage,
                    showValueClass: "hci-dic-show-value",
                    liClick: () => {
                      this.filter();
                      let SLC = this.sidebar.getChild("dictionaries-component") as SearchListComponent;
                      SLC.setData(this.dictionariesReference);
                    }
                  },
                  {
                    type: LiNavComponent,
                    id: "sort",
                    title: "",
                    spanClass: "hci-dic-span-2",
                    liClass: "hci-dic-li-r",
                    iClass: "fa-solid fa-arrow-down-a-z hci-dic-i",
                    ulClass: "hci-dic-ul",
                    expandable: true,
                    iRight: true,
                    liClick: (click: MouseEvent) => {
                      let SLCC = <SearchListControllerComponent>this.sidebar.getChild("SLCC");
                      SLCC.setSort(click, "displayName");
                      this.sortDictionariesUp = !this.sortDictionariesUp;
                      let sort = this.sidebar.getChild("filterAndSortContainer").getChild("sort");

                      if (this.sortDictionariesUp) {
                        sort.config.iClass = "fas fa-solid fa-arrow-down-a-z";
                      } else {
                        sort.config.iClass = "fa-solid fa-arrow-up-a-z";
                      }

                      sort.updateConfig(sort.config);
                    }
                  }
                ]
            },
            {
              type: SearchListComponent,
              id: "dictionaries-component",
              title: "Dictionaries",
              itemTemplate: this.dictionaryTemplate,
              sorts: [new HciSortDto("displayName", true)],
              data: this.dictionariesReference,
              loadingSubjects: [this.loadingSubject]
            }
          ]
      }
    );
  }

  filter() {
    this.filterReadOnlyDictionaries = !this.filterReadOnlyDictionaries;
    let FRO = this.sidebar.getChild("filterAndSortContainer").getChild("filterReadOnly");

    if (this.filterReadOnlyDictionaries) {
      this.dictionariesReference = this.dictionariesReference.filter(this.checkReadOnly);
      FRO.config.spanClass = "hci-dic-span-1";
      FRO.config.showValue = (FRO.config.showValueFlag) ? this.dictionariesReference.length + " Result(s)" : "";
      FRO.updateConfig(FRO.config);
    } else {
      this.dictionariesReference = this.dictionariesReferenceCopy;
      FRO.config.spanClass = "hci-dic-span-3";
      FRO.config.showValue = (FRO.config.showValueFlag) ? this.dictionariesReference.length + " Result(s)" : "";
      FRO.updateConfig(FRO.config);
    }
  }

  /*******************************************************
   * Helper Method To Determine If Dictionary Is Read Only
   *******************************************************/
  checkReadOnly(dictionary) {
    return !dictionary.readOnly;
  }

  /***********************************************************
   * Helper Method To Determine If Dictionary Is Internal Only
   ***********************************************************/
  checkInternalOnly(dictionary) {
    return !dictionary.internalOnly;
  }
}
