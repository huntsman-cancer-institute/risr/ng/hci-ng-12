import {
  AfterViewInit, ChangeDetectorRef, Component, HostBinding, Input, isDevMode,
  ViewChild, OnDestroy
} from "@angular/core";
import {NgForm} from "@angular/forms";

import { Observable, Subscription } from "rxjs";
import {finalize} from "rxjs/operators";

import {CrudService} from "../services/crud.service";

/**
 * This is a generic component that is designed to work with the crud api.  The component builds itself based on the
 * metadata.  This component could be independent so it has its own form and can interact with the crud api on its own.
 *
 * "id": false,
 * "field": "idNameType",
 * "type": "Long",
 * "notNull": true,
 * "min": null,
 * "max": null,
 * "dictionaryClassName": "hci.ri.core.subject.model.NameType",
 * "phi": false
 *
 * @since 10.0.0
 */
@Component({
  selector: "hci-crud",
  template: `
    <form #crudForm="ngForm"
          class="d-flex flex-nowrap flex-row flex-grow-1">
      <div class="d-flex flex-nowrap flex-column flex-grow-1">
        <hci-busy [busy]="[busy]" [showIcon]="false"></hci-busy>
        <ng-container *ngFor="let fieldArray of fields">
          <div class="d-flex flex-nowrap mb-1">
            <ng-container *ngFor="let field of fieldArray">
              <input *ngIf="field.metadata.type === 'String'" attr.aria-label="hci-ng-crud-input-string-{{field.metadata.field}}"
                     [(ngModel)]="field.data"
                     type="text"
                     [id]="field.metadata.field"
                     [name]="field.metadata.field"
                     [disabled]="!editable"
                     [placeholder]="field.metadata.field"
                     [minLength]="field.metadata.min"
                     [maxLength]="field.metadata.max"
                     [required]="field.metadata.notNull"
                     class="crud-input"
                     [ngStyle]="field.styles"
                     [class.phi]="field.metadata.phi" />
              <input *ngIf="!field.metadata.dictionaryClassName && (field.metadata.type === 'Integer' || field.metadata.type === 'Long')"
                     [(ngModel)]="field.data" attr.aria-label="hci-ng-crud-input-number-{{field.metadata.field}}"
                     type="number"
                     [id]="field.metadata.field"
                     [name]="field.metadata.field"
                     [disabled]="!editable"
                     [placeholder]="field.metadata.field"
                     [minLength]="field.metadata.min"
                     [maxLength]="field.metadata.max"
                     [required]="field.metadata.notNull"
                     class="crud-input"
                     [ngStyle]="field.styles"
                     [class.phi]="field.metadata.phi" />
              <hci-native-select *ngIf="field.metadata.dictionaryClassName"
                                 [(ngModel)]="field.data"
                                 [id]="field.metadata.field"
                                 [name]="field.metadata.field"
                                 [url]="field.metadata.dictionaryClassName"
                                 [required]="field.metadata.required"
                                 class="crud-input"
                                 [ngStyle]="field.styles">
              </hci-native-select>
            </ng-container>
          </div>
        </ng-container>
      </div>
      <button *ngIf="saveable && !crudForm.form.dirty"
              (click)="save()"
              class="btn btn-icon ml-1"
              [disabled]="true">
        <i class="far fa-circle fa-lg"></i>
      </button>
      <button *ngIf="saveable && crudForm.form.dirty && crudForm.form.valid && !warning"
              (click)="save()"
              class="btn btn-icon btn-green ml-1">
        <span *ngIf="!busy">
          <i class="fas fa-check fa-lg"></i>
        </span>
        <span *ngIf="busy">
          <i class="fas fa-spinner fa-lg fa-spin"></i>
        </span>
      </button>
      <button *ngIf="saveable && warning"
              [ngbPopover]="warning"
              triggers="mouseenter:mouseleave"
              popoverClass="warn"
              (click)="save()"
              class="btn btn-icon btn-red ml-1">
        <i class="fas fa-exclamation-circle fa-lg"></i>
      </button>
      <button *ngIf="saveable && crudForm.form.dirty && !crudForm.form.valid && !warning"
              [disabled]="true">
              class="btn btn-icon btn-red ml-1">
        <i class="fas fa-times fa-lg"></i>
      </button>
    </form>
  `,
  styles: [`

    .crud-input {
      width: 100%;
    }

    .crud-input:first-child {
      border-top-left-radius: 0.25rem;
      border-bottom-left-radius: 0.25rem;
    }

    .crud-input:last-child {
      border-top-right-radius: 0.25rem;
      border-bottom-right-radius: 0.25rem;
    }

    .ng-invalid {
      background-color: rgba(255, 100, 100, 0.25);
    }

    .phi {
      box-shadow: #cc7700 1px 1px 1px inset;
      border-color: orange;
    }

    ::ng-deep .hci-dropdown {
      min-width: 20%;
    }

  `]
})
export class CrudComponent implements AfterViewInit, OnDestroy {

  @HostBinding("class") classList: string = "d-flex flex-nowrap";

  @Input() saveable: boolean = true;
  @Input() editable: boolean = true;
  @Input() className: string;
  @Input("fields") displayFields: string[] = [];
  @Input() ids: any[];
  @Input() data: any = {};
  @Input() styles: any[];

  @ViewChild("crudForm", {static: true}) form: NgForm;

  id: string;
  metadata: any = {};
  fields: any[];
  busy: boolean = false;
  warning: string;

  metadataSubscription: Subscription;

  constructor(private crudService: CrudService,
              private changeDetectorRef: ChangeDetectorRef) {}

  ngAfterViewInit(): void {
    this.metadataSubscription = this.crudService.getMetadata(this.className).subscribe((metadata: any) => {
      this.fields = [];

      for (let displayFieldArray of this.displayFields) {
        let fieldArray: any[] = [];

        for (let displayField of displayFieldArray) {
          for (let metadataField of metadata.fields) {
            if (displayField === metadataField.field) {
              let field: any = {
                data: undefined,
                metadata: metadataField,
                styles: undefined
              };

              if (this.styles) {
                for (let fieldStyle of this.styles) {
                  if (displayField === fieldStyle.field) {
                    field.styles = fieldStyle.styles;
                  }
                }
              }

              fieldArray.push(field);
              break;
            }
          }
        }

        this.fields.push(fieldArray);
      }

      for (let metadataField of metadata.fields) {
        if (metadataField.id) {
          this.id = metadataField.field;
          break;
        }
      }

      this.metadata = metadata;

      this.refreshData();
      this.changeDetectorRef.detectChanges();
    });
  }

  ngOnDestroy(): void {
    this.metadataSubscription.unsubscribe();
  }

  /**
   * Push the data from the bound inputs to the data representing this crud class.
   */
  createData(): void {
    if (!this.data) {
      this.data = {};
    }

    if (this.ids) {
      for (let id of this.ids) {
        this.data[id.key] = id.value;
      }
    }

    for (let fieldArray of this.fields) {
      for (let field of fieldArray) {
        this.data[field.metadata.field] = field.data;
      }
    }
  }

  /**
   * If data exists, set it and refresh the bound data.
   *
   * @param data
   */
  setData(data: any): void {
    if (isDevMode()) {
      console.debug("CrudComponent.setData: " + this.className);
      console.debug(data);
    }

    if (data !== undefined) {
      this.data = data;
      this.refreshData();
    } else {
      this.data = {};
    }
  }

  /**
   * Pushes the data to the fields for binding to the form inputs.
   */
  refreshData(): void {
    if (this.data && this.fields) {
      for (let fieldArray of this.fields) {
        for (let field of fieldArray) {
          field.data = this.data[field.metadata.field];
        }
      }

      this.changeDetectorRef.detectChanges();
    }
  }

  /**
   * Gets a save request and refreshes the data upon a successful call.
   */
  save(): void {
    this.busy = true;
    this.warning = undefined;

    this.getSaveRequest()
      .pipe(finalize(() => {
        this.busy = false;
      }))
      .subscribe((data: Object) => {
        this.setData(data);
        this.form.form.markAsPristine();
      },
      (error) => {
        this.warning = error;
        console.error("CrudComponent.save: " + this.className + ": " + error);
      });
  }

  /**
   * Return an observable from the crud post or put.  This is used such that the crud container can manage the request
   * for this component.
   *
   * @returns {Observable<Object>}
   */
  getSaveRequest(): Observable<Object> {
    this.createData();

    if (this.data[this.id]) {
      return this.crudService.put(this.className, this.data[this.id], this.data);
    } else {
      return this.crudService.post(this.className, this.data);
    }
  }
}
