import {ChangeDetectorRef, Component, HostBinding, Input, isDevMode, QueryList, ViewChildren} from "@angular/core";

import {CrudService} from "../services/crud.service";
import {CrudComponent} from "./crud.component";
import {Observable} from "rxjs/Observable";
import {forkJoin} from "rxjs/Observable/forkJoin";

/**
 * This is a generic component that is designed to work with the crud api.  The component builds itself based on the
 * metadata.  It ties together multiple crud components.  For example, Subject or StudySubject may have names, phone
 * numbers, or addresses associated with it.  Each one corresponding to its own CRUD rest endpoint.
 *
 * @since 10.0.0
 */
@Component({
  selector: "hci-crud-container",
  template: `
    <ng-container *ngFor="let crudGroup of crudGroups" class="crud-group">
      <div *ngIf="crudGroup.mode === 'singular'"
           class="d-flex crud-group p-2">
        <h4 style="flex: 0 0 15%;">{{crudGroup.display}}</h4>
        <hci-crud [className]="crudGroup.className"
                  [fields]="crudGroup.displayFields"
                  [styles]="crudGroup.styles"
                  [ids]="ids"
                  [data]="crudGroup.data"
                  [saveable]="saveable"
                  style="flex: 1 1 85%;"></hci-crud>
      </div>
    </ng-container>
  `,
  styles: [`
  
    .crud-group {
      background-color: #cfecfc;
      margin-bottom: 1rem;
      border-radius: 0.5rem;
      border: #3d7a99 1px solid;
    }
    
  `]
})
export class CrudContainerComponent {

  @HostBinding("class") classList: string = "d-flex flex-column";

  @Input() data: any;
  @Input() saveable: boolean = true;
  @Input() crudGroups: any[];
  @Input() ids: any[];

  @ViewChildren(CrudComponent) crudComponents: QueryList<CrudComponent>;

  constructor(private crudService: CrudService,
              private changeDetectorRef: ChangeDetectorRef) {}

  setData(data: any): void {
    if (isDevMode()) {
      console.debug("CrudContainerComponent.setData");
      console.debug(data);
    }

    this.data = data;

    this.crudComponents.forEach((crud: CrudComponent) => {
      for (let crudGroup of this.crudGroups) {
        if (crudGroup.className === crud.className && crudGroup.dataField) {
          crud.setData(this.data[crudGroup.dataField]);
          break;
        }
      }
    });
  }

  save(): void {
    this.crudComponents.forEach((crud: CrudComponent) => {
      crud.save();
    });
  }

  saveJoin(): Observable<any> {
    let requests: Observable<any>[] = [];

    this.crudComponents.forEach((crud: CrudComponent) => {
      requests.push(crud.getSaveRequest());
    });

    return forkJoin(requests);
  }
}
