export {CrudModule} from "./crud.module";
export {CrudService} from "./services/crud.service";

export {CRUD_ENDPOINT} from "./services/crud.service";

export {CrudContainerComponent} from "./components/crud-container.component";
export {CrudComponent} from "./components/crud.component";

export {FieldViewablePipe} from "./pipes/field-viewable.pipe";
