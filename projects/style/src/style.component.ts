import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '@hci-style',
  template: '',
  styleUrls: ['./core.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StyleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
