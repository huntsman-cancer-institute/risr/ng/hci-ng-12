import {Pipe, PipeTransform} from "@angular/core";

import {GraphicalAttribute} from "../model/graphical-attribute.entity";

/**
 * A filter for attributes in a container.  All attributes have the id of the container even those that belong
 * to a grid attribute.  However, when we populate the container, we don't want to render those grid attributes.
 * The grid will handle those itself.
 */
@Pipe({
  name: "isGroupAttribute",
  pure: false
})
export class IsGroupAttributePipe implements PipeTransform {

  transform(list: GraphicalAttribute[], returnGroup: boolean): GraphicalAttribute[] {
    if (!list) {
      return [];
    }

    return list.filter((attribute: GraphicalAttribute) => {
      if (attribute.idGroupAttribute) {
        return returnGroup;
      } else {
        return !returnGroup;
      }
    });
  }
}
