import {
  ChangeDetectorRef, Component, ContentChildren, ElementRef, Input, isDevMode, TemplateRef,
  QueryList, Renderer2, SimpleChange, ViewChild, ViewChildren, OnInit, OnDestroy, HostBinding
} from "@angular/core";

import { Subject, Subscription } from "rxjs";
import {NgbAccordion, NgbModal, NgbPanel} from "@ng-bootstrap/ng-bootstrap";

import {AccordionNavComponent} from "@huntsman-cancer-institute/misc";

import {AttributeService} from "../services/attribute.service";
import {AttributeConfiguration} from "../model/attribute-configuration.entity";
import {AttributeValueSet} from "../model/attribute-value-set.entity";
import {AttributeContainer} from "../model/attribute-container.entity";

/**
 * This component should be added on to any screen that displays an entity with an idAttributeValueSet.  This configuration
 * represents the parent of separate containers, which in turn contain attributes which display those values in the
 * attributeValueSet.
 */
@Component({
  selector: "hci-attribute-configuration",
  template: `
    <hci-busy [busySubjects]="[loadingSubject]"></hci-busy>

    <ng-container *ngIf="attributeConfiguration && attributeValueSet">
      <ngb-accordion #accordion="ngbAccordion" class="y-auto">
        <ng-content></ng-content>
        <ng-container *ngFor="let attributeContainer of attributeConfiguration.attributeContainers">
          <ngb-panel [id]="'id-attribute-container-' + attributeContainer.idAttributeContainer" [title]="attributeContainer.containerName">
            <ng-template ngbPanelTitle>
              <div *ngIf="editPopup" [id]="'id-attribute-container-' + attributeContainer.idAttributeContainer + '-header'"
                   class="d-flex flex-grow-1 attribute-container-header {{'sort-order-' + attributeContainer.sortOrder}}">
                <div class="ml-auto mr-0" (click)="edit(editModal, attributeContainer)">
                  <i class="fas fa-pencil-alt"></i>
                </div>
              </div>
            </ng-template>
            <ng-template ngbPanelContent>
              <div class="attribute-container"
                   [class.col-md-12]="attributeContainer.isAutoLayout && attributeContainer.isAutoLayout === 'Y'"
                   [class.flex]="attributeContainer.isAutoLayout && attributeContainer.isAutoLayout === 'Y'"
                   [class.flex-wrap]="attributeContainer.isAutoLayout && attributeContainer.isAutoLayout === 'Y'"
                   [class.absolute]="!attributeContainer.isAutoLayout || attributeContainer.isAutoLayout === 'N'"
                   [class.x-auto]="!attributeContainer.isAutoLayout || attributeContainer.isAutoLayout === 'N'"
                   [style.height.px]="(!attributeContainer.isAutoLayout || attributeContainer.isAutoLayout === 'N') ? windowDimension.height : 'auto'">
                <ng-container *ngFor="let attribute of attributeContainer.graphicalAttributes | isGroupAttribute: false">
                  <ng-container *ngIf="attributeContainer.isAutoLayout && attributeContainer.isAutoLayout === 'Y' && attribute.codeAttributeDataType !== 'LINE'">
                    <hci-attribute-flex [id]="'id-attribute-' + attribute.idAttribute"
                                        [attribute]="attribute"
                                        [editInline]="editInline"
                                        [class.attribute]="true"
                                        [class.col-4]="attribute.codeAttributeDataType !== 'GA' && attribute.codeAttributeDataType !== 'LINE'"
                                        [class.col-12]="attribute.codeAttributeDataType === 'GA' || attribute.codeAttributeDataType === 'LINE'"></hci-attribute-flex>
                  </ng-container>
                  <ng-container *ngIf="!attributeContainer.isAutoLayout || attributeContainer.isAutoLayout === 'N'">
                    <hci-attribute-absolute [id]="'id-attribute-' + attribute.idAttribute"
                                            [attribute]="attribute"
                                            [editInline]="editInline"
                                            [class.attribute]="true"></hci-attribute-absolute>
                  </ng-container>
                </ng-container>
              </div>
            </ng-template>
          </ngb-panel>
        </ng-container>
      </ngb-accordion>
    </ng-container>

    <ng-template #editModal let-close="close">
      <div class="modal-header">
        {{editContainer.containerName}}
      </div>
      <div class="modal-body d-flex flex-column hci-cod-edit">
        <ng-container *ngFor="let attribute of editContainer.graphicalAttributes | isGroupAttribute: false">
          <hci-attribute-edit [id]="'edit-id-attribute-' + attribute.idAttribute"
                              [attribute]="attribute"
                              class="attribute"></hci-attribute-edit>
        </ng-container>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" (click)="close('Save')">Save</button>
        <button class="btn btn-primary" (click)="close('Cancel')">Cancel</button>
      </div>
    </ng-template>
  `
})
export class AttributeConfigurationComponent implements OnInit, OnDestroy {
  @HostBinding("class") classList: string = "hci-attribute-configuration hci-cod d-flex flex-column flex-grow-1";

  @Input() idAttributeValueSet: number;
  @Input() idParentObject: number;

  @Input() accordionNav: AccordionNavComponent;

  @Input() editInline: boolean = true;
  @Input() editPopup: boolean =  false;
  @Input() editable: boolean = true;

  @ViewChildren(NgbPanel) vPanels: QueryList<NgbPanel>;
  @ContentChildren(NgbPanel) cPanels: QueryList<NgbPanel>;

  attributeConfiguration: AttributeConfiguration;
  attributeValueSet: AttributeValueSet;

  loadingSubject: Subject<boolean> = new Subject<boolean>();

  editContainer: AttributeContainer;

  windowDimension: any = {};

  subscriptions: Subscription = new Subscription();

  constructor(private attributeService: AttributeService,
              private elementRef: ElementRef,
              private renderer: Renderer2,
              private changeDetectorRef: ChangeDetectorRef,
              private modalService: NgbModal) {}

  /**
   * Upon init, subscribe to the configuration and value set.
   */
  ngOnInit() {
    if (! this.editable) {
      this.editInline = false;
      this.editPopup = false;
    }

    this.loadingSubject = this.attributeService.getLoadingSubject();

    this.subscriptions.add(this.attributeService.getAttributeConfigurationSubject().subscribe((attributeConfiguration: AttributeConfiguration) => {
      this.attributeConfiguration = attributeConfiguration;

      this.attributeService.setAttributeValueSet(this.idAttributeValueSet, this.idParentObject);
    }));

    this.subscriptions.add(this.attributeService.attributeConfigurationDimensionSubject.subscribe((windowDimension: any) => {
      this.windowDimension = windowDimension;
    }));

    this.subscriptions.add(this.attributeService.getAttributeValueSet().subscribe((attributeValueSet: AttributeValueSet) => {
      this.attributeValueSet = attributeValueSet;

      if (this.attributeValueSet) {
        this.attributeService.notifyAttributes();
      }
    }));
  }

  @Input() set boundData(value: any) {
    this.attributeService.setBoundData(value);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  @ViewChild(NgbAccordion, {static: false})
  set accordion(accordion: NgbAccordion) {
    if (accordion) {
      accordion.panels.reset([this.cPanels.toArray(), this.vPanels.toArray()]);
      accordion.ngAfterContentChecked();

      if (this.accordionNav && !this.accordionNav.accordion) {
        this.accordionNav.setAccordion(accordion);
      }

      this.changeDetectorRef.detectChanges();
    }
  }

  ngOnChanges(changes: {[propName: string]: SimpleChange}) {
    if (! this.editable) {
      this.editInline = false;
      this.editPopup = false;
    }

    if (changes["accordionNav"] && this.accordionNav && this.accordion) {
      this.accordionNav.accordion = this.accordion;
      this.accordionNav.panels = this.accordion.panels;
    }
  }

  getAttributeService(): AttributeService {
    return this.attributeService;
  }

  edit(modal: TemplateRef<any>, editContainer: AttributeContainer): void {
    this.editContainer = editContainer;

    this.modalService.open(modal, {windowClass: "modal-lg"}).result.then((result) => {
      if (result === "Save") {
        this.attributeService.updateAttributeValueSet();
      } else if (result === "Cancel") {
        this.attributeService.clearUpdatedAttributeValues();
      }
    }, (reason) => {});
  }

  post(): void {
    this.attributeService.updateAttributeValueSet();
  }

}
