import {Component, forwardRef, Input} from "@angular/core";

import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

import {GraphicalAttribute} from "../model/graphical-attribute.entity";

@Component({
  selector: "hci-attribute-default",
  template: `
    <!-- String -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'S'">
      <input type="text"
             class="form-control"
             [id]="id"
             [name]="name"
             [(ngModel)]="value" attr.aria-label="hci-ng-string-attribute-default-{{name}}">
    </ng-container>

    <!-- Text -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'TXT'">
      <textarea type="text"
                class="form-control"
                [id]="id"
                [name]="name"
                spellcheck="spellcheck"
                lang="en"
                [(ngModel)]="value">
      </textarea>
    </ng-container>

    <!-- Checkbox -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'CB'">
      <input type="checkbox"
             class="form-control"
             [id]="id"
             [name]="name"
             [(ngModel)]="value"
             [checked]="value && value.toUpperCase() === 'Y'" attr.aria-label="hci-ng-checkbox-attribute default-{{name}}">
    </ng-container>

    <!-- Numeric -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'N'">
      <input type="number"
             class="form-control"
             [id]="id"
             [name]="name"
             [(ngModel)]="value" attr.aria-label="hci-ng-numeric-attribute default-{{name}}">
    </ng-container>

    <!-- Integer -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'I'">
      <input type="number"
             class="form-control"
             [id]="id"
             [name]="name"
             [(ngModel)]="value" attr.aria-label="hci-ng-number-attribute default-{{name}}">
    </ng-container>

    <!-- Date -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'D'">
      <input type="date"
             class="form-control"
             [id]="id"
             [name]="name"
             [(ngModel)]="value" attr.aria-label="hci-ng-date-attribute default-{{name}}">
    </ng-container>

    <!-- Date Time -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'DT'">
      <input type="date"
             class="form-control"
             [id]="id"
             [name]="name"
             [(ngModel)]="value" attr.aria-label="hci-ng-date-time-attribute default-{{name}}">
    </ng-container>

    <!-- Boolean -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'B'">
      <select [(ngModel)]="value"
             class="form-control edit-renderer"
             [id]="id"
             [name]="name">
        <option [ngValue]="'Y'" [selected]="value && value.toUpperCase() === 'Y'">Y</option>
        <option [ngValue]="'N'" [selected]="value && value.toUpperCase() === 'N'">N</option>
      </select>
    </ng-container>

    <!-- Extended Boolean -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'EB'">
      <select [(ngModel)]="value"
              class="form-control edit-renderer"
              [id]="id"
              [name]="name">
        <option [ngValue]="'Y'" [selected]="value && value.toUpperCase() === 'Y'">Y</option>
        <option [ngValue]="'N'" [selected]="value && value.toUpperCase() === 'N'">N</option>
        <option [ngValue]="'U'" [selected]="value && value.toUpperCase() === 'U'">U</option>
      </select>
    </ng-container>

    <!-- Choice: Single -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'AC' && attribute.isMultiValue.toUpperCase() === 'N'">
      <select [(ngModel)]="value"
              class="form-control edit-renderer"
              [id]="id"
              [name]="name">
        <option *ngFor="let attributeChoice of attribute.attributeChoices"
                [ngValue]="attributeChoice"
                [selected]="value && value.toUpperCase() === attributeChoice.idAttributeChoice.toString().toUpperCase()">
          {{ attributeChoice.choice }}
        </option>
      </select>
    </ng-container>

    <!-- Choice: Multi -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'AC' && attribute.isMultiValue.toUpperCase() === 'Y'">
      <input type="text"
             class="form-control"
             [id]="id"
             [name]="name"
             disabled attr.aria-label="hci-ng-choice-multiple-attribute default-{{name}}">
    </ng-container>

    <!-- Dictionary -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'DICT'">
      <hci-native-select [(ngModel)]="value"
                         [className]="attribute.attributeDictionary.className"
                         [id]="id"
                         [name]="name">
      </hci-native-select>
    </ng-container>

    <!-- Grid -->
    <ng-container *ngIf="attribute.codeAttributeDataType.toUpperCase() === 'GA'">
      <input type="text"
             class="form-control"
             [id]="id"
             [name]="name"
             disabled attr.aria-label="hci-ng-grid-attribute default-{{name}}">
    </ng-container>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AttributeDefaultComponent),
      multi: true
    }
  ]
})
export class AttributeDefaultComponent implements ControlValueAccessor {

  _value: string = "";

  @Input() id: string;
  @Input() name: string;
  @Input() attribute: GraphicalAttribute;

  onChange: any = (_: any) => {};
  onTouched: any = () => {};

  get value(): string {
    return this._value;
  };

  set value(v: string) {
    this.onTouched();
    if (v !== this.value) {
      this._value = v;
      this.onChange(v);
    }
  }

  constructor() {}

  ngOnInit(): void {}

  writeValue(v: string) {
    this._value = v;
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }
}
