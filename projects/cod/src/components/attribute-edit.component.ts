import {
  Component,
  ElementRef,
  Renderer2
} from '@angular/core';

import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AttributeBase} from './attribute-base';
import {AttributeService} from '../services/attribute.service';

/**
 * This component is specifically designed to exist in a modal for editing.
 * This is different from the flex attribute because components such as the grid need extra configuration for editing.
 */
@Component(
  {
    selector: 'hci-attribute-edit',
    template:
      `
        <!-- String -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'S'">
          <div #attributeRef
               class="d-flex flex-row">
            <div *ngIf="attribute.displayName" class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>

            <div *ngIf="(attribute.h == undefined || (attribute.h && attribute.h <= 25))" class="d-flex col-md-6">
              <input #inputRef
                     type="text"
                     [ngModel]="attributeValues[0].valueString"
                     (ngModelChange)="valueStringChange($event)"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'" attr.aria-label="hci-ng-attribute-edit-input-string-{{hciNgAttributeAbsoluteInputDisplayName}}"
              />
            </div>

            <div *ngIf="(attribute.h && attribute.h > 25)" class="d-flex col-md-6">
              <textarea
                #inputRef
                type="text"
                spellcheck="spellcheck"
                lang="en"
                [ngModel]="attributeValues[0].valueString"
                (ngModelChange)="valueStringChange($event)"
                [disabled]="!editInline || attribute.isCalculated === 'Y'"
                style="width: 500px; height: 125px; resize: none;"
              >
              </textarea>
            </div>

          </div>
        </ng-container>

        <!-- Text -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'TXT'">
          <div #attributeRef
               class="d-flex flex-row">
            <div class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>
            <div class="d-flex col-md-6">

          <textarea
            #inputRef
            type="text"
            spellcheck="spellcheck"
            lang="en"
            [ngModel]="attributeValues[0].valueLongText.textData"
            (ngModelChange)="valueTextChange($event)"
            [disabled]="!editInline || attribute.isCalculated === 'Y'"
            style="width: 500px; height: 125px; resize: none;"
          >
          </textarea>

            </div>
          </div>
        </ng-container>

        <!-- Numeric -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'N'">
          <div #attributeRef
               class="d-flex flex-row">
            <div *ngIf="attribute.displayName" class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>
            <div class="d-flex col-md-6">
              <input #inputRef
                     type="number"
                     [ngModel]="attributeValues[0].valueNumeric"
                     (ngModelChange)="valueNumericChange($event)" attr.aria-label="hci-ng-attribute-edit-input-numeric-{{hciNgAttributeAbsoluteInputDisplayName}}"/>
            </div>
          </div>
        </ng-container>

        <!-- Integer -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'I'">
          <div #attributeRef
               class="d-flex flex-row">
            <div *ngIf="attribute.displayName" class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>
            <div class="d-flex col-md-6">
              <input #inputRef
                     type="number"
                     [ngModel]="attributeValues[0].valueInteger"
                     (ngModelChange)="valueIntegerChange($event)" attr.aria-label="hci-ng-attribute-edit-input-integer-{{hciNgAttributeAbsoluteInputDisplayName}}"/>
            </div>
          </div>
        </ng-container>

        <!-- Date -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'D'">
          <div #attributeRef
               class="d-flex flex-row">
            <div *ngIf="attribute.displayName" class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>
            <div class="d-flex col-md-6">
              <input #inputRef
                     matInput
                     name="valueDate"
                     [(ngModel)]="attributeValues[0].valueDate"
                     (ngModelChange)="valueDateChange($event)"
                     [matDatepicker]="valueDate"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'" attr.aria-label="hci-ng-attribute-edit-input-date-{{hciNgAttributeAbsoluteInputDisplayName}}"
              >
              <mat-datepicker-toggle matSuffix [for]="valueDate" class="cod-dp-toggle"></mat-datepicker-toggle>
              <mat-datepicker #valueDate></mat-datepicker>
            </div>
          </div>
        </ng-container>

        <!-- Date Time -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'DT'">
          <div #attributeRef
               class="d-flex flex-row">
            <div *ngIf="attribute.displayName" class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>
            <div class="d-flex col-md-6">
              <input #inputRef
                     matInput
                     name="valueDateTime"
                     [(ngModel)]="attributeValues[0].date"
                     (ngModelChange)="valueDateChange($event)"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     [ngxMatDatetimePicker]="dtpicker" attr.aria-label="hci-ng-attribute-edit-input-date-time-{{hciNgAttributeAbsoluteInputDisplayName}}">
              <mat-datepicker-toggle matSuffix [for]="dtpicker" class="cod-dp-toggle"></mat-datepicker-toggle>
              <ngx-mat-datetime-picker #dtpicker [showSeconds]="true"></ngx-mat-datetime-picker>
            </div>
          </div>
        </ng-container>

        <!-- Checkbox -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'CB'">
          <div #attributeRef
               class="d-flex flex-row">
            <div *ngIf="attribute.displayName" class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>
            <div class="d-flex col-md-6">
              <input #inputRef
                     type="checkbox"
                     [checked]="attributeValues[0].valueString === 'Y'"
                     (change)="valueCheckboxChange($event)"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     class="form-control" attr.aria-label="hci-ng-attribute-edit-input-checkbox-{{hciNgAttributeAbsoluteInputDisplayName}}"/>
            </div>
          </div>
        </ng-container>

        <!-- Boolean -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'B'">
          <div #attributeRef
               class="d-flex flex-row">
            <div *ngIf="attribute.displayName" class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>
            <div class="d-flex col-md-6">
              <select [ngModel]="attributeValues[0].valueString"
                      (ngModelChange)="valueStringChange($event)"
                      class="edit-renderer">
                <option [ngValue]="undefined"></option>
                <option [ngValue]="'Y'" [selected]="attributeValues[0].valueString === 'Y'">Yes</option>
                <option [ngValue]="'N'" [selected]="attributeValues[0].valueString === 'N'">No</option>
              </select>
            </div>
          </div>
        </ng-container>

        <!-- Extended Boolean -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'EB'">
          <div #attributeRef
               class="d-flex flex-row">
            <div *ngIf="attribute.displayName" class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>
            <div class="d-flex col-md-6">
              <select [ngModel]="attributeValues[0].valueString"
                      (ngModelChange)="valueStringChange($event)"
                      class="edit-renderer">
                <option [ngValue]="undefined"></option>
                <option [ngValue]="'Y'" [selected]="attributeValues[0].valueString === 'Y'">Yes</option>
                <option [ngValue]="'N'" [selected]="attributeValues[0].valueString === 'N'">No</option>
                <option [ngValue]="'U'" [selected]="attributeValues[0].valueString === 'U'">Unknown</option>
              </select>
            </div>
          </div>
        </ng-container>

        <!-- Choice: Single -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'AC' && attribute.isMultiValue === 'N'">
          <div #attributeRef
               class="d-flex flex-row">
            <div *ngIf="attribute.displayName" class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>
            <div class="d-flex col-md-6">
              <select
                [ngModel]="(attributeValues[0].valueAttributeChoice)?attributeValues[0].valueAttributeChoice.idAttributeChoice:undefined"
                (ngModelChange)="valueChoiceChange($event)"
                class="edit-renderer">
                <option [ngValue]="undefined"></option>
                <option *ngFor="let attributeChoice of attributeChoices"
                        [ngValue]="attributeChoice.idAttributeChoice">
                  {{ attributeChoice.choice }}
                </option>
              </select>
            </div>
          </div>
        </ng-container>

        <!-- Choice: Multi -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'AC' && attribute.isMultiValue === 'Y'">
          <div #attributeRef
               class="d-flex flex-row">
            <div *ngIf="attribute.displayName" class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-column col-md-6" style="row-gap: 5px">
              <ng-container *ngFor="let attributeChoice of attributeChoices">
                <div class="d-flex">
                  <input type="checkbox"
                         [checked]="attributeChoice.value"
                         [disabled]="!editInline || attribute.isCalculated === 'Y'"
                         (change)="valueMultiChoiceChange(attributeChoice)"
                         class="form-control checkbox mt-auto mb-auto mr-2" attr.aria-label="hci-ng-attribute-edit-input-choice-multiple-{{attributeChoice.choice}}"/>
                  <div class="cod-label pl-1">
                    {{attributeChoice.choice}}
                  </div>
                </div>
              </ng-container>
            </div>
          </div>
        </ng-container>

        <!-- Dictionary: Multi -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'DICT' && attribute.isMultiValue === 'Y'">
          <div #attributeRef
               class="d-flex flex-row">
            <div *ngIf="attribute.displayName" class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-column col-md-6" style="row-gap: 5px">
              <ng-container *ngFor="let entry of dictionaryEntries">
                <div class="d-flex">
                  <input type="checkbox"
                         [checked]="entry.checked"
                         [disabled]="!editInline || attribute.isCalculated === 'Y'"
                         (change)="valueMultiDictChange(entry)"
                         [disabled]="!editInline || attribute.isCalculated === 'Y'"
                         class="form-control checkbox" attr.aria-label="hci-ng-attribute-edit-input-dictionary-choice-{{entry.display}}"/>
                  <div class="cod-label pl-1">
                    {{entry.display}}
                  </div>
                </div>
              </ng-container>
            </div>
          </div>
        </ng-container>

        <!-- Dictionary -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'DICT' && attribute.isMultiValue === 'N'">
          <div #attributeRef
               class="d-flex flex-row">
            <div *ngIf="attribute.displayName" class="d-flex col-md-6">
              {{attribute.displayName}}
            </div>
            <div class="d-flex col-md-6">
              <select #inputRef
                      [ngModel]="attributeValues[0].valueIdDictionary"
                      (ngModelChange)="valueDictChange($event)"
                      [disabled]="!editInline || attribute.isCalculated === 'Y'"
                      class="form-control edit-renderer">
                <option [ngValue]="undefined"></option>
                <option *ngFor="let entry of dictionaryEntries"
                        [ngValue]="entry.value">
                  {{ entry.display }}
                </option>
              </select>
            </div>
          </div>
        </ng-container>

        <!-- Grid -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'GA'">
          <div #attributeRef
               class="d-flex flex-column">
            <div *ngIf="attribute.displayName" class="d-flex col-md-12">
              <div>
                {{attribute.displayName}}
              </div>
              <div style="margin-left: auto;">
                <button class="btn-ga" (click)="addGridRow(editGridModal, attribute.idAttribute)">
              <span class="ga-icon">
                <i class="fas fa-plus fa-xs"></i>
              </span>
                </button>
                <button class="btn-ga" (click)="removeGridRow(attribute.idAttribute)">
              <span class="ga-icon">
                <i class="fas fa-minus fa-xs"></i>
              </span>
                </button>
              </div>
            </div>
            <div class="d-flex col-md-12">
              <ag-grid-angular #gridAttribute
                               class="ag-theme-alpine"
                               (gridReady)="this.onGridReady($event)"
                               (modelUpdated)="onModelUpdated($event)"
                               (gridSizeChanged)="onGridSizeChanged($event)"
                               (rowDoubleClicked)="editGridRow(editGridModal, attribute.idAttribute, $event)"
                               [gridOptions]="this.gridOptions"
                               [rowSelection]="'single'"
                               [columnDefs]="gridColumns"
                               [rowData]="gridData"
                               [style.width]="attribute.w + 'px'"
                               [style.height]="attribute.h + 'px'">
              </ag-grid-angular>
            </div>
          </div>
        </ng-container>

        <ng-template #editGridModal let-close="close">
          <div class="modal-header">
            {{attribute.displayName}} Grid Row
          </div>
          <div class="modal-body d-flex flex-column hci-cod-edit">
            <ng-container *ngFor="let attribute of editGroupRowAttributes">
              <hci-attribute-edit [id]="'edit-id-attribute-' + attribute.idAttribute"
                                  [groupAttributeRowId]="editGroupAttributeRowId"
                                  [attribute]="attribute"
                                  class="attribute"></hci-attribute-edit>
            </ng-container>
          </div>
          <div class="modal-footer">
            <button class="btn btn-primary" (click)="close('Save')">Save</button>
            <button class="btn btn-primary" (click)="close('Cancel')">Cancel</button>
          </div>
        </ng-template>
      `,
    styles: [
      `
        .hci-cod button.mat-icon-button.mat-button-base {
          height: 20px;
          width: 20px;
          line-height: unset;
        }

        .btn-ga {
          padding: 0px;
          height: 18px;
          width: 18px;
        }

        .ga-icon {
          font-size: .9em;
          vertical-align: top;
        }

        .hci-cod .mat-datepicker-toggle-default-icon {
          height: 20px;
          width: 20px;
        }
      `
    ]
  }
)
export class AttributeEditComponent extends AttributeBase {
  hciNgAttributeAbsoluteInputDisplayName = "";
  // dictionaryEndpoint = this.attributeService.dictionaryEndpoint;

  constructor(attributeService: AttributeService, elementRef: ElementRef, renderer: Renderer2, modalService: NgbModal) {
    super(attributeService, elementRef, renderer, modalService);
  }

  init(): void {
    super.init();
    if (this.attribute && this.attribute.displayName) {
      this.hciNgAttributeAbsoluteInputDisplayName = this.attribute.displayName;
    }
  }
}
