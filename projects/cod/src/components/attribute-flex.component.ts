import {
  Component,
  ElementRef,
  Renderer2,
  ViewEncapsulation
} from '@angular/core';

import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AttributeBase} from './attribute-base';
import {AttributeService} from '../services/attribute.service';
import {GraphicalAttribute} from '../model/graphical-attribute.entity';

/**
 * The view for attributes organized in a flex layout.  This will arrange all attributes in a four column layout
 * with grids taking up all 12 columns.  The attributes here will fill the this parent component which defines the
 * column size.
 */
@Component(
  {
    selector: 'hci-attribute-flex',
    encapsulation: ViewEncapsulation.None,
    template:
      `
        <!-- Line -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'LINE'">
          <div #attributeRef
               class="d-flex flex-row cod-type-line">
            <div class="d-flex cod-line mr-1 mt-auto mb-auto flex-shrink-0"></div>
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1 flex-shrink-0">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex cod-line grow mt-auto mb-auto"></div>
          </div>
        </ng-container>

        <!-- Label -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'LABEL'">
          <div #attributeRef
               class="d-flex flex-column cod-type-label">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
          </div>
        </ng-container>

        <!-- String -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'S'">
          <div #attributeRef
               class="d-flex flex-column cod-type-s">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1">
              <input #inputRef
                     type="text"
                     [(ngModel)]="attributeValues[0].valueString"
                     (ngModelChange)="valueStringChange($event)"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     class="form-control" attr.aria-label="hci-ng-attribute-flex-input-string-{{hciNgAttributeAbsoluteInputDisplayName}}"/>
            </div>
          </div>
        </ng-container>

        <!-- Text -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'TXT'">
          <div #attributeRef
               class="d-flex flex-column cod-type-txt">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1">
          <textarea
            #inputRef
            type="text"
            spellcheck="spellcheck"
            lang="en"
            [ngModel]="attributeValues[0].valueLongText.textData"
            (change)="valueTextChange($event)"
            [disabled]="!editInline || attribute.isCalculated === 'Y'"
            class="form-control">
          </textarea>
            </div>
          </div>
        </ng-container>

        <!-- Checkbox -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'CB'">
          <div #attributeRef
               class="d-flex flex-column cod-type-cb">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1">
              <input #inputRef
                     type="checkbox"
                     [checked]="attributeValues[0].valueString === 'Y'"
                     (change)="valueCheckboxChange($event)"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     class="form-control" attr.aria-label="hci-ng-attribute-flex-input-checkbox-{{hciNgAttributeAbsoluteInputDisplayName}}"/>
            </div>
          </div>
        </ng-container>

        <!-- Numeric -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'N'">
          <div #attributeRef
               class="d-flex flex-column cod-type-n">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1">
              <input #inputRef
                     type="number"
                     [(ngModel)]="attributeValues[0].valueNumeric"
                     (ngModelChange)="valueNumericChange($event)"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     class="form-control" attr.aria-label="hci-ng-attribute-flex-input-numeric-{{hciNgAttributeAbsoluteInputDisplayName}}"/>
            </div>
          </div>
        </ng-container>

        <!-- Integer -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'I'">
          <div #attributeRef
               class="d-flex flex-column cod-type-i">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1">
              <input #inputRef
                     type="number"
                     [(ngModel)]="attributeValues[0].valueInteger"
                     (ngModelChange)="valueIntegerChange($event)"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     class="form-control" attr.aria-label="hci-ng-attribute-flex-input-integer-{{hciNgAttributeAbsoluteInputDisplayName}}"/>
            </div>
          </div>
        </ng-container>

        <!-- Date -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'D'">
          <div #attributeRef
               class="d-flex flex-column cod-type-d">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1">
              <input #inputRef
                     matInput
                     name="valueDate"
                     [(ngModel)]="attributeValues[0].valueDate"
                     (ngModelChange)="valueDateChange($event)"
                     [matDatepicker]="valueDate"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     class="form-control" attr.aria-label="hci-ng-attribute-flex-input-date-{{hciNgAttributeAbsoluteInputDisplayName}}">
              <mat-datepicker-toggle matSuffix [for]="valueDate" class="cod-dp-toggle"></mat-datepicker-toggle>
              <mat-datepicker #valueDate></mat-datepicker>
            </div>
          </div>
        </ng-container>

        <!-- Date Time -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'DT'">
          <div #attributeRef
               class="d-flex flex-column cod-type-dt">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1">
              <input #inputRef
                     matInput
                     name="valueDateTime"
                     class="form-control"
                     [(ngModel)]="attributeValues[0].date"
                     (ngModelChange)="valueDateChange($event)"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     [ngxMatDatetimePicker]="dtpicker" attr.aria-label="hci-ng-attribute-flex-input-date-time-{{hciNgAttributeAbsoluteInputDisplayName}}">
              <mat-datepicker-toggle matSuffix [for]="dtpicker" class="cod-dp-toggle"></mat-datepicker-toggle>
              <ngx-mat-datetime-picker #dtpicker [showSeconds]="true"></ngx-mat-datetime-picker>
            </div>
          </div>
        </ng-container>

        <!-- Boolean -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'B'">
          <div #attributeRef
               class="d-flex flex-column cod-type-b">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1">
              <select #inputRef
                      [ngModel]="attributeValues[0].valueString"
                      (ngModelChange)="valueStringChange($event)"
                      [disabled]="!editInline || attribute.isCalculated === 'Y'"
                      class="form-control edit-renderer">
                <option [ngValue]="undefined"></option>
                <option [ngValue]="'Y'" [selected]="attributeValues[0].valueString === 'Y'">Yes</option>
                <option [ngValue]="'N'" [selected]="attributeValues[0].valueString === 'N'">No</option>
              </select>
            </div>
          </div>
        </ng-container>

        <!-- Extended Boolean -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'EB'">
          <div #attributeRef
               class="d-flex flex-column cod-type-eb">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1">
              <select #inputRef
                      [ngModel]="attributeValues[0].valueString"
                      (ngModelChange)="valueStringChange($event)"
                      [disabled]="!editInline || attribute.isCalculated === 'Y'"
                      class="form-control edit-renderer">
                <option [ngValue]="undefined"></option>
                <option [ngValue]="'Y'" [selected]="attributeValues[0].valueString === 'Y'">Yes</option>
                <option [ngValue]="'N'" [selected]="attributeValues[0].valueString === 'N'">No</option>
                <option [ngValue]="'U'" [selected]="attributeValues[0].valueString === 'U'">Unknown</option>
              </select>
            </div>
          </div>
        </ng-container>

        <!-- Choice: Single -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'AC' && attribute.isMultiValue === 'N'">
          <div #attributeRef
               class="d-flex flex-column cod-type-ac-n">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1">
              <select
                [ngModel]="(attributeValues[0].valueAttributeChoice)?attributeValues[0].valueAttributeChoice.idAttributeChoice:undefined"
                (ngModelChange)="valueChoiceChange($event)"
                [disabled]="!editInline || attribute.isCalculated === 'Y'"
                class="form-control edit-renderer">
                <option [ngValue]="undefined"></option>
                <option *ngFor="let attributeChoice of attributeChoices"
                        [ngValue]="attributeChoice.idAttributeChoice">
                  {{ attributeChoice.choice }}
                </option>
              </select>
            </div>
          </div>
        </ng-container>

        <!-- Choice: Multi -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'AC' && attribute.isMultiValue === 'Y'">
          <div #attributeRef
               class="d-flex flex-column cod-type-ac-y">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1 flex-column y-auto" style="max-height: 125px; row-gap: 5px">
              <ng-container *ngFor="let attributeChoice of attributeChoices">
                <div class="d-flex flex-shrink-0">
                  <input type="checkbox"
                         [checked]="attributeChoice.value"
                         [disabled]="!editInline || attribute.isCalculated === 'Y'"
                         (change)="valueMultiChoiceChange(attributeChoice)"
                         [disabled]="!editInline || attribute.isCalculated === 'Y'"
                         class="form-control checkbox" attr.aria-label="hci-ng-attribute-flex-input-choice-multiple-{{attributeChoice.choice}}"/>
                  <div class="cod-label pl-1">
                    {{ attributeChoice.choice }}
                  </div>
                </div>
              </ng-container>
            </div>
          </div>
        </ng-container>

        <!-- Dictionary: Multi -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'DICT' && attribute.isMultiValue === 'Y'">
          <div #attributeRef
               class="d-flex flex-column cod-type-ac-y">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1 flex-column y-auto" style="max-height: 125px; row-gap: 5px">
              <ng-container *ngFor="let entry of dictionaryEntries">
                <div class="d-flex flex-shrink-0">
                  <input type="checkbox"
                         [checked]="entry.checked"
                         [disabled]="!editInline || attribute.isCalculated === 'Y'"
                         (change)="valueMultiDictChange(entry)"
                         [disabled]="!editInline || attribute.isCalculated === 'Y'"
                         class="form-control checkbox" attr.aria-label="hci-ng-attribute-flex-input-dictionary-choice-{{entry.display}}"/>
                  <div class="cod-label pl-1">
                    {{ entry.display }}
                  </div>
                </div>
              </ng-container>
            </div>
          </div>
        </ng-container>

        <!-- Dictionary -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'DICT' && attribute.isMultiValue === 'N'">
          <div #attributeRef
               class="d-flex flex-column cod-type-dict">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1">
              {{ attribute.displayName }}
            </div>
            <div class="d-flex flex-grow-1">
              <select #inputRef
                      [ngModel]="attributeValues[0].valueIdDictionary"
                      (ngModelChange)="valueDictChange($event)"
                      [disabled]="!editInline || attribute.isCalculated === 'Y'"
                      class="form-control edit-renderer">
                <option [ngValue]="undefined"></option>
                <option *ngFor="let entry of dictionaryEntries"
                        [ngValue]="entry.value">
                  {{ entry.display }}
                </option>
              </select>
            </div>
          </div>
        </ng-container>

        <!-- Grid -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'GA'">
          <div #attributeRef class="d-flex flex-column cod-type-ga">
            <div *ngIf="attribute.idAttribute" class="d-flex cod-top-label mr-1">
              <div>
                {{ attribute.displayName }}
              </div>
              <div *ngIf="editInline" style="margin-left: auto;">
                <button
                  class="btn-ga"
                  [disabled]="(!this.attributeService.editButtonsEnabled)"
                  (click)="addGridRow(editGridModal, attribute.idAttribute)"
                >
                  <span class="ga-icon">
                    <i class="fas fa-plus fa-xs"></i>
                  </span>
                </button>
                <button
                  class="btn-ga"
                  [disabled]="(!this.attributeService.editButtonsEnabled)"
                  (click)="removeGridRow(attribute.idAttribute)"
                >
                  <span class="ga-icon">
                    <i class="fas fa-minus fa-xs"></i>
                  </span>
                </button>
              </div>
            </div>
            <div class="d-flex flex-grow-1">
              <ag-grid-angular #gridAttribute
                               class="ag-theme-alpine"
                               (gridReady)="this.onGridReady($event)"
                               (modelUpdated)="onModelUpdated($event)"
                               (gridSizeChanged)="onGridSizeChanged($event)"
                               (rowDoubleClicked)="editGridRow(editGridModal, attribute.idAttribute, $event)"
                               [gridOptions]="this.gridOptions"
                               [rowSelection]="'single'"
                               [columnDefs]="gridColumns"
                               [rowData]="gridData"
                               [style.width]="attribute.w + 'px'"
                               [style.height]="attribute.h + 'px'">
              </ag-grid-angular>
            </div>
          </div>
        </ng-container>

        <ng-template #editGridModal let-close="close">
          <div class="modal-header">
            {{ attribute.displayName }} Grid Row
          </div>
          <div class="modal-body d-flex flex-column hci-cod-edit">
            <ng-container *ngFor="let attribute of editGroupRowAttributes">
              <hci-attribute-edit [id]="'edit-id-attribute-' + attribute.idAttribute"
                                  [groupAttributeRowId]="editGroupAttributeRowId"
                                  [attribute]="attribute"
                                  class="attribute"></hci-attribute-edit>
            </ng-container>
          </div>
          <div class="modal-footer">
            <button
              [disabled]="(!this.attributeService.editButtonsEnabled)"
              class="btn btn-primary"
              (click)="close('Save')"
            >
              Save
            </button>

            <button
              class="btn btn-primary"
              (click)="close('Cancel')"
            >
              Cancel
            </button>
          </div>
        </ng-template>
      `,
    styles: [
      `
        .hci-cod button.mat-icon-button.mat-button-base {
          height: 20px;
          width: 20px;
          line-height: unset;
        }

        .btn-ga {
          padding: 0px;
          height: 18px;
          width: 18px;
        }

        .ga-icon {
          font-size: .9em;
          vertical-align: top;
        }

        .hci-cod .mat-datepicker-toggle-default-icon {
          height: 20px;
          width: 20px;
        }
      `
    ]
  }
)
export class AttributeFlexComponent extends AttributeBase {
  hciNgAttributeAbsoluteInputDisplayName = "";
  constructor(
    attributeService: AttributeService,
    elementRef: ElementRef,
    renderer: Renderer2,
    modalService: NgbModal
  ) {
    super(attributeService, elementRef, renderer, modalService);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit(): void {
    this.refresh();
    if (this.attribute && this.attribute.displayName) {
      this.hciNgAttributeAbsoluteInputDisplayName = this.attribute.displayName;
    }
  }

  /**
   * Flex needs to extend grid columns because we no longer want to use absolute width.  Here we generate percentage
   * based upon the absolute width, but still use absolute width for the min width.
   */
  initializeGridColumns(attributes: GraphicalAttribute[], columns: any[]) {
    super.initializeGridColumns(attributes, columns);

    let width = 0;
    for (const attribute of attributes) {
      width += attribute.w;
    }

    for (const attribute of attributes) {
      const widthPercent: number = Math.floor(100 * (attribute.w / width));

      for (const column of columns) {
        if (column.externalConfig && column.externalConfig.idAttribute === attribute.idAttribute) {
          column.minWidth = attribute.w;
          column.widthPercent = widthPercent;
          break;
        }
      }
    }
  }
}
