export interface AttributeConfigurationDTO {
  idAttributeConfiguration: number;
  codeAttributeContext: string;
  extensionDescription: string;
  codeAttributeSecurityContext: string;
  codeAttributeSecurityContextDisplay: string;
  idFilter1: number;
  filter1Display: string;
  requireFilter1: string;
  idFilter2: number;
  filter2Display: string;
  requireFilter2: string;
}
