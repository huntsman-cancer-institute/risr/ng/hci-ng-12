
export interface AttributeDictionary {
  idAttributeDictionary: number;
  attributeDictionary: string;
  description: string;
  tableName: string;
  className: string;
  xpath: string;
  xmlDisplayAttribute: string;
  xmlValueAttribute: string;
  xpathDisplay: string;
  isActive: string;
}
