/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {async, TestBed, inject} from "@angular/core/testing";
import {Router, NavigationExtras} from "@angular/router";
import {HttpClientModule, HttpClient, HttpErrorResponse} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

import {} from "jasmine";
import {Observable} from "rxjs";

import {UserService} from "./user.service";
import {PermissionEntity} from "./authorization/permission.entity";
import {UserEntity} from "./user.entity";
import {RoleEntity} from "./authorization/role.entity";

import {
  AUTHENTICATED_USER_ENDPOINT
} from "./user.service";

/**
 * Unit test suite for the {@link UserService}
 *
 * @since 1.0.0
 */

class MockRouter {
  navigate(commands: any[], extras?: NavigationExtras): Promise<boolean> {
    return Promise.resolve(true);
  }
}

class MockLocalStorageService {
  private storageValue: any = null;

  set(key: string, value: any): boolean {
    this.storageValue = value;
    return true;
  }

  get(key: string): any {
    return this.storageValue;
  }

  remove(...keys: Array<string>): boolean {
    this.storageValue = null;
    return true;
  }
}

describe("UserService Tests", () => {
  const authenticatedUserUrl: string = "https://localhost:8080/api/user/authenticated";
  const activeSessionEndpoint: string = "https://localhost:8080/api/user/user-session/active";
  const userSessionEndpoint: string = "https://localhost:8080/api/user-session";
  const loginPath: string = "/login";
  const defaultSuccessUrl: string = "";

  const user0: UserEntity = new UserEntity("42", "thugnificent", [
    new RoleEntity("user-admin"),
    new RoleEntity("study-foo-researcher"),
    new RoleEntity("specimen-receiver-study-goo")
  ]);

  const user1: UserEntity = new UserEntity("u0066784", "huey_freeman", [
    new RoleEntity("study-foo-admin", [
      new PermissionEntity("specimen", ["create", "read", "update", "delete"], ["foo"]),
      new PermissionEntity("report", ["administer"], ["foo"])
    ]),
    new RoleEntity("study-goo-researcher", [
      new PermissionEntity("specimen", ["read"], ["goo"])
    ]),
    new RoleEntity("specimen-receiver", [
      new PermissionEntity("specimen", ["create", "update"])
    ])
  ]);

  const user2: UserEntity = new UserEntity("foo", "suge", []);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {provide: AUTHENTICATED_USER_ENDPOINT, useValue: authenticatedUserUrl},
        UserService
      ]
    });
  });

  it("Should retrieve the authenticated user from the server if it is available.",
    async(
      inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {
        let userService: UserService = new UserService(http, authenticatedUserUrl);

        userService.getAuthenticatedUser().subscribe((authUser: UserEntity) => {
          expect(JSON.stringify(authUser)).toBe(JSON.stringify(user0));
        });

        let request = backend.expectOne({url: authenticatedUserUrl, method: "GET"});
        request.flush(user0);
      })
    )
  );

  it("Should retrieve the authenticated user from cache if it is available.",
    async(
      inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {
        let userService: UserService = new UserService(http, authenticatedUserUrl);

        userService.getAuthenticatedUser().subscribe((authUser: UserEntity) => {
          expect(JSON.stringify(authUser)).toBe(JSON.stringify(user1));
        });

        let request = backend.expectOne({url: authenticatedUserUrl, method: "GET"});
        request.flush(user1);

        userService.getAuthenticatedUser().subscribe((authUser: UserEntity) => {
          expect(JSON.stringify(authUser)).toBe(JSON.stringify(user1));
        });
      })
    )
  );

  it("Should throw an error with the error message when a unexpected response status is returned retrieving the authenticated user.",
    async(
      inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {
        let userService: UserService = new UserService(http, authenticatedUserUrl);

        userService.getAuthenticatedUser().subscribe(() => {
          fail("Expected error");
        }, (error) => {
          expect(error).toBeDefined();
        });

        let request = backend.expectOne({url: authenticatedUserUrl, method: "GET"});
        request.error(new ErrorEvent("Error"));
      })
    )
  );

});

