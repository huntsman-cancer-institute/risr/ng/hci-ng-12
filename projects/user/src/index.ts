/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */

/**
 * A barrel file for the HCI ng2 user package.
 *
 * @since 1.0.0
 */
export {UserModule} from "./user.module"
export {UserEntity} from "./user.entity"
export {RoleEntity} from "./authorization/role.entity"
export {PermissionEntity} from "./authorization/permission.entity"

/**
 * The opaque tokens for service configuration.
 */
export {
  AUTHENTICATED_USER_ENDPOINT
} from "./user.service"

export {UserService} from "./user.service"

export {RoleCheckDirective} from "./authorization/role-check.directive";
export {RoleCheckUnlessNullDirective} from "./authorization/role-check-unless-null.directive";

