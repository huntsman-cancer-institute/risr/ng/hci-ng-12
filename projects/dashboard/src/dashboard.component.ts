/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {
  ChangeDetectorRef, Component, ElementRef, isDevMode, QueryList, ViewChildren, ViewChild,
  HostBinding
} from "@angular/core";
import {animate, state, style, transition, trigger} from "@angular/animations";

import {DragulaService} from "ng2-dragula";
import domtoimage from "dom-to-image";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

import {Dashboard, WidgetInstance} from "./model/model";
import {DashboardService} from "./services/dashboard.service";
import {WidgetConfigureComponent} from "./configure/widget-configure.component";
import {WidgetContainerComponent} from "./widget/widget-container.component";
import {DashboardGlobalService} from "./services/dashboard-global.service";

/**
 * Dashboard (within a card-group) iterates over configured widgets to produce each one in a card.  The widgets can
 * always get access to any configuration held by the dashboard service, but they are intended to be largely
 * independent.
 *
 * @since 1.0.0
 */
@Component({
  selector: "dashboard",
  template: `
    <ng-template #confirmDialog let-c="close" let-d="dismiss">
      <div class="modal-header">
        <h5 class="modal-title">Delete</h5>
      </div>
      <div class="modal-body">
        <p>Are you sure?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" (click)="c('Yes')">Yes</button>
        <button type="button" class="btn btn-secondary" (click)="d('No')">No</button>
      </div>
    </ng-template>

    <widget-add [dashboardIndex]="selectedIndex"></widget-add>

    <div class="edit-mode-header"
         [@openEditModeHeader]="editMode"
         [@openEditModeHeaderDisplay]="editMode">
      <button class="btn btn-primary btn-space"
              (click)="addWidget()">
        <i class="fas fa-plus fa-2x"></i>
        <br />
        <span>Widget</span>
      </button>
      <button *ngIf="configMode === 'true'"
              class="btn btn-primary btn-space red"
              (click)="deleteWidget()">
        <i class="fas fa-times fa-2x"></i>
        <br />
        <span>Widget</span>
      </button>
      <button class="btn btn-primary btn-space"
              (click)="addDashboard()">
        <i class="fas fa-plus fa-2x"></i>
        <br />
        <span>Dashboard</span>
      </button>
      <button class="btn btn-primary btn-space red"
              *ngIf="dashboards.length > 1"
              (click)="deleteDashboard(confirmDialog)">
        <i class="fas fa-times fa-2x"></i>
        <br />
        <span>Dashboard</span>
      </button>
      <button class="btn btn-primary btn-space green"
              (click)="setEditMode(false)">
        <i class="fas fa-check fa-2x"></i>
        <br />
        <span>Done</span>
      </button>
    </div>

    <div class="outlet-row">

      <!-- Iterate over all dashboards.  Show non selected dashboards off screen. -->
      <ng-template ngFor let-dashboard [ngForOf]="dashboards" let-dashboardIndex="index">
        <div class="d-flex"
             [style.left]="selectedIndex !== dashboardIndex ? '100vw' : 'inherit'"
             [style.position]="selectedIndex !== dashboardIndex ? 'fixed' : 'inherit'"
             [@openConfigContent]="configMode">
          <widget-container
              [dashboard]="dashboard"
              [dashboardIndex]="dashboardIndex">
          </widget-container>
        </div>
      </ng-template>

      <div [style.display]="configMode === 'true' ? 'flex' : 'none'" class="sidebar-widget-config" [@openConfigSidebar]="configMode">
        <div class="outlet-column" style="background-color: white; height: 100%; padding: 2px;">
          <div class="d-flex flex-nowrap">
            <div><h4>Settings</h4></div>
            <div style="margin-right: 0px; margin-left: auto;">
              <span style="color: var(--pure-green);" (click)="saveConfiguration()">
                <i class="fas fa-check-circle fa-2x"></i>
              </span>
              <span style="color: var(--pure-red);" (click)="cancelConfiguration()">
                <i class="fas fa-times-circle fa-2x"></i>
              </span>
            </div>
          </div>
          <widget-configure #widgetConfigure></widget-configure>
        </div>
      </div>
    </div>

    <ng-container *ngIf="dashboards?.length > 1">
      <div class="dashboard-nav-trigger">
        <span (mouseenter)="openDashboardNavState = 'opened'">
          <i class="fas fa-th fa-2x"></i>
        </span>
      </div>

      <div class="dashboard-nav"
           [@openDashboardNav]="openDashboardNavState"
           [style.left]="'calc(50vw - ' + dashboards?.length * 50 + 'px)'"
           (mouseleave)="openDashboardNavState = 'closed'">
        <ng-container *ngFor="let dashboard of dashboards; index as i">
          <div [id]="'dash-image-' + i"
               class="widget-image"
               [ngClass]="i === selectedIndex ? 'selected' : ''"
               (click)="selectDashboard(i)">
          </div>
        </ng-container>
      </div>
    </ng-container>

    <ng-container *ngIf="helpMode">
      <div id="helpParent">
        <div id="helpChild">
          <div style="width: 100%; display: inline-flex;">
            <h4>Help</h4>
            <span style="margin-right: 0; margin-left: auto; color: var(--pure-red);" (click)="setHelpMode(false)">
              <i class="fas fa-times fa-lg"></i>
            </span>
          </div>
          <div>
            The dashboard is a collection of widgets that you can customize to your own needs.  In this edit mode, you
            can drag and drop widgets to change the order and double click a widget to change its configuration.
          </div>
        </div>
      </div>
    </ng-container>
  `,
  animations: [
    trigger("openConfigContent",
      [
        state("false", style({
          width: "100%"
        })),
        state("true", style({
          width: "80%"
        })),
        transition("false => true", animate(300)),
        transition("true => false", animate(300))
      ]
    ),
    trigger("openConfigSidebar",
      [
        state("false", style({
          opacity: "0",
          width: "0%"
        })),
        state("true", style({
          opacity: "1",
          width: "20%"
        })),
        transition("false => true", animate(300)),
        transition("true => false", animate(300))
      ]
    ),
    trigger("openDashboardNav",
      [
        state("closed", style({
          opacity: "0",
          bottom: "-150px"
        })),
        state("opened", style({
          opacity: "1",
          bottom: "40px"
        })),
        transition("closed => opened", animate(250)),
        transition("opened => closed", animate(100))
      ]
    ),
    /*trigger("openEditModeHeaderContent", [
      state("closed", style({
        "padding-top": "0px"
      })),
      state("opened", style({
        "padding-top": "91px"
      })),
      transition("closed => opened", animate(300)),
      transition("opened => closed", animate(300))
    ]),*/
    trigger("openEditModeHeader", [
      state("closed", style({
        "opacity": "0",
        "overflow-y": "hidden"
      })),
      state("opened", style({
        "opacity": "1",
        "overflow-y": "visible"
      })),
      transition("closed => opened", animate(300)),
      transition("opened => closed", animate(300))
    ]),
    trigger("openEditModeHeaderDisplay", [
      state("closed", style({
        "display": "none"
      })),
      state("opened", style({
        "display": "flex"
      })),
      transition("closed => opened", animate(100)),
      transition("opened => closed", animate("100ms 100ms"))
    ])
  ],
  styles: [`

    #helpParent {
      position: fixed;
      display: flex;
      width: 100vw;
      height: 100vh;
      left: 0;
      top: 0;
      z-index: 999;
      background: rgba(0, 0, 0, 0.25);
    }

    #helpChild {
      width: 75%;
      height: 75%;
      margin-left: auto;
      margin-right: auto;
      margin-top: auto;
      margin-bottom: auto;
      background: white;
      border-radius: 25px;
      box-shadow: 2px 2px black;
      padding: 20px;
    }

    .sidebar-widget-config {
      background-color: #ffdddd;
      border-left: gray 1px solid;
      padding: 10px;
      padding-right: 0px;
      opacity: 0;
      width: 0%;
    }

    .modal {
      background-color: rgba(0, 0, 0, 0.25);
    }

    .edit-mode-header {
      border-bottom: black 1px dashed;
      padding-top: 10px;
      padding-bottom: 10px;
      z-index: 10;
      background-color: white;
    }

    .edit-mode-header button {
      padding: .2rem .5rem;
    }

    .btn-space {
      margin-left: 10px;
    }

    .btn.green {
      background-color: green;
    }

    .btn.red {
      background-color: red;
    }

    .dashboard-nav-trigger {
      bottom: 40px;
      position: fixed;
      z-index: 100;
      display: inline-block;
      width: 100%;
      text-align: center;
      padding-top: 15px;
      background: white;
      background: linear-gradient(0deg, white, white, transparent);
    }

    .dashboard-nav {
      bottom: -150px;
      position: fixed;
      z-index: 101;
      display: inline-block;
      background-color: white;
    }

    .widget-image {
      display: inline-block;
      max-height: 100px;
      height: 100px;
      min-height: 100px;
      max-width: 100px;
      width: 100px;
      min-width: 100px;
      border: black 1px solid;
      vertical-align: bottom;
      overflow-y: hidden;
      overflow-x: hidden;
    }

    .widget-image.selected {
      border: red 2px solid;
    }
  `],
  providers: [
    DashboardService
  ]
})
export class DashboardComponent {

  @HostBinding("class") classList: string = "outlet-column";

  public openDashboardNavState: string = "closed";

  public helpMode: boolean = false;
  public editMode: string = "closed";
  public selectedIndex = 0;

  public dashboards: Dashboard[] = [];

  public showWidgetContainerNav: boolean = false;

  configMode: string = "false";

  @ViewChildren(WidgetContainerComponent, { read: ElementRef }) widgetContainers: QueryList<ElementRef>;

  @ViewChild("widgetConfigure", {static: false}) widgetConfigure: WidgetConfigureComponent;

  constructor(private changeDetectorRef: ChangeDetectorRef,
              private elementRef: ElementRef,
              private dashboardService: DashboardService,
              private dashboardGlobalService: DashboardGlobalService,
              private dragulaService: DragulaService,
              private modalService: NgbModal) {
    this.dashboards = this.dashboardService.getDashboards();

    this.dashboardService.dashboardsState.subscribe((dashboards: Dashboard[]) => {
      this.dashboards = dashboards;
    });

    this.dashboardService.configurationWidgetInstanceSubject.subscribe((widgetInstance: WidgetInstance) => {
      if (widgetInstance) {
        this.configMode = "true";
      } else {
        this.configMode = "false";
      }
    });

    this.dashboardService.getEditMode().subscribe((editMode: boolean) => {
      if (isDevMode()) {
        console.info("getEditMode " + editMode);
      }

      if (editMode) {
        this.editMode = "opened";
      } else {
        this.editMode = "closed";
      }
    });

    /*let bag = this.dragulaService.find("attribute-choices");
    if (bag !== undefined) {
      this.dragulaService.destroy("attribute-choices");
    }
    this.dragulaService.createGroup("attribute-choices", {
      direction: "vertical",
      moves: function (el: any, container: any, handle: any) {
        return true;
      }
    });
    this.dragulaService.find("attribute-choices")
      .drake
      .containers
      .push(this.elementRef.nativeElement.querySelector("#attribute-choices"));
    this.dragulaService.drop("").subscribe((value: any) => {
      let n: number = 1;

      for (let el of this.elementRef.nativeElement.querySelectorAll("widget-host")) {
        this.dashboardService.saveWidgetSortOrder(this.selectedIndex, +el.getAttribute("id"), n++);
      }
    });*/
  }

  ngOnInit(): void {
    this.dashboardGlobalService.setActiveDashboardService(this.dashboardService);
  }

  ngAfterViewInit() {
    this.widgetContainers.changes.subscribe((widgetContainers: QueryList <ElementRef>) => {
      this.renderDashboardNavImages();
    });
  }

  ngOnDestroy(): void {
    this.dashboardGlobalService.setActiveDashboardService();
  }

  selectDashboard(index: number) {
    this.selectedIndex = index;
  }

  /**
   * Informs dashboardService of editMode change.  Widget hosts will listen for this change.
   *
   * @param editMode
   */
  setEditMode(editMode: boolean) {
    this.dashboardService.setEditMode(editMode);
  }

  setHelpMode(helpMode: boolean) {
    if (isDevMode()) {
      console.info("setHelpMode " + helpMode);
    }

    this.helpMode = helpMode;
    this.changeDetectorRef.markForCheck();
  }

  /**
   * Use dom-to-image to render each widgetContainer as an image.  Add an image for each widgetContainer
   * to the dashboardNav view.
   */
  renderDashboardNavImages() {
    setTimeout(() => {
      var self = this;
      this.widgetContainers.forEach((elementRef: ElementRef, index: number) => {
        var el = self.elementRef.nativeElement.querySelector("#dash-image-" + index);
        if (el) {
          domtoimage.toPng(elementRef.nativeElement)
            .then(function (dataUrl) {
              var img = new Image();
              img.width = 100;
              img.src = dataUrl;
              el.innerHTML = "";
              el.appendChild(img);
              self.showWidgetContainerNav = true;
            })
            .catch(function (error) {
              console.error("Widget Image Error", error);
            });
        }
      });
    }, 2000);
  }

  /**
   * Sends trigger to dashboardService to open the widget add component.  That component listens for
   * this trigger.
   */
  addWidget() {
    this.dashboardService.addWidgetSubject.next(true);
  }

  deleteWidget() {
    this.dashboardService.deleteWidget(this.dashboardService.configurationWidgetInstanceSubject.getValue());
  }

  cancelConfiguration() {
    this.dashboardService.cancelConfigurationWidget();
  }

  saveConfiguration() {
    this.dashboardService.saveConfigurationWidget();
  }

  /**
   * Calls dashboardService to add a dashboard.  Add dashboard will update dashboards through subscribe and
   * this waits for completion of the process which returns the new selectedIndex (which is the last dashboard).
   */
  addDashboard() {
    let dashboard: Dashboard = new Dashboard();
    dashboard.name = "Temp";
    dashboard.sortOrder = this.dashboards.length;
    dashboard.widgetInstances = [];
    this.dashboardService.addDashboard(dashboard).subscribe((index: number) => {
      this.selectedIndex = index;
    });
  }

  /**
   * Calls dashboardService to delete dashboard.  Delete dashboard will update dashboards through subscribe and
   * this waits for completion of the process which returns the new selectedIndex.
   *
   * @param content
   */
  deleteDashboard(content) {
    this.modalService.open(content).result.then((result: string) => {
      if (result === "Yes") {
        this.dashboardService.deleteDashboard(this.selectedIndex).subscribe((index: number) => {
          this.selectedIndex = index;
        });
      }
    });
  }

}
