import {Injectable} from "@angular/core";

import {DashboardService} from "./dashboard.service";

/**
 * Single instance of this service which can be used to reference dashboard anywhere within the application.  The
 * dashboard service has been moved to be provided by the dashboard component so this is needed to hook in.
 */
@Injectable()
export class DashboardGlobalService {

  activeDashboardService: DashboardService

  setActiveDashboardService(activeDashboardService?: DashboardService): void {
    this.activeDashboardService = activeDashboardService;
  }

  getActiveDashboardService(): DashboardService {
    return this.activeDashboardService;
  }
}
