import {Inject, Injectable, InjectionToken, isDevMode, Type} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";

import {BehaviorSubject, forkJoin, Observable, Subject} from "rxjs";
import {map} from "rxjs/operators";

import {WidgetComponent} from "../widget/widget.component";
import {Dashboard, Widget, WidgetAttribute, WidgetAttributeContainer,
    WidgetAttributeValue, WidgetAttributeValueSet, WidgetCategory, WidgetInstance} from "../model/model";

export let USER_PROFILE_ENDPOINT: InjectionToken<string> = new InjectionToken("userProfileEndpoint");
export let DASHBOARD_TYPE: InjectionToken<string> = new InjectionToken("dashboardType");
export let WIDGET_COMPONENTS: InjectionToken<Type<any>[]> = new InjectionToken("widgetComponents");

/**
 * The service holds the configuration array for the widgets.  Widgets can access their configuration from here.
 *
 * @since 1.0.0
 */
@Injectable()
export class DashboardService {

  public addWidgetSubject: Subject<boolean> = new Subject<boolean>();
  public configurationWidgetInstanceSubject: BehaviorSubject<WidgetInstance> = new BehaviorSubject<WidgetInstance>(undefined);
  public updateWidgetInstanceSubject: Subject<WidgetInstance> = new Subject<WidgetInstance>();

  public dashboardsState: Subject<Dashboard[]> = new Subject<Dashboard[]>();

  private widgetAttributeUpdatedSubject: Subject<number> = new Subject<number>();

  private dashboards: Dashboard[] = [];

  private widgets: BehaviorSubject<Widget[]> = new BehaviorSubject<Widget[]>(undefined);
  private widgetCategories: WidgetCategory[];

  private widgetRendered: Subject<boolean> = new Subject<boolean>();
  private editMode: boolean = false;
  private editModeSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  private idNew: number = -1;
  private configurationWidgetInstance: WidgetInstance;
  private configurationAttributeContainers: WidgetAttributeContainer[];
  private attributeContainerMap: Map<string, WidgetAttributeContainer> = new Map<string, WidgetAttributeContainer>();

  private configWidgetComponent: WidgetComponent;
  private preConfigAttributeValueSet: WidgetAttributeValueSet;

  private widgetComponentCreated: Subject<number> = new Subject<number>();
  private registeredWidgetComponents: Map<number, WidgetComponent> = new Map<number, WidgetComponent>();
  private widgetComponentSubjects: Map<number, BehaviorSubject<WidgetComponent>> = new Map<number, BehaviorSubject<WidgetComponent>>();

  private entryComponentNames: string[];
  private entryComponents: Type<any>[];

  /**
   * Interacts with the backend service to get the user profile and widgets.  Also manages widgets as they are added and
   * edited such that widget attributes are stored in one place.
   *
   * @param http
   * @param userProfileEndpoint
   */
  constructor(private http: HttpClient,
              @Inject(USER_PROFILE_ENDPOINT) private userProfileEndpoint: string,
              @Inject(DASHBOARD_TYPE) private dashboardType: string) {
    if (isDevMode()) {
      console.debug("DashboardService constructor");
    }

    this.init();
  }

  init() {
    this.attributeContainerMap.set("Common", new WidgetAttributeContainer());

    this.widgetRendered.next(false);
    this.editModeSubject.next(this.editMode);

    this.http.get(this.userProfileEndpoint + "/user-profile/my").subscribe((response: any) => {
      forkJoin([
        this.http.get(this.userProfileEndpoint + "/attribute/common-attributes"),
        this.http.get(this.userProfileEndpoint + "/widget/widget-categories"),
        this.http.get(this.userProfileEndpoint + "/widget/widget-list", {params: new HttpParams().set("dashboardType", this.dashboardType)}),
        this.http.get(this.userProfileEndpoint + "/dashboard", {params: new HttpParams().set("dashboardType", this.dashboardType)})
      ]).subscribe(responses => {
        this.setCommonAttributes(responses[0]);
        this.setWidgetCategories(responses[1]);
        this.setWidgets(responses[2]);
        this.setDashboards(responses[3]);

        this.pushDashboardState();
      });
    });
  }

  setConfigWidgetComponent(configWidgetComponent: WidgetComponent) {
    this.configWidgetComponent = configWidgetComponent;
  }

  getConfigWidgetComponent(): WidgetComponent {
    return this.configWidgetComponent;
  }

  setEntryComponents(entryComponentNames: string[], entryComponents: Type<any>[]) {
    if (isDevMode()) {
      console.debug("setEntryComponents: " + entryComponents.length);
    }

    this.entryComponentNames = entryComponentNames;
    this.entryComponents = entryComponents;

    for (let type of this.entryComponents) {
      console.debug(type);
    }
  }

  getEntryComponent(component: string): Type<any> {
    if (!this.entryComponents) {
      console.warn("DashboardService: No entryComponents!");
    }

    if (this.entryComponentNames && this.entryComponents) {
      for (var i = 0; i < this.entryComponentNames.length; i++) {
        if (this.entryComponentNames[i] === component) {
          return this.entryComponents[i];
        }
      }
    }
    return undefined;
  }

  doAddWidget() {
    this.addWidgetSubject.next(true);
  }

  getCommonAttributes(): Map<string, WidgetAttributeContainer> {
    return this.attributeContainerMap;
  }

  setCommonAttributes(response: any) {
    if (isDevMode()) {
      console.debug("DashboardService.setCommonAttributes()");
    }

    let commonAttributeContainer: WidgetAttributeContainer = new WidgetAttributeContainer();
    commonAttributeContainer.name = "Common";
    commonAttributeContainer.sortOrder = 0;
    commonAttributeContainer.attributes = new WidgetAttribute().deserializeArray(response);
    commonAttributeContainer.generateAttributeMap();
    this.attributeContainerMap = new Map<string, WidgetAttributeContainer>();
    this.attributeContainerMap.set("Common", commonAttributeContainer);

    if (isDevMode()) {
      console.debug("DashboardService.setCommonAttributes(); Setting common attributes:");
      console.debug(commonAttributeContainer.attributes);
    }
  }

  /**
   * Return the array of dashboards.  Each dashboard contains an array of widgets.
   *
   * @returns {Dashboard[]}
   */
  getDashboards(): Dashboard[] {
    return this.dashboards;
  }

  /**
   * Calls the backend to fetch the list of widgets available.
   *
   */
  setWidgets(response: any) {
    if (isDevMode()) {
      console.debug("DashboardService.setWidgets()");
    }

    this.widgets.next(<Widget[]>response);
  }

  /**
   * Calls the backend to fetch the list of widget categories available.
   *
   */
  setWidgetCategories(response: any) {
    if (isDevMode()) {
      console.debug("DashboardService.setWidgetCategories()");
    }

    this.widgetCategories = <WidgetCategory[]>response;
  }

  /**
   * Sets the dashboards after the user profile is fetched.  Upon fetch, the widgets within each dashboard are sorted
   * by their sortOrder.
   *
   * @param dashboards
   */
  setDashboards(response: any) {
    if (isDevMode()) {
      console.debug("DashboardService.setDashboards()");
    }

    this.dashboards = new Dashboard().deserializeArray(response);

    if (!this.dashboards) {
      this.dashboards = [];
    }

    if (isDevMode()) {
      console.debug(this.dashboards);
    }

    for (var d = 0; d < this.dashboards.length; d++) {
      this.dashboards[d].widgetInstances = this.dashboards[d].widgetInstances.sort((a: WidgetInstance, b: WidgetInstance) => {
        if (a.sortOrder !== undefined && b.sortOrder !== undefined) {
          let aa: number = <number>a.sortOrder;
          let bb: number = <number>b.sortOrder;
          if (aa < bb) {
            return -1;
          } else if (aa > bb) {
            return 1;
          } else {
            return 0;
          }
        } else if (a.sortOrder !== undefined) {
          return -1;
        } else if (b.sortOrder !== undefined) {
          return 1;
        } else {
          return 0;
        }
      });
    };
  }

  pushDashboardState() {
    this.dashboardsState.next(this.dashboards);
  }

  /**
   * Returns the list of widget categories for searching/filtering widgets.
   *
   * @returns {WidgetCategory[]}
   */
  getWidgetCategories() : WidgetCategory[] {
    return this.widgetCategories;
  }

  /**
   * Filter widgets by title or keyword.
   * TODO: Add any other metrics on which to filter by.
   *
   * @param filter
   * @returns {Widget[]}
   */
  getWidgetsByFilter(filter: string): Widget[] {
    if (!filter || filter.length === 0) {
      let filteredList: Widget[] = [];
      for (var i = 0; i < this.widgets.getValue().length; i++) {
        if (this.widgets.getValue()[i].instantiable) {
          filteredList.push(this.widgets.getValue()[i]);
        }
      }
      return filteredList;
    } else if (filter) {
      let filteredList: Widget[] = [];
      filter = filter.toLowerCase();
      for (var i = 0; i < this.widgets.getValue().length; i++) {
        if (this.widgets.getValue()[i].instantiable) {
          if (this.widgets.getValue()[i].title && this.widgets.getValue()[i].title.toLowerCase().indexOf(filter) !== -1) {
            filteredList.push(this.widgets.getValue()[i]);
          } else if (this.widgets.getValue()[i].description && this.widgets.getValue()[i].description.toLowerCase().indexOf(filter) !== -1) {
            filteredList.push(this.widgets.getValue()[i]);
          }
        }
      }
      return filteredList;
    } else {
      return this.widgets.getValue();
    }
  }

  /**
   * For a particular attribute, return all current attribute values for it.
   *
   * @param idWidgetInstance
   * @param idWidgetAttribute
   * @returns {WidgetInstanceAttributeValue[]}
   */
  getAttributeValues(idWidgetInstance: number, idWidgetAttribute: number): WidgetAttributeValue[] {
    let attributeValues: WidgetAttributeValue[] = [];

    for (var d = 0; d < this.dashboards.length; d++) {
      for (var w = 0; w < this.dashboards[d].widgetInstances.length; w++) {
        if (this.dashboards[d].widgetInstances[w].idWidgetInstance === idWidgetInstance) {
          attributeValues = this.dashboards[d].widgetInstances[w].attributeValueSet.attributeValues.filter((value: WidgetAttributeValue) => {
            return value.idWidgetAttribute === idWidgetAttribute;
          });
          if (attributeValues.length === 0) {
            return this.dashboards[d].widgetInstances[w].widget.attributeValueSet.attributeValues.filter((value: WidgetAttributeValue) => {
              return value.idWidgetAttribute === idWidgetAttribute;
            });
          } else {
            return attributeValues;
          }
        }
      }
    }

    return [];
  }

  setConfigurationAttributeContainers(configurationAttributeContainers: WidgetAttributeContainer[]) {
    if (isDevMode()) {
      console.debug("DashboardService.setConfigurationAttributeContainers()");
      console.debug(configurationAttributeContainers);
    }

    this.configurationAttributeContainers = configurationAttributeContainers;
  }

  /**
   * For a particular attribute, return all current attribute values for it.
   *
   * @param idWidgetAttribute
   * @returns {WidgetInstanceAttributeValue[]}
   */
  getConfigurationAttributeValues(idWidgetAttribute: number): WidgetAttributeValue[] {
    if (isDevMode()) {
      console.debug("DashboardService.getConfigurationAttributeValues(); idWidgetAttribute = " + idWidgetAttribute);
    }

    let attributeValues: WidgetAttributeValue[] = [];

    if (this.configurationAttributeContainers) {
      for (var ac = 0; ac < this.configurationAttributeContainers.length; ac++) {
        for (var a = 0; a < this.configurationAttributeContainers[ac].attributes.length; a++) {
          if (this.configurationAttributeContainers[ac].attributes[a].idWidgetAttribute === idWidgetAttribute) {
            for (var v = 0; v < this.configurationWidgetInstance.attributeValueSet.attributeValues.length; v++) {
              if (this.configurationAttributeContainers[ac].attributes[a].idWidgetAttribute === this.configurationWidgetInstance.attributeValueSet.attributeValues[v].idWidgetAttribute) {
                attributeValues.push(this.configurationWidgetInstance.attributeValueSet.attributeValues[v]);
              }
            }
            if (attributeValues.length === 0) {
              let attributeValue: WidgetAttributeValue = new WidgetAttributeValue();
              attributeValue.idWidgetAttributeValue = this.getIdNew();
              attributeValue.idWidgetAttribute = idWidgetAttribute;

              if (this.configurationAttributeContainers[ac].attributes[a].defaultValue) {
                if (this.configurationAttributeContainers[ac].attributes[a].dataType === "integer") {
                  attributeValue.valueInteger = parseInt(this.configurationAttributeContainers[ac].attributes[a].defaultValue);
                } else if (this.configurationAttributeContainers[ac].attributes[a].dataType === "checkbox") {
                  attributeValue.valueBoolean = this.configurationAttributeContainers[ac].attributes[a].defaultValue.toLowerCase() === "true" ? true : false;
                } else {
                  attributeValue.valueString = this.configurationAttributeContainers[ac].attributes[a].defaultValue;
                }
              }

              attributeValues.push(attributeValue);
            }
            break;
          }
        }
      }
    }

    if (isDevMode()) {
      console.debug(attributeValues);
    }

    return attributeValues;
  }

  /**
   * After completion of a drag and drop, iterate the element selector with a number starting at 1 and save each
   * widget instance with the new sort order.  If the existing sort order is the same, then don't update.
   *
   * @param selectedDashboard
   * @param idWidgetInstance
   * @param sortOrder
   */
  saveWidgetSortOrder(selectedDashboard: number, idWidgetInstance: number, sortOrder: number) {
    for (var w = 0; w < this.dashboards[selectedDashboard].widgetInstances.length; w++) {
      if (this.dashboards[selectedDashboard].widgetInstances[w].idWidgetInstance === idWidgetInstance
          && this.dashboards[selectedDashboard].widgetInstances[w].sortOrder !== sortOrder) {
        this.http
          .post(this.userProfileEndpoint + "/widget/widget-instance/" + idWidgetInstance + "/sort-order", { "sortOrder": sortOrder })
          .subscribe(response => {
            // 200
          });
      }
    }
  }

  /**
   * Set the widget instance to be edited (or added).
   *
   * @param widgetInstance The widget which may or may not be persisted.
   * @param add Indicate if the widget is being added or an existing widget being configured.
   */
  setConfigurationWidgetInstance(widgetInstance: WidgetInstance, widgetComponent: WidgetComponent, add: boolean) {
    this.setConfigWidgetComponent(widgetComponent);

    if (!widgetInstance.attributeValueSet) {
      this.http
        .put(this.userProfileEndpoint + "/widget/widget-instance/" + widgetInstance.idWidgetInstance + "/attribute-value-set", JSON.stringify(new WidgetAttributeValueSet()))
        .subscribe((response: any) => {
          widgetInstance.attributeValueSet = new WidgetAttributeValueSet().deserialize(response);

          this.postSetConfigurationWidgetInstance(widgetInstance, add);
        });
    } else {
      this.postSetConfigurationWidgetInstance(widgetInstance, add);
    }
  }

  postSetConfigurationWidgetInstance(widgetInstance: WidgetInstance, add: boolean) {
    if (isDevMode()) {
      console.debug("postSetConfigurationWidgetInstance");
    }

    this.configurationWidgetInstance = widgetInstance;
    this.configurationWidgetInstanceSubject.next(widgetInstance);

    this.preConfigAttributeValueSet = new WidgetAttributeValueSet().deserialize(widgetInstance.attributeValueSet);
    if (isDevMode()) {
      console.debug("postSetConfigurationWidgetInstance: " + JSON.stringify(this.preConfigAttributeValueSet));
    }
  }

  /**
   * When values are updated in the add/config screen, push them to the stored instance.  If any there are new values,
   * then they must be pushed.  If there are values that no longer exist, then they must be deleted.
   *
   * @param idWidgetAttribute
   * @param attributeValues
   * @returns {WidgetInstanceAttributeValue[]}
   */
  setConfigurationAttributeValues(idWidgetAttribute: number, attributeValues: WidgetAttributeValue[]): WidgetAttributeValue[] {
    /* Add new values. */
    for (var i = 0; i < attributeValues.length; i++) {
      let exists: boolean = false;
      for (var v = 0; v < this.configurationWidgetInstance.attributeValueSet.attributeValues.length; v++) {
        if (this.configurationWidgetInstance.attributeValueSet.attributeValues[v].idWidgetAttributeValue === attributeValues[i].idWidgetAttributeValue) {
          this.configurationWidgetInstance.attributeValueSet.attributeValues[v] = attributeValues[i];
          exists = true;
          break;
        }
      }

      if (!exists) {
        if (!attributeValues[i].idWidgetAttributeValue) {
          attributeValues[i].idWidgetAttributeValue = this.getIdNew();
        }
        this.configurationWidgetInstance.attributeValueSet.attributeValues.push(attributeValues[i]);
      }
    }

    /* Delete old values. */
    for (var v = this.configurationWidgetInstance.attributeValueSet.attributeValues.length - 1; v >= 0; v--) {
      if (this.configurationWidgetInstance.attributeValueSet.attributeValues[v].idWidgetAttribute !== idWidgetAttribute) {
        continue;
      }

      let exists: boolean = false;
      for (var i = 0; i < attributeValues.length; i++) {
        if (this.configurationWidgetInstance.attributeValueSet.attributeValues[v].idWidgetAttributeValue === attributeValues[i].idWidgetAttributeValue) {
          exists = true;
          break;
        }
      }
      if (!exists) {
        this.configurationWidgetInstance.attributeValueSet.attributeValues.splice(v, 1);
      }
    }

    return attributeValues;
  }

  /**
   * Maintain uniqueness of new instances.  At some point we must decide when to persist to database to have the ids generated.
   *
   * @returns {number}
   */
  getIdNew(): number {
    return this.idNew--;
  }

  cancelConfigurationWidget() {
    if (isDevMode()) {
      console.debug("cancelConfigurationWidget: " + JSON.stringify(this.preConfigAttributeValueSet));
    }

    let dashboardIndex: number = undefined;
    let widgetIndex: number = undefined;

    for (var d = 0; d < this.dashboards.length; d++) {
      for (var w = 0; w < this.dashboards[d].widgetInstances.length; w++) {
        if (this.dashboards[d].widgetInstances[w].idWidgetInstance === this.configurationWidgetInstance.idWidgetInstance) {
          dashboardIndex = d;
          widgetIndex = w;
          break;
        }
      }
      if (dashboardIndex && widgetIndex) {
        break;
      }
    }

    this.dashboards[dashboardIndex].widgetInstances[widgetIndex].attributeValueSet = this.preConfigAttributeValueSet;
    this.configurationWidgetInstanceSubject.next(undefined);
    this.updateWidgetInstanceSubject.next(this.dashboards[dashboardIndex].widgetInstances[widgetIndex]);
  }

  /**
   * When attributes are finalized after adding/editing a widget, this will set those attributes to the widget instance.
   */
  saveConfigurationWidget() {
    let dashboardIndex: number = undefined;
    let widgetIndex: number = undefined;

    for (var d = 0; d < this.dashboards.length; d++) {
      for (var w = 0; w < this.dashboards[d].widgetInstances.length; w++) {
        if (this.dashboards[d].widgetInstances[w].idWidgetInstance === this.configurationWidgetInstance.idWidgetInstance) {
          dashboardIndex = d;
          widgetIndex = w;
          break;
        }
      }
      if (dashboardIndex && widgetIndex) {
        break;
      }
    }

    let idWidgetInstance: number = this.dashboards[dashboardIndex].widgetInstances[widgetIndex].idWidgetInstance;

    this.http
      .post(this.userProfileEndpoint + "/widget/widget-instance/" + idWidgetInstance + "/attribute-value-set", JSON.stringify(this.configurationWidgetInstance.attributeValueSet))
      .subscribe(((response: any) => {
        this.dashboards[dashboardIndex].widgetInstances[widgetIndex].attributeValueSet = new WidgetAttributeValueSet().deserialize(response);

        this.updateWidgetInstanceSubject.next(this.dashboards[dashboardIndex].widgetInstances[widgetIndex]);
      }));

    this.configurationWidgetInstance = undefined;
    this.configurationWidgetInstanceSubject.next(undefined);
  }

  /**
   * Add a new widget into the currently selected dashboard.
   *
   * @param selectedWidget
   * @param widgetInstance
   * @param dashboardIndex
   * @param step
   */
  addWidget(selectedWidget: Widget, widgetInstance: WidgetInstance, dashboardIndex: number, step: number) {
    if (step === 2) {
      widgetInstance = this.configurationWidgetInstance;
    }
	  widgetInstance.idDashboard = this.dashboards[dashboardIndex].idDashboard;
    widgetInstance.sortOrder = this.dashboards[dashboardIndex].widgetInstances.length;
	  widgetInstance.idReset();

    this.http
      .put(this.userProfileEndpoint + "/widget/widget-instance/", JSON.stringify(widgetInstance))
      .subscribe((response: any) => {
        widgetInstance = new WidgetInstance().deserialize(response);
        this.dashboards[dashboardIndex].widgetInstances.push(widgetInstance);
        this.dashboardsState.next(this.dashboards);

        if (widgetInstance.widget && widgetInstance.widget.hasAttributes()) {
          this.setConfigurationWidgetInstance(widgetInstance, this.getWidgetComponent(widgetInstance.idWidgetInstance), false);
        }
      });
  }

  getWidget(component: string): Observable<Widget> {
    return this.http
      .get(this.userProfileEndpoint + "/widget/component/" + component)
      .pipe(map((response: any) => new Widget().deserialize(response)));
  }

  addDashboard(dashboard: Dashboard): Observable<number> {
    let newIndex: Observable<number> = new Observable(observer => {
      this.http
        .put(this.userProfileEndpoint + "/dashboard/", dashboard)
        .pipe(map((response: any) => new Dashboard().deserialize(response)))
        .subscribe((dashboard: Dashboard) => {
          this.dashboards.push(dashboard);
          observer.next(this.dashboards.length - 1);
        });
    });

    return newIndex;
  }

  deleteDashboard(index: number): Observable<number> {
    let newIndex: Observable<number> = new Observable(observer => {
      this.http
        .delete(this.userProfileEndpoint + "/dashboard/" + this.dashboards[index].idDashboard, {})
        .subscribe((response: any) => {
          this.dashboards.splice(index, 1);
          if (this.dashboards.length === index) {
            observer.next(index - 1);
          } else {
            observer.next(index);
          }
          this.dashboardsState.next(this.dashboards);
        });
    });

    return newIndex;
  }

  deleteWidget(widgetInstance: WidgetInstance) {
    this.http
      .delete(this.userProfileEndpoint + "/widget/widget-instance/" + widgetInstance.idWidgetInstance, {})
      .subscribe((response: any) => {
        for (var d = 0; d < this.dashboards.length; d++) {
          for (var w = 0; w < this.dashboards[d].widgetInstances.length; w++) {
            if (this.dashboards[d].widgetInstances[w].idWidgetInstance === widgetInstance.idWidgetInstance) {
              this.dashboards[d].widgetInstances.splice(w, 1);
              break;
            }
          }
        }
        this.configurationWidgetInstanceSubject.next(undefined);
      });
  }

  /**
   * Returns a subject which is used to notify widget add/edit windows when the widget is finished rendering.  In some
   * cases attribute choices are based on data which the widget might fetch.  Before trying to render attributes for editing
   * that widget, we have to wait for the data get to finish so the attribute choices will be available.
   *
   * @returns {Subject<boolean>}
   */
  getWidgetRendered(): Subject<boolean> {
    return this.widgetRendered;
  }

  /**
   * Allows any listeners to be made aware of when a widget rendering has finished.
   *
   * @param widgetRendered
   */
  setWidgetRendered(widgetRendered: boolean) {
    this.widgetRendered.next(widgetRendered);
  }

  getEditMode(): Subject<boolean> {
    return this.editModeSubject;
  }

  setEditMode(editMode: boolean) {
    this.editMode = editMode;
    this.editModeSubject.next(this.editMode);
    this.configurationWidgetInstanceSubject.next(undefined);
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
    this.editModeSubject.next(this.editMode);
  }

  setWidgetAttributeUpdatedSubject(idWidgetAttribute: number) {
    this.widgetAttributeUpdatedSubject.next(idWidgetAttribute);
  }

  getWidgetAttributeUpdatedSubject(): Subject<number> {
    return this.widgetAttributeUpdatedSubject;
  }

  getWidgetComponent(idWidgetInstance: number): WidgetComponent {
    return this.registeredWidgetComponents.get(idWidgetInstance);
  }

  registerWidgetComponent(idWidgetInstance: number, widgetComponent: WidgetComponent) {
    this.registeredWidgetComponents.set(idWidgetInstance, widgetComponent);

    if (!this.widgetComponentSubjects.has(idWidgetInstance)) {
      this.widgetComponentSubjects.set(idWidgetInstance, new BehaviorSubject<WidgetComponent>(widgetComponent));
    }
    this.widgetComponentSubjects.get(idWidgetInstance).next(widgetComponent);
  }

  unregisterWidgetcomponent(idWidgetInstance: number) {
    this.registeredWidgetComponents.delete(idWidgetInstance);
  }

  getWidgetComponentSubject(idWidgetInstance: number): Subject<WidgetComponent> {
    if (!this.widgetComponentSubjects.has(idWidgetInstance)) {
      this.widgetComponentSubjects.set(idWidgetInstance, new BehaviorSubject<WidgetComponent>(undefined));
    }
    return this.widgetComponentSubjects.get(idWidgetInstance);
  }

  getWidgetComponentCreated(): Subject<number> {
    return this.widgetComponentCreated;
  }
}
