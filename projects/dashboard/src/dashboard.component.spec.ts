/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {inject, TestBed} from "@angular/core/testing";

import {} from "jasmine";

import {DashboardModule} from "./dashboard.module";
import {DashboardComponent} from "./dashboard.component";
import {DashboardService, USER_PROFILE_ENDPOINT} from "./services/dashboard.service";

/**
 * @since 1.0.0
 */
describe("DashboardComponent Tests", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        DashboardModule
      ],
      providers: [
        {provide: USER_PROFILE_ENDPOINT, use: "http://localhost/user-profile"},
        DashboardService
      ]
    });
  });

  /*it ("Dashboard should be empty.", inject([DashboardService], (dashboardService: DashboardService) => {
      let fixture = TestBed.createComponent(DashboardComponent);
      let dashboard = fixture.componentInstance;

      let length: number = dashboardService.getDashboards().length;

      expect(length === 0).toBeTruthy();
    })
  );*/
  /* Figure out done() and async
   it ("Dashboard should have two widgets.", async(() => {
   let fixture = TestBed.createComponent(DashboardComponent);
   let dashboard = fixture.componentInstance;

   console.log("dashboardService.init()");
   let dashboardService: DashboardService = TestBed.get(DashboardService);
   dashboardService.init();

   dashboardService.dashboardsState.subscribe((dashboards: Dashboard[]) => {
   (result) => {
   console.info("dashboardService.dashboardsState.subscribe");
   expect(dashboards[0].widgetInstances.length === 1).toBeTruthy();
   done();
   }
   });

   console.log("Test 2 Done");
   }
   ));*/

});
