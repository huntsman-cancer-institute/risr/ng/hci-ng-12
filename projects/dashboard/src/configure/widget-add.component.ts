/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, Input, ViewChild} from "@angular/core";
import {animate, state, style, transition, trigger} from "@angular/animations";

import {Subscription} from "rxjs";

import { Widget, WidgetCategory, WidgetInstance, WidgetAttributeValue, WidgetAttributeValueSet, WidgetAttributeContainer} from "../model/model";
import {DashboardService} from "../services/dashboard.service";
import {WidgetComponent} from "../widget/widget.component";
import {WidgetConfigureComponent} from "./widget-configure.component";

/**
 * @since 1.0.0
 */
@Component({
  selector: "widget-add",
  template: `
    <div class="overlay" [@openBacksplash]="openState">
      <div class="modal-dialog" [@openModal]="openState" role="document">
        <div class="modal-header">
          <h4 class="modal-title">Add Widget</h4>
          <button type="button" class="close" aria-label="Close" (click)="close()">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" [style.display]="step === 1 ? 'block' : 'none'">
            <div class="modal-body-left">
              <ng-template ngFor let-widgetCategory [ngForOf]="widgetCategories">
                <div class="widget-category"
                     [ngClass]="widgetCategory.idWidgetCategory === selectedWidgetCategory?.idWidgetCategory ? 'selected' : ''"
                     (click)="selectWidgetCategory(widgetCategory)">
                  <ng-container *ngIf="widgetCategory.icon">
                    <ng-container *ngIf="widgetCategory.icon.indexOf('.svg') > 0; else faIcon">
                      <img [src]="widgetCategory.icon" class="widget-category-icon" />
                    </ng-container>
                    <ng-template #faIcon>
                      <i class="{{widgetCategory.icon}} fa-2x widget-category-icon"></i>
                    </ng-template>
                  </ng-container>
                  <ng-container *ngIf="!widgetCategory.icon">
                    <i class="fas fa-question-circle fa-2x widget-category-icon"></i>
                  </ng-container>

                </div>
              </ng-template>
            </div>
            <div class="modal-body-right">
              <div class="input-group widget-search">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="search"><i class="fas fa-search fa-lg"></i></span>
                </div>
                <input type="text"
                       #searchWidget
                       class="form-control"
                       placeholder=""
                       aria-describedby="search"
                       (keyup)="searchWidgets(searchWidget.value)" aria-label="hci-ng-search-widget-add">
                <button class="btn" style="margin-left: 10px;" (click)="setDisplayMode('list')">
                  <i class="fas fa-list"></i>
                </button>
                <button class="btn" style="margin-left: 10px;" (click)="setDisplayMode('grid')">
                  <i class="fas fa-th"></i>
                </button>
              </div>
              <ng-container *ngIf="widgets && widgets.length > 0">
                <div>No widgets matched the search criteria.</div>
              </ng-container>
              <ng-template ngFor let-widget [ngForOf]="widgets">
                <div class="widget-label"
                     *ngIf="displayMode === 'list'"
                     (click)="selectWidget(widget)">
                  <div class="widget-icon-host">
                    <ng-container *ngIf="widget.icon">
                      <ng-container *ngIf="widget.icon.indexOf('.svg') > 0; else faIcon">
                        <img [src]="widget.icon" class="widget-icon" />
                      </ng-container>
                      <ng-template #faIcon>
                        <i class="{{widget.icon}} fa-2x widget-icon"></i>
                      </ng-template>
                    </ng-container>
                    <ng-container *ngIf="!widget.icon">
                      <i class="fas fa-question-circle fa-2x widget-icon"></i>
                    </ng-container>
                  </div>
                  <div class="widget-description-host">
                    <div class="widget-title">{{widget.title}}</div>
                    <div>{{widget.description}}</div>
                  </div>
                </div>
                <div class="grid widget-label"
                     *ngIf="displayMode === 'grid'"
                     (click)="selectWidget(widget)">
                  <div class="grid widget-icon-host">
                    <ng-container *ngIf="widget.icon">
                      <ng-container *ngIf="widget.icon.indexOf('.svg') > 0; else faIcon">
                        <img [src]="widget.icon" class="widget-icon" />
                      </ng-container>
                      <ng-template #faIcon>
                        <i class="{{widget.icon}} fa-2x widget-icon"></i>
                      </ng-template>
                    </ng-container>
                    <ng-container *ngIf="!widget.icon">
                      <i class="fas fa-question-circle fa-2x widget-icon"></i>
                    </ng-container>
                  </div>
                  <div class="grid widget-description-host">
                    <div class="grid widget-title">{{widget.title}}</div>
                  </div>
                </div>
              </ng-template>
            </div>
        </div>
        <div class="modal-footer">
          <ng-container *ngIf="step === 1">
            <button type="button" class="btn btn-secondary" (click)="close()">Cancel</button>
            <button type="button" class="btn btn-secondary" (click)="add()">Add</button>
          </ng-container>
        </div>
      </div>
    </div>
  `,
  animations: [
    trigger("openBacksplash",
      [
        state("in", style({
          display: "none"
        })),
        state("hidden", style({
          display: "none"
        })),
        state("opened", style({
          display: "flex"
        })),
        transition("hidden => opened", animate(0)),
        transition("opened => hidden", animate(200))
      ]
    ),
    trigger("openModal",
      [
        state("in", style({
          opacity: "0"
        })),
        state("hidden", style({
          opacity: "0"
        })),
        state("opened", style({
          opacity: "1"
        })),
        transition("hidden => opened", animate(500)),
        transition("opened => hidden", animate(200))
      ]
    )
  ],
  styles: [`

    .overlay {
      position: fixed;
      z-index: 9999;
      top: 0;
      bottom: 0;
      background-color: rgba(0, 0, 0, 0.4);
      width: 100vw;
      display: none;
    }

    .modal-dialog {
      max-width: 75vw;
      min-width: 75vw;
      margin-left: auto;
      margin-right: auto;
      margin-top: auto;
      margin-bottom: auto;
      background-color: white;
      border: black 1px solid;
      border-left: none;
      pointer-events: all;
    }

    .modal-body {
      width: 100%;
      display: inline-flex;
    }

    .modal-body-left {
      display: inline-block;
      overflow-y: auto;
      overflow-x: hidden;
      min-height: 300px;
      max-height: 300px;
    }

    .modal-body-right {
      width: 70%;
      vertical-align: top;
      padding-left: 15px;
      border-left: black 1px solid;
      margin-left: 15px;
      display: inline-block;
      overflow-y: auto;
      min-height: 300px;
      max-height: 300px;
    }

    .widget-category {
      margin: 10px;
      border-radius: 20px;
      background-color: #eeeeee;
    }

    .widget-category.selected {
      border: red 2px solid;
    }

    .widget-category-icon {
      width: 80px;
      height: 80px;
      padding: 5px;
      margin-left: auto;
      margin-right: auto;
      display: flex;
    }

    .widget-search {
      margin-bottom: 10px;
    }

    .widget-label {
      padding: 10px;
      background-color: #eeeeee;
      border-radius: 10px;
      margin-bottom: 5px;
    }

    .widget-label.grid {
      display: inline-block;
      width: 150px;
      overflow-y: hidden;
      overflow-x: hidden;
      max-height: 100px;
      min-height: 100px;
      margin: 5px;
    }

    .widget-icon-host {
      display: inline-block;
      width: 50px;
      vertical-align: top;
    }

    .widget-icon {
      width: 50px;
      height: 50px;
      margin-left: auto;
      margin-right: auto;
      display: flex;
    }

    .widget-title {
      font-weight: bold;
    }

    .widget-description-host {
      display: inline-block;
      width: calc(100% - 55px);
    }

  `]
})
export class WidgetAddComponent {

  @Input()
  dashboardIndex: number = 0;

  public step: number = 0;

  public widgets: Widget[];
  public widgetCategories: WidgetCategory[];

  public displayMode: string = "list";
  public openState: string = "hidden";
  public widgetRendered: boolean = false;

  public selectedWidget: Widget;
  public selectedWidgetCategory: WidgetCategory;

  private widgetInstance: WidgetInstance;

  private widgetRenderedSubscription: Subscription;

  @ViewChild(WidgetConfigureComponent, {static: false})
  private widgetConfigureComponent: WidgetConfigureComponent;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.dashboardService.addWidgetSubject.subscribe((addWidget: boolean) => {
      this.init();
      this.openState = "opened";
    });
  }

  init() {
    this.widgetRenderedSubscription = this.dashboardService.getWidgetRendered().subscribe((widgetRendered: boolean) => {
      if (widgetRendered && this.step === 2) {
        this.widgetRendered = true;
      }
      this.widgetRenderedSubscription.unsubscribe();
    });

    this.widgets = this.dashboardService.getWidgetsByFilter(undefined);
    this.widgetCategories = this.dashboardService.getWidgetCategories();
    this.step = 1;
  }

  /**
   * Re-filter the widget list based upon a change in widget category.
   *
   * @param {WidgetCategory} widgetCategory
   */
  selectWidgetCategory(selectedWidgetCategory: WidgetCategory) {
    this.widgets = this.dashboardService.getWidgetsByFilter(undefined);
    if (this.selectedWidgetCategory && this.selectedWidgetCategory.idWidgetCategory === selectedWidgetCategory.idWidgetCategory) {
      this.selectedWidgetCategory = undefined;
    } else {
      this.selectedWidgetCategory = selectedWidgetCategory;
      this.widgets = this.widgets.filter((widget: Widget) => {
        if (widget.widgetCategories && widget.widgetCategories.length > 0) {
          return widget.widgetCategories.filter((widgetCategory: WidgetCategory) => {
            return widgetCategory.idWidgetCategory === selectedWidgetCategory.idWidgetCategory;
          }).length > 0;
        } else {
          return false;
        }
      });
    }
  }

  /**
   * When the widget is selected there are two options.  If there are no attributes to be set by the user, then add the
   * widget.  If there are instance attributes that must be set, then switch to a widget configuration screen.
   *
   * @param widget
   */
  selectWidget(widget: Widget) {
    this.widgetInstance = new WidgetInstance();
    this.widgetInstance.idWidgetInstance = this.dashboardService.getIdNew();
    this.widgetInstance.widget = widget;
    this.widgetInstance.attributeValueSet = this.getDefaultWidgetAttributeValues(this.widgetInstance.idWidgetInstance, widget.attributeContainers);
    this.add();
  }

  setDisplayMode(displayMode: string) {
    this.displayMode = displayMode;
  }

  getDefaultWidgetAttributeValues(idWidgetInstance: number, attributeContainers: WidgetAttributeContainer[]): WidgetAttributeValueSet {
    let valueSet: WidgetAttributeValueSet = new WidgetAttributeValueSet();
    valueSet.idWidgetAttributeValueSet = this.dashboardService.getIdNew();
    let values: WidgetAttributeValue[] = [];

    for (let container of attributeContainers) {
      for (let attribute of container.attributes) {
        if (!attribute.userEditable) {
          continue;
        }
        if (attribute.multiValue) {
          continue;
        }

        let value = new WidgetAttributeValue();
        value.idWidgetAttributeValue = this.dashboardService.getIdNew();
        value.idWidgetAttribute = attribute.idWidgetAttribute;

        if (attribute.dataType === "integer") {
          value.valueInteger = parseInt(attribute.defaultValue);
        } else {
          value.valueString = attribute.defaultValue;
        }
        values.push(value);
      }
    }

    valueSet.attributeValues = values;
    return valueSet;
  }

  add() {
    this.dashboardService.addWidget(this.selectedWidget, this.widgetInstance, this.dashboardIndex, this.step);
    this.close();
  }

  back() {
    this.dashboardService.setWidgetRendered(false);
    this.widgetRendered = false;
    this.step = 1;
  }

  close() {
    this.dashboardService.setWidgetRendered(false);
    this.widgetRendered = false;
    this.step = 0;
    this.openState = "hidden";
  }

  searchWidgets(filter: string) {
    this.widgets = this.dashboardService.getWidgetsByFilter(filter);
  }
}
