import {Component, TemplateRef, ViewChild, ViewContainerRef} from "@angular/core";
import {TestBed, async} from "@angular/core/testing";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {} from "jasmine";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {NavigationService} from "../services/navigation.service";
import {NavigationGlobalService} from "../services/navigation-global.service";
import {NavigationModule} from "../navigation.module";
import {SidebarComponent} from "../impl/sidebar.component";
import {SearchListComponent} from "../components/search-list/search-list.component";
import {HciGroupingDto, HciPagingDto} from "hci-ng-grid-dto";

@Component({
  selector: "hci-test",
  template: `
    <hci-sidebar #sidebar></hci-sidebar>

    <ng-template #groupTemplate let-rowGroup="rowGroup">
      <div class="search-list-group flex-grow-1">
        <div class="nav-item" style="width: 100%; padding-left: 10px;">
          <div class="mr-1">Name:</div>
          <div class="font-weight-bold">{{rowGroup.lastName}}</div>
        </div>
      </div>
    </ng-template>

    <ng-template #itemTemplate let-row="row">
      <div class="search-list-item ml-4">
        <div [id]="row.firstName + '-' + row.lastName">{{row.firstName}}, {{row.lastName}}</div>
      </div>
    </ng-template>
  `,
  providers: [
    NavigationService
  ]
})
class TestComponent {
  @ViewChild(SidebarComponent, {static: true}) sidebar: ViewContainerRef;
  @ViewChild("groupTemplate", {read: TemplateRef, static: true}) groupTemplate: TemplateRef<any>;
  @ViewChild("itemTemplate", {read: TemplateRef, static: true}) itemTemplate: TemplateRef<any>;
}

describe("TestComponent Role Tests", () => {
  let httpMock: HttpTestingController;

  let data: Object[] = [
    {id: 1, firstName: "Bob", lastName: "Black"},
    {id: 2, firstName: "Charlie", lastName: "Black"},
    {id: 3, firstName: "Jane", lastName: "Green"},
    {id: 4, firstName: "Mary", lastName: "Brown"}
  ];

  var fixture;
  var cmpt;
  var element;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        HttpClientTestingModule,
        NgbModule,
        NavigationModule
      ],
      providers: [
        NavigationGlobalService
      ],
      declarations: [
        TestComponent
      ]
    });

    httpMock = TestBed.get(HttpTestingController);

    fixture = TestBed.createComponent(TestComponent);
    cmpt = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  it("Populates Rows.",
    async(() => {
      cmpt.sidebar.setConfig({
        id: "sidebar",
        children: [
          {
            type: SearchListComponent,
            id: "search-list",
            title: "Search",
            groupTemplate: cmpt.groupTemplate,
            itemTemplate: cmpt.itemTemplate,
            data: data
          }
        ]
      });
      fixture.detectChanges();

      expect(element.querySelectorAll(".search-list-item").length).toBe(4);
      expect(element.querySelector("#Bob-Black")).toBeDefined();
    })
  );

  it("Populates Grouped Rows.",
    async(() => {
      cmpt.sidebar.setConfig({
        id: "sidebar",
        children: [
          {
            type: SearchListComponent,
            id: "search-list",
            title: "Search",
            groupTemplate: cmpt.groupTemplate,
            itemTemplate: cmpt.itemTemplate,
            data: data,
            groupingFields: ["lastName"]
          }
        ]
      });
      fixture.detectChanges();

      expect(element.querySelectorAll(".search-list-group").length).toBe(3);
    })
  );

  it("Pages rows.",
    async(() => {
      cmpt.sidebar.setConfig({
        id: "sidebar",
        children: [
          {
            type: SearchListComponent,
            id: "search-list",
            title: "Search",
            groupTemplate: cmpt.groupTemplate,
            itemTemplate: cmpt.itemTemplate,
            data: [
              {id: 1, firstName: "Bob", lastName: "Black"},
              {id: 2, firstName: "Charlie", lastName: "Black"},
              {id: 3, firstName: "Jane", lastName: "Green"},
              {id: 4, firstName: "Mary", lastName: "Brown"}
            ],
            pageSize: 3
          },
        ]
      });
      fixture.detectChanges();
      (<SearchListComponent>cmpt.sidebar.getComponent("search-list")).pagingSubject.next(new HciPagingDto(1, 3));

      expect(element.querySelectorAll(".search-list-item").length).toBe(1);
    })
  );

  /*it("External Basic.",
    async(() => {
      cmpt.sidebar.setConfig({
        id: "sidebar",
        children: [
          {
            type: SearchListComponent,
            id: "search-list",
            title: "Search",
            groupTemplate: cmpt.groupTemplate,
            itemTemplate: cmpt.itemTemplate,
            dataCall: "/api/data-call"
          },
        ]
      });
      fixture.detectChanges();

      httpMock.expectOne("/api/data-call").flush({data: [
          {id: 1, firstName: "Bob", lastName: "Black"},
          {id: 2, firstName: "Charlie", lastName: "Black"},
          {id: 3, firstName: "Jane", lastName: "Green"},
          {id: 4, firstName: "Mary", lastName: "Brown"}
        ], gridDto: {paging: {dataSize: 4}}});

      //expect(element.querySelectorAll(".search-list-item").length).toBe(1);
    })
  );*/

});
