import {Injectable, isDevMode, Type} from "@angular/core";

import {BehaviorSubject, Subject} from "rxjs";

import {RoleEntity} from "@huntsman-cancer-institute/user";

import {NavComponent} from "../components/nav.component";
import {NavigationGlobalService} from "./navigation-global.service";

/**
 * This service manages injected components and handles events between components.
 */
@Injectable()
export class NavigationService {

  private id: string;
  private idNavComponent: number = -1;

  private registeredComponents: Map<string, NavComponent> = new Map<string, NavComponent>();
  private eventMap: Map<string, Subject<string>> = new Map<string, Subject<string>>();
  private userRoles: BehaviorSubject<RoleEntity[]> = new BehaviorSubject<RoleEntity[]>([]);

  constructor(private navigationGlobalService: NavigationGlobalService) {}

  setRootComponent(id: string, navComponent: NavComponent): void {
    this.id = id;
    this.navigationGlobalService.register(id, navComponent, this);
  }

  getId(): string {
    return this.id;
  }

  getIdNavComponent(): string {
    this.idNavComponent++;
    return "nav-component-" + this.idNavComponent;
  }

  insertAfter(id: string, config: any) {
    this.registeredComponents.get(id).getParent().insertAfter(id, config);
  }

  insertBefore(id: string, config: any) {
    this.registeredComponents.get(id).getParent().insertBefore(id, config);
  }

  setUserRoles(userRoles: RoleEntity[]) {
    this.userRoles.next(userRoles);
  }

  getUserRoles(): BehaviorSubject<RoleEntity[]> {
    return this.userRoles;
  }

  registerEvent(event: string) {
    this.eventMap.set(event, new Subject<string>());
  }

  emitEvent(event: string, value: string) {
    this.eventMap.get(event).next(value);
  }

  getEvent(event: string): Subject<string> {
    return this.eventMap.get(event);
  }

  registerComponent(id: string, component: NavComponent) {
    if (id !== undefined) {
      this.registeredComponents.set(id, component);
      if (isDevMode()) {
        console.debug("registerComponent: " + id + " " + component.typeName);
      }
    }
  }

  /**
   * Return a component by its id.
   *
   * @param {string} id
   * @returns {NavComponent}
   */
  getComponent(id: string): NavComponent {
    return this.registeredComponents.get(id);
  }

  /**
   * Return a component by its type.
   *
   * @param {string} type
   * @returns {NavComponent}
   */
  getComponentByType(type: string): NavComponent {
    if (isDevMode()) {
      console.debug("getComponentByType: " + type);
    }

    for (let navComponent of this.registeredComponents.values()) {
      if (type === navComponent.typeName) {
        return navComponent;
      }
    }

    return undefined;
  }

}
