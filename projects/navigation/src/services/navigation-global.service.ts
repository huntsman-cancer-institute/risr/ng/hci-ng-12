import {Injectable, isDevMode} from "@angular/core";

import {Subject} from "rxjs";

import {NavigationService} from "./navigation.service";
import {NavComponent} from "../components/nav.component";

/**
 * A singleton service that when a navigation component is instantiated, will register itself and its service with
 * this global service.  That way any navigation component or service can be referenced from anywhere in the
 * implementing application.
 */
@Injectable()
export class NavigationGlobalService {

  uniqueId: number = 0;

  serviceMap: Map<string, NavigationService> = new Map<string, NavigationService>();
  componentMap: Map<string, NavComponent> = new Map<string, NavComponent>();

  notificationEvent: Subject<boolean> = new Subject<boolean>();

  getNavigationComponent(id: string): NavComponent {
    return this.componentMap.get(id);
  }

  getNavigationService(id: string): NavigationService {
    return this.serviceMap.get(id);
  }

  /**
   * For a root navigation component to call when first instantiated.  Stores a reference of that component and service
   * in a map that is referenced by id.
   *
   * @param {string} id
   * @param {NavComponent} navComponent
   * @param {NavigationService} navigationService
   */
  register(id: string, navComponent: NavComponent, navigationService: NavigationService) {
    if (isDevMode()) {
      console.info("NavigationGlobalService.register: " + id);
    }

    if (id) {
      this.componentMap.set(id, navComponent);
      this.serviceMap.set(id, navigationService);
    }
  }

  getNotificationEventSubject(): Subject<boolean> {
    return this.notificationEvent;
  }

  /**
   * Currently, a simple way for navigation components to be made aware of potential changes external to itself.
   */
  pushNotificationEvent(): void {
    this.notificationEvent.next(true);
  }
}
