import {
  Component, Input, TemplateRef
} from "@angular/core";

import {ComponentType} from "@huntsman-cancer-institute/utils";
import {SearchListControllerComponent} from "../search-list/search-list-controller.component";

/**
 * The search tree controller renders as a header to one or more search trees.  Only one search tree can be shown at a time.
 * The controller features the ability to handle filtering and sorting.  Without the controller, the search list can exist,
 * but it can only handle paging internally.
 */
@ComponentType("SearchTreeControllerComponent")
@Component({
  selector: "hci-search-tree-controller",
  exportAs: "searchTreeControllerComponent",
  styles: [`
    .sidebar-container {
      background-color: #E7E7E7;
      color: #fff ;
      box-shadow: none;
      z-index: 1000;
      padding-top: 3px;
      margin-left: -8px;
      margin-right: -8px;
      padding-bottom: 32px;
    }

  `],
  template: `
    <div *ngIf="!hideElements" id="search-title" class="d-flex flex-column">
      <div *ngIf="showTitle" class="d-flex pl-2">
        {{title}}
        <div class="d-flex flex-row ml-auto mr-2">
          <div *ngIf="buttonTemplate"><ng-container *ngTemplateOutlet="buttonTemplate"></ng-container></div>
          <a (click)="toggleShowOptions($event)"><i class="fas fa-filter"></i></a>
        </div>
        <div class="search-options" [class.expanded]="showOptions" *ngIf="filterByAllowedFields.length > 0 && sortByAllowedFields.length > 0">
          <ul ngbNav #nav="ngbNav" class="nav-tabs">
            <li ngbNavItem  *ngIf="filterByAllowedFields.length > 1">
              <a ngbNavLink > Filtering </a>
              <ng-template ngbNavContent>
                <div class="d-flex flex-column">
                  <ng-container *ngFor="let filter of boundFilters">
                    <div class="d-flex m-1" style="align-items: center;">
                      <div style="flex: 0 0 40%;">{{getFieldDisplay(filter.field)}}</div>
                      <input [ngModel]="filter.value"
                             (ngModelChange)="setFilter($event, filter)"
                             (keydown)="applyFilters($event)"
                             style="flex: 1 1 60%;" aria-label="hci-ng-search-tree-controller-filter"/>
                    </div>
                  </ng-container>
                  <div class="d-flex flex-grow-1 m-1">
                    <button (click)="applyFilters()" class="ml-auto mr-1"><i class="fas fa-check-circle"></i></button>
                    <button (click)="setupFilters(true)" class="mr-0"><i class="fas fa-times-circle"></i></button>
                  </div>
                </div>
              </ng-template>
            </li>
            <li ngbNavItem *ngIf="sortByAllowedFields.length > 0">
              <a ngbNavLink > Sorting </a>
              <ng-template ngbNavContent>
                <div class="d-flex flex-column">
                  <ng-container *ngFor="let sortField of sortByAllowedFields">
                    <div class="d-flex m-2" style="align-items: center;" (click)="setSort($event, sortField)">
                      <div style="width: 1.5rem;">
                        <div *ngIf="isSort(sortField) && isSortAsc(sortField)" [style.color]="isFirstSort(sortField) ? 'green' : 'inherit'">
                          <i class="fas fa-arrow-circle-up fa-lg"></i>
                        </div>
                        <div *ngIf="isSort(sortField) && !isSortAsc(sortField)" [style.color]="isFirstSort(sortField) ? 'green' : 'inherit'">
                          <i class="fas fa-arrow-circle-down fa-lg"></i>
                        </div>
                      </div>
                      {{getFieldDisplay(sortField)}}
                    </div>
                  </ng-container>
                </div>
              </ng-template>
            </li>
          </ul>
          <div [ngbNavOutlet]="nav" ></div>
        </div>
      </div>
      <div class="d-flex mb-1 global-search" style="align-items: center;">
        <input [ngModel]="globalSearch" (keyup)="doGlobalSearch($event)" class="ml-2" placeholder="Search" aria-label="hci-ng-search-tree-controller-global-search"/>
        <div class="ml-1 mr-1">
          <i class="fas fa-search"></i>
        </div>
      </div>
      <div class="d-flex p-1 x-auto" *ngIf="hasValidFilters()">
        <div class="d-flex">
          <ng-container *ngFor="let filter of filters">
            <ng-container *ngIf="filter.valid">
              <div (click)="clearFilter(filter)" class="d-flex pill">
                <div class="mr-2">
                  {{filter.field}}
                </div>
                <div class="close-icon">
                  <i class="fas fa-times-circle fa-xs"></i>
                </div>
              </div>
            </ng-container>
          </ng-container>
        </div>
      </div>
      <div *ngIf="searchLists.length > 1" class="d-flex x-hidden list-toggle">
        <mat-button-toggle-group [ngModel]="selectedSearchList" (ngModelChange)="setSelectedSearchList($event)">
          <mat-button-toggle *ngFor="let searchList of searchLists" mat-raised-button-toggle color="primary" [value]="searchList">
            {{searchList}}
          </mat-button-toggle>
        </mat-button-toggle-group>
      </div>
    </div>
    <div *ngIf="hideElements" class="sidebar-container">
      <div id="search-title" class="d-flex flex-column pt-1">
          <div *ngIf="showTitle" class="d-flex pl-3">
            {{title}}
          </div>
      </div>
    </div>
  `
})
export class SearchTreeControllerComponent extends SearchListControllerComponent {

  @Input() buttonTemplate: TemplateRef<any>;
  @Input() hideElements: boolean = false;

  updateConfig(config) {
    super.updateConfig(config);

    if (config.buttonTemplate) {
      this.buttonTemplate = config.buttonTemplate;
    }
    if (config.hideElements) {
      this.hideElements = config.hideElements;
    }
  }
}
