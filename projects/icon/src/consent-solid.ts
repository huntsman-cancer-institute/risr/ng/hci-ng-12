export const hciConsentSolid = {
  prefix: "hci",
  iconName: "consent-solid",
  icon: [512, 512, [], "f140", "M512,4.72C282.96,124.32,157.24,329.27,157.24,329.27L0,287.69l219.59,219.59C219.59,507.28,274.63,217.98,512,4.72z"]
};
