import {Component, EventEmitter, Input, Output} from "@angular/core";
import {MAT_DATE_LOCALE} from "@angular/material/core";

@Component({
  selector: "report-datepicker",
  template: `
    <div class="d-flex" style="width: 100%;">
      <input matInput [matDatepicker]="picker" class="form-control" style="cursor: pointer;" [placeholder]="placeholder" [ngModel]="value" (ngModelChange)="onChange($event)"
             (click)="picker.open()" (blur)="onBlur()" [min]="minDate" [max]="maxDate"
             [ngStyle]="{'background-color':invalid ? 'red' : '#fff'}" attr.aria-label="hci-ng-report-date-picker-{{placeholder}}">
      <span class="input-group-text" id="clearIcon" (click)="clear()"><i class="fas fa-times"></i></span>
      <button class="btn btn-outline-secondary" type="button" (click)="picker.open()" style="height:32px; width:32px;"><i class="fas fa-calendar-alt"></i></button>
      <mat-datepicker #picker ></mat-datepicker>
    </div>
  `,
  styles: [`
    #clearIcon {
      cursor: pointer;
    }
    #clearIcon:Hover {
      background-color: rgb(73, 80, 87);
      color: rgb(233, 236, 239)
    }
  `],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: "en-GB"}
  ]
})
export class ReportDatepickerComponent {
  @Input() value: Date;
  @Input() placeholder: string;
  @Input() minDate: Date;
  @Input() maxDate: Date;
  @Input() invalid: boolean = false;

  @Output() valueChange = new EventEmitter();

  ngOnInit() {
    let today = new Date();
    // set the minDate to 20 years in the past by default
    if (!this.minDate) {
      this.minDate = new Date(today.getFullYear() - 20, today.getMonth(), today.getDate());
    }
    // set max date to 20 years in future by default
    if (!this.maxDate) {
      this.maxDate = new Date(today.getFullYear() + 20, today.getMonth(), today.getDate());
    }
  }

  onChange(newValue) {
    this.value = newValue;
    this.valueChange.emit(newValue);
  }

  clear() {
    this.value = undefined;
    this.valueChange.emit();
  }

  // Clear input field if entered data is invalid (Mat library sets value to null if invalid but doesn't clear input)
  onBlur() {
    if (!this.value) {
      this.value = undefined;
      this.valueChange.emit();
    }
  }
}
