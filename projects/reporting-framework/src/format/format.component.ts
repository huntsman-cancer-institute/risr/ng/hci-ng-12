import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Format} from "../model/format.model";


@Component({
    selector: "hci-format",
    template: `
        <div class="radio-pair">
            <input type="radio" #radio
                   [value]="format.name"
                   [id]="format.name"
                   [checked]="selectedValue === format.name ? true : false"
                   (click)="selectFormat(radio.value)"
                   name="format" attr.aria-label="hci-ng-format-{{selectedValue}}">
            <label>{{format.caption}}</label>
        </div>
    `,
    styles: [`
        .radio-pair{
            display: flex;
            align-items: center;
            padding-right: 5px;
        }
    `]
})
export class FormatComponent {

    @Input() selectedValue: string;
    @Output() selectedValueChange = new EventEmitter<string>();

    format: Format;

    selectFormat(s: string) {
        this.selectedValue = s;
        this.selectedValueChange.emit(s);
    }

}
