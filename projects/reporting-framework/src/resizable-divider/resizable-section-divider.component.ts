import {AfterViewInit, Component, ElementRef, HostListener, Input, OnDestroy, ViewChild} from "@angular/core";

@Component({
    selector: "resizable-section-divider",
    template: `
        <div *ngIf="isVerticalLayout" style="height: 100%; width: 100%;" #verticalContainer>
            <div class="p-1" style="width: 100%;" [style.min-height.%]="minPercentage" [style.max-height.%]="maxPercentage" [style.height.%]="(100 - sectionSize - grabberOffset)">
                <ng-content select="[topSection]"></ng-content>
            </div>
            <div class="d-flex align-items-center justify-content-center" style="height: 20px; width: 100%;">
                <div class="rad-grabber-vertical" (mousedown)="dragging = true;" (touchstart)="dragging = true;">
                    <i class="fas fa-grip-lines"></i>
                </div>
            </div>
            <div class="p-1" style="width: 100%;" [style.min-height.%]="minPercentage" [style.max-height.%]="maxPercentage" [style.height.%]="(sectionSize - grabberOffset)">
                <ng-content select="[bottomSection]"></ng-content>
            </div>
        </div>

        <div *ngIf="!isVerticalLayout" class="d-flex" style="height: 100%; width: 100%;" #horizontalContainer>
            <div class="p-1" style="height: 100%;" [style.min-width.%]="minPercentage" [style.max-width.%]="maxPercentage" [style.width.%]="(100 - sectionSize - grabberOffset)">
                <ng-content select="[leftSection]"></ng-content>
            </div>
            <div class="d-flex align-items-center justify-content-center" style="width: 10px; height: 100%;">
                <div class="rad-grabber" (mousedown)="dragging = true;" (touchstart)="dragging = true;">
                    <i class="fas fa-grip-lines-vertical"></i>
                </div>
            </div>
            <div class="p-1" style="height: 100%;" [style.min-width.%]="minPercentage" [style.max-width.%]="maxPercentage" [style.width.%]="sectionSize - grabberOffset">
                <ng-content select="[rightSection]"></ng-content>
            </div>
        </div>
    `,
  styles: [`
    .rad-grabber {
      cursor: ew-resize;
    }

    .rad-grabber-vertical {
      cursor: ns-resize;
    }
  `]
})
export class ResizableSectionDividerComponent implements AfterViewInit, OnDestroy {
    // Starting width or height % for the right / bottom containers respectively
    @Input() initialPercentage: number = 50;
    @Input() isVerticalLayout: boolean = false;
    @Input() maxPercentage: number = 90;
    @Input() minPercentage: number = 10;

    @ViewChild("verticalContainer", {static: false}) verticalContainer: ElementRef;
    @ViewChild("horizontalContainer", {static: false}) horizontalContainer: ElementRef;

    public sectionSize: number;
    public dragging: boolean = false;
    public grabberOffset = 0;

  private verticalTouchFunction: any;
  private horizontalTouchFunction: any;
  private elementObserver: any;

    ngOnInit() {
        this.sectionSize = this.initialPercentage;

      this.verticalTouchFunction = (event) => {
        if (this.dragging) {
          this.grabberOffset = ((( 20 / this.verticalContainer.nativeElement.offsetHeight) * 100 ) / 2);
          this.sectionSize = ((this.verticalContainer.nativeElement.offsetHeight - (event.touches[0].pageY - this.verticalContainer.nativeElement.getBoundingClientRect().top))
            / (this.verticalContainer.nativeElement.offsetHeightt) * 100);
          event.stopPropagation();
          event.preventDefault();
        }
      };

      this.horizontalTouchFunction = (event) => {
        if (this.dragging) {
          this.grabberOffset = ((( 10 / this.horizontalContainer.nativeElement.offsetWidth) * 100 ) / 2);
          this.sectionSize = ((this.horizontalContainer.nativeElement.offsetWidth - event.touches[0].pageX - this.horizontalContainer.nativeElement.getBoundingClientRect().left)
            / (this.horizontalContainer.nativeElement.offsetWidth) * 100);
          event.stopPropagation();
          event.preventDefault();
        }
      };
    }
    ngAfterViewInit() {
        // Handle initializing grabber size for when elements are initialized as hidden
        this.elementObserver = new IntersectionObserver((entries, observable) => {
               entries.forEach(entry => {
                  if(entry.isIntersecting) {
                      // subtract half the grabber height/width % from each section
                      if(this.isVerticalLayout) {
                          this.grabberOffset = ((( 20 / this.verticalContainer.nativeElement.offsetHeight) * 100 ) / 2);
                      } else {
                          this.grabberOffset = ((( 10 / this.horizontalContainer.nativeElement.offsetWidth) * 100 ) / 2);
                      }
                      observable.disconnect();
                  }
               });
        });

        if(this.isVerticalLayout) {
            this.elementObserver.observe(this.verticalContainer.nativeElement);
            window.addEventListener("touchmove", this.verticalTouchFunction, { passive: false });
        } else {
            this.elementObserver.observe(this.horizontalContainer.nativeElement);
            window.addEventListener("touchmove", this.horizontalTouchFunction, { passive: false });
        }
    }

    @HostListener("document:mousemove", ["$event"])
    mouseMove(event: MouseEvent) {
        if (this.dragging) {
            if(this.isVerticalLayout) {
                this.grabberOffset = ((( 20 / this.verticalContainer.nativeElement.offsetHeight) * 100 ) / 2);
                this.sectionSize = ((this.verticalContainer.nativeElement.offsetHeight - (event.pageY - this.verticalContainer.nativeElement.getBoundingClientRect().top))
                    / (this.verticalContainer.nativeElement.offsetHeight) * 100);
                event.stopPropagation();
                event.preventDefault();
            } else {
                this.grabberOffset = ((( 10 / this.horizontalContainer.nativeElement.offsetWidth) * 100 ) / 2);
                this.sectionSize = ((this.horizontalContainer.nativeElement.offsetWidth - event.pageX  - this.horizontalContainer.nativeElement.getBoundingClientRect().left)
                    / (this.horizontalContainer.nativeElement.offsetWidth) * 100);
                event.stopPropagation();
                event.preventDefault();
            }
        }
    }
    @HostListener("document:mouseup", ["$event"])
    @HostListener("document:touchend", ["$event"])
    mouseUp(event: MouseEvent) {
        if (this.dragging) {
            this.dragging = false;
        }
    }

    ngOnDestroy() {
      if(this.isVerticalLayout) {
        window.removeEventListener("touchmove", this.verticalTouchFunction);
      } else {
        window.removeEventListener("touchmove", this.horizontalTouchFunction);
      }
    }

}
