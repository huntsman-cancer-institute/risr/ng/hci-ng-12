
export class Option {

    public value: string;
    public display: string;
    public displayOrder: number;

}
