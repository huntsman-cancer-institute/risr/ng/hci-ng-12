import {Component, Inject, Input} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
    selector: "spinner-dialog",
    template: `
      <div mat-dialog-content class="full-height padded">
        <div class="full-width full-height t">
          <div class="tr">
            <div class="td vertical-center">
              <mat-spinner [hidden]="false" strokeWidth="{{strokeWidth}}" [diameter]="diameter"></mat-spinner>
            </div>
            <div class="td vertical-center">
              {{message}}
            </div>
          </div>
        </div>
      </div>
      <div mat-dialog-actions class="not-rendered">
      </div>
    `,
    styles: [`
        .t  { display: table; }
        .tr { display: table-row; }
        .td { display: table-cell; }

        .full-height { height: 100%; }
        .full-width  { width:  100%; }

        .vertical-center { vertical-align: middle; }

        .padded {
            padding-top: 0.4em;
            padding-bottom: 0.4em;
        }

        .not-rendered {
            padding: 0;
            margin:  0;

            min-height: 0;
            max-height: 0;
            height:     0;
        }

    `]
})
export class SpinnerDialogComponent {
    @Input("strokeWidth") strokeWidth: number = 3;
    @Input("diameter") diameter: number = 30;

    @Input("message") message: string = "Loading...";

    constructor(private dialogRef: MatDialogRef<SpinnerDialogComponent>,
                @Inject(MAT_DIALOG_DATA) private data) {
        if (this.data) {
            if (!!this.data.message) {
                this.message = this.data.message;
            }
            if (!!this.data.diameter) {
                this.diameter = this.data.diameter;
            }
            if (!!this.data.strokeWidth) {
                this.strokeWidth = this.data.strokeWidth;
            }
        }
    }


}
