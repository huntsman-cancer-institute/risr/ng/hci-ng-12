import {MatDialogRef} from "@angular/material/dialog";
import {FormGroup} from "@angular/forms";

export enum ActionType {
    PRIMARY = "primary",
    SECONDARY = "accent"
}
export interface GDActionConfig {
    formSource?: FormGroup;
    actions: GDAction[];
}
export interface GDAction {
    type: ActionType;
    internalAction?: string;
    externalAction?: (dialogRef?: MatDialogRef<any>) => void | string;
    icon?: string;
    name: string;

}
